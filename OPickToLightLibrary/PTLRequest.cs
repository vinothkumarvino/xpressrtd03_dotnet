using MSClassLibrary;
using System;

namespace OPickToLightLibrary
{
	public class PTLRequest : MSDisposableManager
	{
		private string STX = '\u0002'.ToString();

		private string ETX = '\u0003'.ToString();

		public int SequenceNo = 0;

		public string DpsCommand = string.Empty;

		public PTLRequest()
		{
		}

		public override string ToString()
		{
			string[] sTX = new string[] { this.STX, this.SequenceNo.ToString("000"), null, null, null };
			int length = this.DpsCommand.Length;
			sTX[2] = length.ToString("0000");
			sTX[3] = this.DpsCommand;
			sTX[4] = this.ETX;
			return string.Concat(sTX);
		}
	}
}