using System;

namespace OPickToLightLibrary
{
	public delegate void DataReceivedEventHandler(Guid Id, string systemName, bool IsStock, string data);
}