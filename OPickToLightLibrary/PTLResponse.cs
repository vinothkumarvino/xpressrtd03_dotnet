using MSClassLibrary;
using System;
using System.Runtime.InteropServices;

namespace OPickToLightLibrary
{
	public class PTLResponse : MSDisposableManager
	{
		private PTLResponse.ControllerMessageStruct cmStruct;

		public string SeqNo = string.Empty;

		public string DataLength = string.Empty;

		public string Data = string.Empty;

		public PTLResponse()
		{
		}

		public void Parse(string strInput)
		{
			IntPtr bSTR = Marshal.StringToBSTR(strInput);
			this.cmStruct = (PTLResponse.ControllerMessageStruct)Marshal.PtrToStructure(bSTR, typeof(PTLResponse.ControllerMessageStruct));
			this.SeqNo = this.cmStruct.SeqNo;
			this.DataLength = this.cmStruct.DataLength;
			this.Data = this.cmStruct.Data.Substring(0, this.cmStruct.Data.Length);
		}

		private struct ControllerMessageStruct
		{
			public string SeqNo;

			public string DataLength;

			public string Data;
		}
	}
}