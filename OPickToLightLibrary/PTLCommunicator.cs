using MSClassLibrary;
using MSClassLibrary.Log;
using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;

namespace OPickToLightLibrary
{
	public class PTLCommunicator : MSDisposableManager
	{
		private Socket socketController;

		private NetworkStream nsRecv;

		private Thread oThread;

		private Guid _Id = Guid.Empty;

		private string _systemName = string.Empty;

		private string _IpAddress = string.Empty;

		private int _PortNo = -1;

		private int _MessageSequenceNo = 0;

		private bool _ExitFlag = false;

		private bool _IsStock = false;

		private bool _buzzerOn = false;

		private bool _flashOn = false;

		public bool BuzzerOn
		{
			get
			{
				return this._buzzerOn;
			}
			set
			{
				this._buzzerOn = value;
			}
		}

		public bool FlashOn
		{
			get
			{
				return this._flashOn;
			}
			set
			{
				this._flashOn = value;
			}
		}

		public bool IsStock
		{
			get
			{
				return this._IsStock;
			}
			set
			{
				this._IsStock = value;
			}
		}

		public string SystemName
		{
			get
			{
				return this._systemName;
			}
		}

		public PTLCommunicator(string systemName)
		{
			this._systemName = systemName;
		}

		public bool Connect(string IpAddress, int Port)
		{
			bool flag = false;
			this._IpAddress = IpAddress;
			this._PortNo = Port;
			if ((this.socketController == null ? false : this.socketController.Connected))
			{
				flag = true;
			}
			else
			{
				try
				{
					try
					{
						IPEndPoint pEndPoint = new IPEndPoint(IPAddress.Parse(this._IpAddress), this._PortNo);
						this.socketController = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
						SysLog.WriteLog(6, string.Concat(new string[] { "Connecting to Controller...(", this._IpAddress, "@", this._PortNo.ToString(), " [Session:", this._systemName, "])" }));
						this.socketController.Connect(pEndPoint);
						if (this.oThread != null)
						{
							if (this.oThread.IsAlive)
							{
								this.oThread.Abort(1000);
								this.oThread.Join();
							}
						}
						this.oThread = new Thread(new ThreadStart(this.Receive));
						this.oThread.Start();
						SysLog.WriteLog(6, "Connecting to Controller - Success");
						this.Initialize(this._systemName);
						flag = true;
					}
					catch (Exception exception)
					{
						SysLog.WriteLog(3, exception.Message);
					}
				}
				finally
				{
					SysLog.WriteLog(6, "Connect - Done");
				}
			}
			return flag;
		}

		public void Disconnect()
		{
			try
			{
				this._ExitFlag = false;
				if (this.nsRecv != null)
				{
					this.nsRecv.ReadTimeout = 10;
					this.nsRecv.Close();
					this.nsRecv.Dispose();
				}
				if ((this.socketController == null ? false : this.socketController.Connected))
				{
					this.socketController.Shutdown(SocketShutdown.Both);
					this.socketController.Close();
				}
				else if (this.socketController != null)
				{
					this.socketController.Close();
				}
				if (this.oThread != null)
				{
					this.oThread.Abort(1000);
					this.oThread.Join();
				}
			}
			catch
			{
			}
		}

		protected override void Finalize()
		{
			try
			{
				this.Disconnect();
			}
			finally
			{
				base.Finalize();
			}
		}

		public int GetMessageSequenceNo()
		{
			if (this._MessageSequenceNo > 999)
			{
				this._MessageSequenceNo = 0;
			}
			this._MessageSequenceNo++;
			return this._MessageSequenceNo;
		}

		public bool GetNetworkStatus()
		{
			bool flag;
			try
			{
				if (this.socketController == null)
				{
					flag = false;
				}
				else
				{
					flag = (!this.socketController.Poll(1, SelectMode.SelectRead) ? true : this.socketController.Available != 0);
				}
			}
			catch (SocketException socketException)
			{
				flag = false;
			}
			return flag;
		}

		private void Initialize(string SessionName)
		{
			using (PTLRequest pTLRequest = new PTLRequest())
			{
				pTLRequest.DpsCommand = "@";
				pTLRequest.SequenceNo = this.GetMessageSequenceNo();
				this.Send(pTLRequest.ToString());
			}
			Thread.Sleep(500);
			using (PTLRequest messageSequenceNo = new PTLRequest())
			{
				messageSequenceNo.DpsCommand = "A";
				messageSequenceNo.SequenceNo = this.GetMessageSequenceNo();
				this.Send(messageSequenceNo.ToString());
			}
			Thread.Sleep(2000);
			using (PTLRequest pTLRequest1 = new PTLRequest())
			{
				pTLRequest1.DpsCommand = "Z";
				pTLRequest1.SequenceNo = this.GetMessageSequenceNo();
				this.Send(pTLRequest1.ToString());
			}
			Thread.Sleep(500);
		}

		private void OnDataReceived(string data)
		{
			DataReceivedEventHandler dataReceivedEventHandler = this.DataReceived;
			if (dataReceivedEventHandler != null)
			{
				dataReceivedEventHandler(this._Id, this._systemName, this._IsStock, data);
			}
			else
			{
			}
		}

		private void PTLSafeExit()
		{
			this._ExitFlag = false;
			if (this.nsRecv != null)
			{
				this.nsRecv.ReadTimeout = 10;
				this.nsRecv.Close();
				this.nsRecv.Dispose();
			}
			if ((this.socketController == null ? false : this.socketController.Connected))
			{
				this.socketController.Shutdown(SocketShutdown.Both);
				this.socketController.Close();
			}
			else if (this.socketController != null)
			{
				this.socketController.Close();
			}
		}

		private void Receive()
		{
			SysLog.WriteLog(6, "ReceiveData-Thread Started.");
			while (!this._ExitFlag)
			{
				try
				{
					try
					{
						this.nsRecv = new NetworkStream(this.socketController);
						if (this.nsRecv.CanRead)
						{
							this.nsRecv.ReadTimeout = -1;
							byte[] numArray = new byte[1024];
							int num = this.nsRecv.Read(numArray, 0, (int)numArray.Length);
							string str = Encoding.ASCII.GetString(numArray, 0, num);
							SysLog.WriteLog(6, string.Concat("<<", str));
							if (!string.IsNullOrEmpty(str))
							{
								this.OnDataReceived(str);
							}
							else
							{
								SysLog.WriteLog(4, "Warning: Symptoms look like, the PTL Controller restarted.");
								SysLog.WriteLog(6, "Closing the Socket");
								this.PTLSafeExit();
								break;
							}
						}
					}
					catch
					{
						this.nsRecv.ReadTimeout = 10;
						this.nsRecv.Close();
					}
				}
				finally
				{
					Thread.Sleep(1000);
				}
			}
		}

		public bool Reconnect()
		{
			bool flag = false;
			if ((this.socketController == null ? false : this.socketController.Connected))
			{
				flag = true;
			}
			else
			{
				try
				{
					try
					{
						IPEndPoint pEndPoint = new IPEndPoint(IPAddress.Parse(this._IpAddress), this._PortNo);
						this.socketController = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
						SysLog.WriteLog(6, string.Concat(new string[] { "Reconnecting to Controller...(", this._IpAddress, "@", this._PortNo.ToString(), " [Session:", this._systemName, "])" }));
						this.socketController.Connect(pEndPoint);
						if (this.oThread != null)
						{
							if (this.oThread.IsAlive)
							{
								this.oThread.Abort(1000);
								this.oThread.Join();
							}
						}
						this.oThread = new Thread(new ThreadStart(this.Receive));
						this.oThread.Start();
						SysLog.WriteLog(6, "Reonnecting to Controller - Success");
						this.Initialize(this._systemName);
						flag = true;
					}
					catch (Exception exception)
					{
						SysLog.WriteLog(3, exception.Message);
					}
				}
				finally
				{
					SysLog.WriteLog(6, "Connect - Done");
				}
			}
			return flag;
		}

		public void Send(string data)
		{
			try
			{
				if (!this.socketController.Connected)
				{
					this.Disconnect();
					this.Connect(this._IpAddress, this._PortNo);
				}
				if (this.socketController.Connected)
				{
					NetworkStream networkStream = new NetworkStream(this.socketController);
					if ((!networkStream.CanWrite ? false : data.Length > 0))
					{
						SysLog.WriteLog(6, string.Concat(">>", data));
						networkStream.Write(Encoding.ASCII.GetBytes(data), 0, data.Length);
						networkStream.Flush();
					}
				}
			}
			catch (Exception exception)
			{
				SysLog.WriteLog(3, exception.Message);
			}
		}

		public event DataReceivedEventHandler DataReceived;
	}
}