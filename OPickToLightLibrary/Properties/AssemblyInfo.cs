﻿using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Runtime.Versioning;

[assembly: AssemblyCompany("Hewlett-Packard Company")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCopyright("Copyright © Hewlett-Packard Company 2017")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: AssemblyProduct("OPickToLightLibrary")]
[assembly: AssemblyTitle("OPickToLightLibrary")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: CompilationRelaxations(8)]
[assembly: ComVisible(false)]
[assembly: Debuggable(DebuggableAttribute.DebuggingModes.Default | DebuggableAttribute.DebuggingModes.DisableOptimizations | DebuggableAttribute.DebuggingModes.IgnoreSymbolStoreSequencePoints | DebuggableAttribute.DebuggingModes.EnableEditAndContinue)]
[assembly: Guid("c123a75b-8763-4e54-b970-8fe007f874ef")]
[assembly: RuntimeCompatibility(WrapNonExceptionThrows=true)]
