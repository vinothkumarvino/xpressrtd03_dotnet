using System;
using System.Globalization;
using System.Linq;
using System.Text;

namespace OPickToLightLibrary.Comm
{
	public class SickData
	{
		private const int DATASIZE = 108;

		private DateTime _timeStamp = DateTime.Now;

		private string _patientID = string.Empty;

		private string _orderReference = string.Empty;

		private string _checkSum = string.Empty;

		private int _toteNo = 1;

		private int _totalNoOfTotes = 1;

		public string CheckSum
		{
			get
			{
				return this._checkSum;
			}
		}

		public string OrderReference
		{
			get
			{
				return this._orderReference;
			}
			set
			{
				this._orderReference = value;
			}
		}

		public string PatientID
		{
			get
			{
				return this._patientID;
			}
			set
			{
				this._patientID = value;
			}
		}

		public DateTime TimeStamp
		{
			get
			{
				return this._timeStamp;
			}
			set
			{
				this._timeStamp = value;
			}
		}

		public int TotalNoOfTotes
		{
			get
			{
				return this._totalNoOfTotes;
			}
			set
			{
				this._totalNoOfTotes = value;
			}
		}

		public int ToteNo
		{
			get
			{
				return this._toteNo;
			}
			set
			{
				this._toteNo = value;
			}
		}

		public string TotePaging
		{
			get
			{
				string str = string.Format("{0}/{1}", this._toteNo, this._totalNoOfTotes);
				return str;
			}
		}

		public SickData()
		{
		}

		private string CalculateCheckSum(byte[] someBytes)
		{
			byte num = 0;
			byte[] numArray = someBytes;
			for (int i = 0; i < (int)numArray.Length; i++)
			{
				num = (byte)(num + numArray[i] & 255);
			}
			return num.ToString("X2");
		}

		public byte[] GetBytes()
		{
			byte[] array = Enumerable.Repeat<byte>(32, 108).ToArray<byte>();
			string str = this.GetString();
			Buffer.BlockCopy(str.ToCharArray(), 0, array, 0, str.Length);
			return array;
		}

		public string GetString()
		{
			string empty = string.Empty;
			long num = long.Parse(this._timeStamp.ToString("yyyyMMddHHmmssff"));
			byte[] bytes = BitConverter.GetBytes(num);
			string str = string.Format("{0}|{1}|{2}|{3}|", new object[] { Encoding.ASCII.GetString(bytes), this.TotePaging, this._patientID, this._orderReference });
			empty = string.Concat(str, this.CalculateCheckSum(Encoding.ASCII.GetBytes(str))).PadRight(108).Substring(0, 108);
			return empty;
		}

		public void Parse(byte[] data)
		{
			string str = Encoding.ASCII.GetString(data);
			if (str.Contains("|"))
			{
				string[] strArrays = str.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
				if ((int)strArrays.Length == 5)
				{
					string str1 = string.Format("{0}|{1}|{2}|{3}|", new object[] { strArrays[0], strArrays[1], strArrays[2], strArrays[3] });
					string str2 = this.CalculateCheckSum(Encoding.ASCII.GetBytes(str1)).ToString();
					if (strArrays[5].Trim().Equals(str2))
					{
						long num = BitConverter.ToInt64(Encoding.ASCII.GetBytes(strArrays[0]), 0);
						this._timeStamp = DateTime.ParseExact(num.ToString(), "yyyyMMddHHmmssff", CultureInfo.InvariantCulture);
						if (strArrays[1].Contains("/"))
						{
							string[] strArrays1 = strArrays[1].Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
							if ((int)strArrays1.Length == 2)
							{
								this._toteNo = int.Parse(strArrays1[0]);
								this._totalNoOfTotes = int.Parse(strArrays1[1]);
							}
						}
						this._patientID = strArrays[2];
						this._orderReference = strArrays[3];
					}
				}
			}
		}
	}
}