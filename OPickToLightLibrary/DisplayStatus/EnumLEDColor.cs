using System;

namespace OPickToLightLibrary.DisplayStatus
{
	public enum EnumLEDColor
	{
		Blue = 1,
		Green = 2,
		LightBlue = 3,
		Red = 4,
		Purple = 5,
		Orange = 6,
		White = 7
	}
}