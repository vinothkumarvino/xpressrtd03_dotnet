using System;

namespace OPickToLightLibrary.DisplayStatus
{
	public static class LEDColor
	{
		public static string GetP5Settings()
		{
			return string.Format("{0}{1}{2}{3}{4}", new object[] { 'ÿ', 'ÿ', '\u007F', 'd', 'ÿ' });
		}

		public static string LightCombo(EnumLEDColor lColor, bool lightFlash, bool buzzerEnabled)
		{
			string empty = string.Empty;
			string str = string.Empty;
			string empty1 = string.Empty;
			empty1 = (!buzzerEnabled ? "00100001" : "00100011");
			switch (lColor)
			{
				case EnumLEDColor.Blue:
				{
					if (!lightFlash)
					{
						empty = "00010001";
						str = "00010010";
					}
					else
					{
						empty = "00010001";
						str = "00010011";
					}
					break;
				}
				case EnumLEDColor.Green:
				{
					if (!lightFlash)
					{
						empty = "00010001";
						str = "00100001";
					}
					else
					{
						empty = "00010001";
						str = "00110001";
					}
					break;
				}
				case EnumLEDColor.LightBlue:
				{
					if (!lightFlash)
					{
						empty = "00010001";
						str = "00100010";
					}
					else
					{
						empty = "00010001";
						str = "00110011";
					}
					break;
				}
				case EnumLEDColor.Red:
				{
					if (!lightFlash)
					{
						empty = "00010010";
						str = "00010001";
					}
					else
					{
						empty = "00010011";
						str = "00010001";
					}
					break;
				}
				case EnumLEDColor.Purple:
				{
					if (!lightFlash)
					{
						empty = "00010010";
						str = "00010010";
					}
					else
					{
						empty = "00010011";
						str = "00010011";
					}
					break;
				}
				case EnumLEDColor.Orange:
				{
					if (!lightFlash)
					{
						empty = "00010010";
						str = "00100001";
					}
					else
					{
						empty = "00010011";
						str = "00110001";
					}
					break;
				}
				default:
				{
					empty = "00010010";
					str = "00100010";
					break;
				}
			}
			int num = Convert.ToInt32(empty, 2);
			int num1 = Convert.ToInt32(str, 2);
			int num2 = Convert.ToInt32(empty1, 2);
			string str1 = string.Format("{0}{1}{2}", (char)num, (char)num1, (char)num2);
			return str1;
		}

		public static string LightOff()
		{
			string str = "00010001";
			string str1 = "00010001";
			string str2 = "00010001";
			int num = Convert.ToInt32(str, 2);
			int num1 = Convert.ToInt32(str1, 2);
			int num2 = Convert.ToInt32(str2, 2);
			string str3 = string.Format("{0}{1}{2}", (char)num, (char)num1, (char)num2);
			return str3;
		}

		public static string LightOnly(EnumLEDColor lColor, bool lightFlash, bool buzzerEnabled)
		{
			string empty = string.Empty;
			string str = string.Empty;
			string empty1 = string.Empty;
			empty1 = (!buzzerEnabled ? "00010001" : "00010011");
			switch (lColor)
			{
				case EnumLEDColor.Blue:
				{
					if (!lightFlash)
					{
						empty = "00010001";
						str = "00010010";
					}
					else
					{
						empty = "00010001";
						str = "00010011";
					}
					break;
				}
				case EnumLEDColor.Green:
				{
					if (!lightFlash)
					{
						empty = "00010001";
						str = "00100001";
					}
					else
					{
						empty = "00010001";
						str = "00110001";
					}
					break;
				}
				case EnumLEDColor.LightBlue:
				{
					if (!lightFlash)
					{
						empty = "00010001";
						str = "00100010";
					}
					else
					{
						empty = "00010001";
						str = "00110011";
					}
					break;
				}
				case EnumLEDColor.Red:
				{
					if (!lightFlash)
					{
						empty = "00010010";
						str = "00010001";
					}
					else
					{
						empty = "00010011";
						str = "00010001";
					}
					break;
				}
				case EnumLEDColor.Purple:
				{
					if (!lightFlash)
					{
						empty = "00010010";
						str = "00010010";
					}
					else
					{
						empty = "00010011";
						str = "00010011";
					}
					break;
				}
				case EnumLEDColor.Orange:
				{
					if (!lightFlash)
					{
						empty = "00010010";
						str = "00100001";
					}
					else
					{
						empty = "00010011";
						str = "00110001";
					}
					break;
				}
				default:
				{
					empty = "00010010";
					str = "00100010";
					break;
				}
			}
			int num = Convert.ToInt32(empty, 2);
			int num1 = Convert.ToInt32(str, 2);
			int num2 = Convert.ToInt32(empty1, 2);
			string str1 = string.Format("{0}{1}{2}", (char)num, (char)num1, (char)num2);
			return str1;
		}
	}
}