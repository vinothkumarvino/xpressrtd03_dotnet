using System;

namespace OPickToLightLibrary.DisplayStatus
{
	public class UpDownLight
	{
		public const string OFF = "0001";

		public const string ON = "0010";

		public const string FLASH = "0011";

		public const string HIGH_SPEED_FLASHING = "0100";

		public const string NO_CHANGE = "1111";

		public UpDownLight()
		{
		}
	}
}