using MSClassLibrary;
using System;

namespace XPressPTL.PickToLight.Common
{
	internal class PtlItem : MSDisposableManager
	{
		public int OrderSeqID = -1;

		public string OrderId = string.Empty;

		public string ArticleID = string.Empty;

		public string ArticleName = string.Empty;

		public string ItemCode = string.Empty;

		public string ItemBarCode = string.Empty;

		public bool OrderRejectByArticleMaster = false;

		public int PackSize = 1;

		public int RequestQuantity = 0;

		public int DispensingChannel = 0;

		public int DispensedQuantity = 0;

		public long ConsisOrderID = (long)0;

		public DateTime MinimumExpiryDate = DateTime.MinValue;

		public DateTime ExpiryDate = DateTime.MinValue;

		public string ToteID = string.Empty;

		public int OrderPackQuantity
		{
			get
			{
				int requestQuantity = -1;
				try
				{
					requestQuantity = this.RequestQuantity / this.PackSize;
				}
				catch
				{
				}
				return requestQuantity;
			}
		}

		public decimal OrderPackQuantityExtended
		{
			get
			{
				decimal minusOne = decimal.MinusOne;
				try
				{
					minusOne = this.RequestQuantity / this.PackSize;
				}
				catch
				{
				}
				return minusOne;
			}
		}

		public string StatusCode
		{
			get
			{
				string str = "CA";
				if ((this.DispensedQuantity <= 0 ? false : this.OrderPackQuantity == this.DispensedQuantity))
				{
					str = "CM";
				}
				else if ((this.DispensedQuantity <= 0 ? false : this.OrderPackQuantity > this.DispensedQuantity))
				{
					str = "CP";
				}
				return str;
			}
		}

		public PtlItem()
		{
		}
	}
}