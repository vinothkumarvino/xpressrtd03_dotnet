using System;

namespace XPressPTL.PickToLight.Common
{
	public enum EnumPtlRequest
	{
		HelloRequest,
		ColorInfoRequest,
		UnknownRequest
	}
}