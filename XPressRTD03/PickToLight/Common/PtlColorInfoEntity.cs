using System;
using System.Collections.Generic;

namespace XPressPTL.PickToLight.Common
{
	[Serializable]
	public class PtlColorInfoEntity
	{
		private string cmdText = string.Empty;

		private string msgID = string.Empty;

		private string timeStamp = string.Empty;

		private string msgCode = string.Empty;

		private string reasonCode = string.Empty;

		private EnumLightPower onOff = EnumLightPower.OFF;

		private int resentCount = 0;

		private string pigeonholeIds = string.Empty;

		private EnumXpressColor color = EnumXpressColor.NONE;

		private string content = string.Empty;

		public EnumXpressColor Color
		{
			get
			{
				return this.color;
			}
			set
			{
				this.color = value;
			}
		}

		public string Content
		{
			get
			{
				return this.content;
			}
			set
			{
				this.content = value;
			}
		}

		public string InvalidBinRequest
		{
			get
			{
				return this.cmdText.ToUpper().Replace("[REQUEST]", "[REJECTED]^[INVALID_PIGEONHOLE_ID]");
			}
		}

		public bool IsValidMessage
		{
			get
			{
				bool flag = false;
				if (this.MsgCode.Equals("[COLORINFO]"))
				{
					flag = true;
				}
				return flag;
			}
		}

		public bool IsValidReasonCode
		{
			get
			{
				bool flag = false;
				if ((this.ReasonCode.Contains("[REQUEST]") || this.ReasonCode.Contains("[FRONT]") ? true : this.ReasonCode.Contains("[BACK]")))
				{
					flag = true;
				}
				return flag;
			}
		}

		public string MsgCode
		{
			get
			{
				return this.msgCode;
			}
			set
			{
				this.msgCode = value;
			}
		}

		public string MsgID
		{
			get
			{
				return this.msgID;
			}
			set
			{
				this.msgID = value;
			}
		}

		public EnumLightPower OnOff
		{
			get
			{
				return this.onOff;
			}
			set
			{
				this.onOff = value;
			}
		}

		public string PigeonholeIds
		{
			get
			{
				return this.pigeonholeIds;
			}
			set
			{
				this.pigeonholeIds = value;
			}
		}

		public List<string> PigeonHoleList
		{
			get
			{
				return new List<string>(this.PigeonholeIds.Split(new char[] { '\u005E' }, StringSplitOptions.RemoveEmptyEntries));
			}
		}

		public string ReasonCode
		{
			get
			{
				return this.reasonCode;
			}
			set
			{
				this.reasonCode = value;
			}
		}

		public int ResentCount
		{
			get
			{
				return this.resentCount;
			}
			set
			{
				this.resentCount = value;
			}
		}

		public string TimeStamp
		{
			get
			{
				return this.timeStamp;
			}
			set
			{
				this.timeStamp = value;
			}
		}

		public PtlColorInfoEntity()
		{
		}

		public PtlColorInfoEntity(string input)
		{
			try
			{
				string[] strArrays = input.Split(new char[] { ',' }, StringSplitOptions.None);
				if ((int)strArrays.Length >= 9)
				{
					if (strArrays[2].ToUpper().Equals("[COLORINFO]"))
					{
						this.msgID = strArrays[0].Trim();
						this.timeStamp = strArrays[1].Trim();
						this.msgCode = strArrays[2].Trim();
						this.reasonCode = strArrays[3].Trim();
						this.resentCount = int.Parse(string.Concat("0", strArrays[5].Trim()));
						this.pigeonholeIds = strArrays[6].Trim();
						string str = strArrays[7].Trim().Replace("|", "");
						if (str.ToUpper().Equals("RED"))
						{
							this.color = EnumXpressColor.RED;
						}
						else if (str.ToUpper().Equals("BLUE"))
						{
							this.color = EnumXpressColor.BLUE;
						}
						else if (str.ToUpper().Equals("GREEN"))
						{
							this.color = EnumXpressColor.GREEN;
						}
						else if (str.ToUpper().Equals("WHITE"))
						{
							this.color = EnumXpressColor.WHITE;
						}
						else if (str.ToUpper().Equals("AQUA"))
						{
							this.color = EnumXpressColor.AQUA;
						}
						else if (str.ToUpper().Equals("ORANGE"))
						{
							this.color = EnumXpressColor.ORANGE;
						}
						else if (str.ToUpper().Equals("PURPLE"))
						{
							this.color = EnumXpressColor.PURPLE;
						}
						this.content = strArrays[8].Trim();
						if (!strArrays[4].Trim().ToUpper().Equals("ON"))
						{
							this.onOff = EnumLightPower.OFF;
							this.color = EnumXpressColor.NONE;
						}
						else
						{
							this.onOff = EnumLightPower.ON;
						}
						this.cmdText = input;
					}
				}
			}
			catch
			{
			}
		}
	}
}