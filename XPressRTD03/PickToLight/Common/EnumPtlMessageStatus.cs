using System;

namespace XPressPTL.PickToLight.Common
{
	public enum EnumPtlMessageStatus
	{
		OK,
		INCOMPLETE,
		DUPLICATE
	}
}