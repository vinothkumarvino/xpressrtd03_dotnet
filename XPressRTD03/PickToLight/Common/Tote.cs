using System;
using System.Collections.Generic;

namespace XPressPTL.PickToLight.Common
{
	internal class Tote
	{
		public string Id = string.Empty;

		public string Description = string.Empty;

		public List<PtlItem> ItemList = new List<PtlItem>();

		public Tote()
		{
		}
	}
}