using System;

namespace XPressPTL.PickToLight.Common
{
	public class PtlHelloEntity
	{
		private string msgID = string.Empty;

		private string timeStamp = string.Empty;

		private string msgCode = string.Empty;

		private string reasonCode = string.Empty;

		private int resentCount = 0;

		public bool IsValidMessage
		{
			get
			{
				bool flag = false;
				if (this.MsgCode.Equals("[HELLO]"))
				{
					flag = true;
				}
				return flag;
			}
		}

		public string MsgCode
		{
			get
			{
				return this.msgCode;
			}
			set
			{
				this.msgCode = value;
			}
		}

		public string MsgID
		{
			get
			{
				return this.msgID;
			}
			set
			{
				this.msgID = value;
			}
		}

		public string ReasonCode
		{
			get
			{
				return this.reasonCode;
			}
			set
			{
				this.reasonCode = value;
			}
		}

		public int ResentCount
		{
			get
			{
				return this.resentCount;
			}
			set
			{
				this.resentCount = value;
			}
		}

		public string Response
		{
			get
			{
				DateTime now;
				string empty = string.Empty;
				if (this.ResentCount == 0)
				{
					object[] msgID = new object[] { this.MsgID, null, null, null };
					now = DateTime.Now;
					msgID[1] = now.ToString("yyyy-MM-ddHH:mm:ss.fff");
					msgID[2] = "[ACK]";
					msgID[3] = EnumPtlMessageStatus.OK.ToString();
					empty = string.Format("{0},{1},{2},[{3}]", msgID);
				}
				else if (this.ResentCount <= 0)
				{
					object[] str = new object[] { this.MsgID, null, null, null };
					now = DateTime.Now;
					str[1] = now.ToString("yyyy-MM-ddHH:mm:ss.fff");
					str[2] = "[ACK]";
					str[3] = EnumPtlMessageStatus.INCOMPLETE.ToString();
					empty = string.Format("{0},{1},{2},[{3}]", str);
				}
				else
				{
					object[] objArray = new object[] { this.MsgID, null, null, null };
					now = DateTime.Now;
					objArray[1] = now.ToString("yyyy-MM-ddHH:mm:ss.fff");
					objArray[2] = "[ACK]";
					objArray[3] = EnumPtlMessageStatus.DUPLICATE.ToString();
					empty = string.Format("{0},{1},{2},[{3}]", objArray);
				}
				return empty;
			}
		}

		public string TimeStamp
		{
			get
			{
				return this.timeStamp;
			}
			set
			{
				this.timeStamp = value;
			}
		}

		public PtlHelloEntity()
		{
		}

		public PtlHelloEntity(string input)
		{
			try
			{
				string[] strArrays = input.Split(new char[] { ',' }, StringSplitOptions.None);
				if ((int)strArrays.Length >= 5)
				{
					if (strArrays[2].Trim().ToUpper().Equals("[HELLO]"))
					{
						this.msgID = strArrays[0].Trim();
						this.timeStamp = strArrays[1].Trim();
						this.msgCode = strArrays[2].Trim();
						this.reasonCode = strArrays[3].Trim();
						this.resentCount = int.Parse(string.Concat("0", strArrays[4].Trim()));
					}
				}
			}
			catch
			{
			}
		}
	}
}