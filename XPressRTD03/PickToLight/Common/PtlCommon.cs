using MSClassLibrary.Log;
using MSMsgQ;
using System;
using System.Messaging;
using XPressPTL;

namespace XPressPTL.PickToLight.Common
{
	public class PtlCommon
	{
		private static PtlCommon instance;

		public static bool PtlConncectionState;

		public static DateTime PtlSendDataTimeStamp;

		public static PtlCommon Instance
		{
			get
			{
				if (PtlCommon.instance == null)
				{
					PtlCommon.instance = new PtlCommon();
				}
				return PtlCommon.instance;
			}
		}

		static PtlCommon()
		{
			PtlCommon.PtlConncectionState = false;
			PtlCommon.PtlSendDataTimeStamp = DateTime.Now;
		}

		private PtlCommon()
		{
		}

		public void SendToOpas(string inputString)
		{
			try
			{
				inputString = string.Format("{0}{1}{2}", '\u0002', inputString, '\u0003');
				EnumQueueName enumQueueName = EnumQueueName.PickToLight2Opas;
				MsgQ.Send(EnumQueueName.PickToLight2Opas, MessagePriority.Normal, AppSysGlobal.GetLabel(enumQueueName.ToString()), inputString);
			}
			catch (Exception exception)
			{
				SysLog.WriteLog(3, exception.Message);
			}
		}
	}
}