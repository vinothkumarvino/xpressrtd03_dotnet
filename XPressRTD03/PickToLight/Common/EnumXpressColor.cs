using System;

namespace XPressPTL.PickToLight.Common
{
	public enum EnumXpressColor
	{
		NONE,
		RED,
		BLUE,
		GREEN,
		WHITE,
		AQUA,
		ORANGE,
		PURPLE
	}
}