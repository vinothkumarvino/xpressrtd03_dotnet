using MSClassLibrary.Log;
using MSMsgQ;
using System;
using System.Messaging;
using XPressPTL;
using XPressPTL.PickToLight.Common;
using XPressPTL.PickToLight.Request;

namespace XPressPTL.PickToLight
{
	public class XPressHandler
	{
		private static XPressHandler instance;

		public static XPressHandler Instance
		{
			get
			{
				if (XPressHandler.instance == null)
				{
					XPressHandler.instance = new XPressHandler();
				}
				return XPressHandler.instance;
			}
		}

		private XPressHandler()
		{
		}

		public void Process(string recvData)
		{
			try
			{
				try
				{
					string[] strArrays = recvData.Split(new char[] { '\u0002', '\u0003' }, StringSplitOptions.RemoveEmptyEntries);
					for (int i = 0; i < (int)strArrays.Length; i++)
					{
						string str = strArrays[i];
						string[] strArrays1 = str.Split(new char[] { ',' });
						if ((int)strArrays1.Length < 3)
						{
							SysLog.WriteLog(4, "Insufficient data for PTL Communication");
						}
						else if (strArrays1[2].Trim().ToUpper().Equals("[HELLO]"))
						{
							FHelloRequest.Instance.Queue2Ptl(EnumPtlRequest.HelloRequest, str);
						}
						else if (!strArrays1[2].Trim().ToUpper().Equals("[COLORINFO]"))
						{
							SysLog.WriteLog(4, "Invalid PTL Command Request");
						}
						else
						{
							FColorInfoRequest.Instance.Queue2Ptl(EnumPtlRequest.ColorInfoRequest, str);
						}
					}
				}
				catch (Exception exception)
				{
					SysLog.WriteLog(3, exception.Message);
				}
			}
			finally
			{
			}
		}

		public void ThrowError(string errMsg)
		{
			try
			{
				try
				{
					string str = string.Format("{0}{1}{2}", '\u0002', errMsg, '\u0003');
					EnumQueueName enumQueueName = EnumQueueName.PickToLight2Opas;
					MsgQ.Send(EnumQueueName.PickToLight2Opas, MessagePriority.Normal, AppSysGlobal.GetLabel(enumQueueName.ToString()), str);
				}
				catch (Exception exception)
				{
					SysLog.WriteLog(3, exception.Message);
				}
			}
			finally
			{
			}
		}
	}
}