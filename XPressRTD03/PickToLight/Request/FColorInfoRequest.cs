using MSClassLibrary.Log;
using MSMsgQ;
using System;
using System.Messaging;
using XPressPTL;
using XPressPTL.PickToLight.Common;

namespace XPressPTL.PickToLight.Request
{
	public class FColorInfoRequest
	{
		private static FColorInfoRequest instance;

		public static FColorInfoRequest Instance
		{
			get
			{
				if (FColorInfoRequest.instance == null)
				{
					FColorInfoRequest.instance = new FColorInfoRequest();
				}
				return FColorInfoRequest.instance;
			}
		}

		private FColorInfoRequest()
		{
		}

		public void Queue2Ptl(EnumPtlRequest ptlRequest, string rData)
		{
			PtlColorInfoEntity ptlColorInfoEntity = new PtlColorInfoEntity(rData);
			if (!ptlColorInfoEntity.IsValidMessage)
			{
				SysLog.WriteLog(4, "Invalid ColorInfo Request");
				object[] msgID = new object[] { ptlColorInfoEntity.MsgID, null, null, null };
				msgID[1] = DateTime.Now.ToString("yyyy-MM-ddHH:mm:ss.fff");
				msgID[2] = "[REJECTED]^[INVALID REQUEST]";
				msgID[3] = string.Empty;
				string str = string.Format("{0},{1},[COLORINFO],{2},0,{3},|OFF|", msgID);
				PtlCommon.Instance.SendToOpas(str);
			}
			else
			{
				EnumQueueName enumQueueName = EnumQueueName.Opas2PickToLightQNormal;
				MsgQ.Send(EnumQueueName.Opas2PickToLightQNormal, MessagePriority.Normal, AppSysGlobal.GetLabel(enumQueueName.ToString()), ptlColorInfoEntity);
			}
		}
	}
}