using MSClassLibrary.Log;
using System;
using XPressPTL.PickToLight.Common;

namespace XPressPTL.PickToLight.Request
{
	internal class FHelloRequest
	{
		private static FHelloRequest instance;

		public static FHelloRequest Instance
		{
			get
			{
				if (FHelloRequest.instance == null)
				{
					FHelloRequest.instance = new FHelloRequest();
				}
				return FHelloRequest.instance;
			}
		}

		private FHelloRequest()
		{
		}

		public void Queue2Ptl(EnumPtlRequest ptlRequest, string rData)
		{
			PtlHelloEntity ptlHelloEntity = new PtlHelloEntity(rData);
			if (!ptlHelloEntity.IsValidMessage)
			{
				SysLog.WriteLog(4, "Invalid Hello Request");
				object[] msgID = new object[] { ptlHelloEntity.MsgID, null, null, null };
				msgID[1] = DateTime.Now.ToString("yyyy-MM-ddHH:mm:ss.fff");
				msgID[2] = "[REJECTED]^[INVALID REQUEST]";
				msgID[3] = string.Empty;
				string str = string.Format("{0},{1},[HELLO],{2},0,{3},||", msgID);
				PtlCommon.Instance.SendToOpas(str);
			}
			else
			{
				PtlCommon.Instance.SendToOpas(ptlHelloEntity.Response);
			}
		}
	}
}