using MSClassLibrary.Log;
using MSMsgQ;
using OPickToLightLibrary;
using OPickToLightLibrary.DisplayStatus;
using OPtlDAL.BO;
using OPtlDAL.Entity;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Messaging;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using XPressPTL.PickToLight;
using XPressPTL.PickToLight.Common;

namespace XPressPTL
{
	public class MSTCPServer
	{
		private TcpListener tcpListener = null;

		private Thread imThread = null;

		private Thread omThread = null;

		private Thread coThread = null;

		private Thread ccThread = null;

		private NetworkStream opasStream = null;

		private TcpClient opasClient = null;

		private long connectedClients = (long)0;

		private string lastMsgSent = string.Empty;

		public MSTCPServer()
		{
		}

		~MSTCPServer()
		{
		}

		private void HandleExtraClientComm(object client)
		{
			using (TcpClient tcpClient = (TcpClient)client)
			{
				try
				{
					try
					{
						using (NetworkStream stream = tcpClient.GetStream())
						{
							ASCIIEncoding aSCIIEncoding = new ASCIIEncoding();
							try
							{
								SysLog.WriteLog(4, string.Format("{0} will not accept any extra clients", AppSysGlobal.SERVICE_NAME));
							}
							catch (SocketException socketException)
							{
								SysLog.WriteLog(3, socketException.Message);
							}
							catch (IOException oException)
							{
								SysLog.WriteLog(3, oException.Message);
							}
							catch (Exception exception)
							{
								SysLog.WriteLog(3, exception.Message);
							}
						}
					}
					catch (SocketException socketException1)
					{
						SysLog.WriteLog(3, socketException1.Message);
					}
					catch (IOException oException1)
					{
						SysLog.WriteLog(3, oException1.Message);
					}
					catch (Exception exception1)
					{
						SysLog.WriteLog(3, exception1.Message);
					}
				}
				finally
				{
					tcpClient.Close();
					SysLog.WriteLog(4, string.Format("Connection Closed by {0}", AppSysGlobal.SERVICE_NAME));
					this.connectedClients -= (long)1;
				}
			}
		}

		private bool HandlePtlRequest(Message requestData)
		{
			bool flag;
			bool flag1 = false;
			if (requestData != null)
			{
				PtlColorInfoEntity body = requestData.Body as PtlColorInfoEntity;
				if (!body.IsValidReasonCode)
				{
					flag1 = true;
				}
				else
				{
					foreach (string pigeonHoleList in body.PigeonHoleList)
					{
						List<PtlXpressMappingEntity> list = (new PtlXpressMapping()).GetList(pigeonHoleList);
						if (list.Count <= 0)
						{
							SysLog.WriteLog(4, string.Format("PigeonholeID [{0}] is not found in the mapping table.", body.PigeonholeIds));
							PtlCommon.Instance.SendToOpas(body.InvalidBinRequest);
							flag1 = true;
						}
						else
						{
							foreach (PtlXpressMappingEntity ptlXpressMappingEntity in list)
							{
								int num = 1;
								bool flag2 = (bool)num;
								ptlXpressMappingEntity.IsFront = (bool)num;
								if (!flag2)
								{
									flag = false;
								}
								else
								{
									flag = (body.ReasonCode.Equals("[REQUEST]") ? true : body.ReasonCode.Equals("[FRONT]"));
								}
								if (flag)
								{
									flag1 = this.LightControlRequest(body, ptlXpressMappingEntity);
								}
								int num1 = 1;
								flag2 = (bool)num1;
								ptlXpressMappingEntity.IsBack = (bool)num1;
								if ((!flag2 ? false : body.ReasonCode.Equals("[BACK]")))
								{
									flag1 = this.LightControlRequest(body, ptlXpressMappingEntity);
								}
							}
						}
					}
				}
			}
			return flag1;
		}

		private void HandleValidClientComm(object client)
		{
			try
			{
				try
				{
					using (TcpClient mAXIMUMBYTES = (TcpClient)client)
					{
						try
						{
							try
							{
								this.opasClient = mAXIMUMBYTES;
								mAXIMUMBYTES.ReceiveBufferSize = AppSysGlobal.MAXIMUM_BYTES;
								mAXIMUMBYTES.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.KeepAlive, true);
								int num = 0;
								StringBuilder stringBuilder = new StringBuilder();
								NetworkStream stream = mAXIMUMBYTES.GetStream();
								NetworkStream networkStream = stream;
								this.opasStream = stream;
								using (networkStream)
								{
									this.opasStream.WriteTimeout = AppSysGlobal.NETWORK_TIMEOUT;
									while (true)
									{
										if ((!mAXIMUMBYTES.Connected ? true : AppSysGlobal.exitFlag))
										{
											break;
										}
										try
										{
											if (mAXIMUMBYTES.Connected)
											{
												byte[] numArray = new byte[AppSysGlobal.MAXIMUM_BYTES];
												num = 0;
												stringBuilder = new StringBuilder();
												do
												{
													num = this.opasStream.Read(numArray, 0, (int)numArray.Length);
													SysLog.WriteLog(6, string.Concat(num.ToString(), " Bytes Recevied."));
													if (num <= 0)
													{
														throw new Exception("ZERO bytes recevied");
													}
													stringBuilder.AppendFormat("{0}", Encoding.UTF8.GetString(numArray, 0, num));
												}
												while (this.opasStream.DataAvailable);
												try
												{
													try
													{
														if (stringBuilder.Length != 0)
														{
															int length = stringBuilder.Length;
															SysLog.WriteLog(6, string.Concat(length.ToString(), " Total Bytes Recevied."));
															string str = stringBuilder.ToString();
															SysLog.WriteLog(8, str);
															string empty = string.Empty;
															if (!MSPtlControl.GetStatus())
															{
																string str1 = string.Format("{0} unable to process your request!", AppSysGlobal.SERVICE_NAME);
																SysLog.WriteLog(4, str1);
																XPressHandler.Instance.ThrowError(str1);
															}
															else
															{
																SysLog.WriteLog(4, string.Format("{0} is READY to process your request.", AppSysGlobal.SERVICE_NAME));
																XPressHandler.Instance.Process(str);
															}
														}
														else
														{
															SysLog.WriteLog(4, string.Format("The OPAS client has disconnected from the {0}.", AppSysGlobal.SERVICE_NAME));
														}
													}
													catch (Exception exception)
													{
														SysLog.WriteLog(3, exception.Message);
													}
												}
												finally
												{
													numArray = null;
													stringBuilder = null;
												}
											}
											else
											{
												this.opasStream.ReadTimeout = 1;
												break;
											}
										}
										catch (SocketException socketException)
										{
											SysLog.WriteLog(3, socketException.Message);
										}
										catch (IOException oException)
										{
											SysLog.WriteLog(3, oException.Message);
										}
									}
								}
							}
							catch (Exception exception1)
							{
								SysLog.WriteLog(3, exception1.Message);
							}
						}
						finally
						{
							mAXIMUMBYTES.Client.Shutdown(SocketShutdown.Both);
							mAXIMUMBYTES.Client.Disconnect(true);
						}
					}
				}
				catch
				{
				}
			}
			finally
			{
				this.opasStream = null;
				this.opasClient = null;
				this.connectedClients -= (long)1;
				SysLog.WriteLog(6, "Client Resources Released...");
			}
		}

		private bool IsRxClientConnected(TcpClient tcpClient)
		{
			bool flag;
			bool flag1 = false;
			try
			{
				if ((tcpClient == null || tcpClient.Client == null ? false : tcpClient.Client.Connected))
				{
					flag = (!tcpClient.Client.Poll(1, SelectMode.SelectRead) ? true : tcpClient.Client.Available != 0);
					return flag;
				}
			}
			catch
			{
			}
			flag = flag1;
			return flag;
		}

		private bool LightControlRequest(PtlColorInfoEntity entity, PtlXpressMappingEntity map)
		{
			bool flag = false;
			PtlBinsEntity ptlBinsEntity = (new PtlBins()).Get(map.SRC);
			if (ptlBinsEntity != null)
			{
				PtlBinLicEntity ptlBinLicEntity = new PtlBinLicEntity()
				{
					ID = Guid.NewGuid(),
					SystemName = ptlBinsEntity.SystemName,
					BinID = ptlBinsEntity.BinID,
					SRC = ptlBinsEntity.SRC,
					IsLightCommandIssued = false,
					ModuleID = ptlBinsEntity.ModuleID,
					ModuleColor = (int)entity.Color,
					ModuleFlash = ptlBinsEntity.ModuleFlash,
					DisplayContent = entity.Content,
					ArticleID = string.Empty,
					StockOut = decimal.Zero,
					StockIn = decimal.Zero,
					Adjustment = decimal.Zero,
					Remarks = string.Empty,
					RequestID = entity.MsgID,
					RequestCode = entity.ReasonCode,
					LightOnOffCommand = entity.OnOff.ToString(),
					OnTimeStamp = DateTime.Now,
					TurnOffByButtonPress = false,
					TurnOffByCommand = false,
					OffTimeStamp = DateTime.MinValue,
					UpdatedBy = AppSysGlobal.SERVICE_NAME,
					UpdatedOn = DateTime.Now,
					CreatedBy = AppSysGlobal.SERVICE_NAME,
					CreatedOn = DateTime.Now
				};
				if (!(new PtlBinLic()).Add(ref ptlBinLicEntity))
				{
					SysLog.WriteLog(2, "Light Control Request Error!");
				}
				else
				{
					flag = true;
				}
			}
			return flag;
		}

		private void OpasConnectionInputManager()
		{
			this.tcpListener.Start();
			while (!AppSysGlobal.exitFlag)
			{
				try
				{
					try
					{
						TcpClient tcpClient = this.tcpListener.AcceptTcpClient();
						this.connectedClients += (long)1;
						SysLog.WriteLog(6, string.Format("Total Clients Connected: {0}", this.connectedClients));
						if (this.connectedClients != (long)1)
						{
							string str = ((IPEndPoint)tcpClient.Client.RemoteEndPoint).Address.ToString();
							string str1 = ((IPEndPoint)tcpClient.Client.RemoteEndPoint).Port.ToString();
							SysLog.WriteLog(6, string.Format("Extra connection request from ({0}@{1})", str, str1));
							(new Thread(new ParameterizedThreadStart(this.HandleExtraClientComm))).Start(tcpClient);
						}
						else
						{
							string str2 = ((IPEndPoint)tcpClient.Client.RemoteEndPoint).Address.ToString();
							SysLog.WriteLog(6, string.Format("Accept connection from ({0})", str2));
							ConfigurationManager.RefreshSection("appSettings");
							try
							{
								if (!ConfigurationManager.AppSettings.AllKeys.Contains<string>(str2))
								{
									this.connectedClients -= (long)1;
									SysLog.WriteLog(6, string.Format("Access denied ({0}), Check your configuration.", str2));
								}
								else if (!ConfigurationManager.AppSettings[str2].Trim().ToLower().Equals("true"))
								{
									this.connectedClients -= (long)1;
									SysLog.WriteLog(6, string.Format("Access denied ({0}), Check your configuration.", str2));
								}
								else
								{
									SysLog.WriteLog(6, string.Format("Accepted connection from ({0})", str2));
									(new Thread(new ParameterizedThreadStart(this.HandleValidClientComm))).Start(tcpClient);
								}
							}
							catch (Exception exception)
							{
								SysLog.WriteLog(3, exception.Message);
							}
						}
					}
					catch (Exception exception1)
					{
						SysLog.WriteLog(3, exception1.Message);
					}
				}
				finally
				{
					GC.Collect();
				}
			}
		}

		private void OpasConnectionOutputManager()
		{
			bool flag = false;
			while (!AppSysGlobal.exitFlag)
			{
				try
				{
					try
					{
						Message message = new Message();
						if (MsgQ.Read(EnumQueueName.PickToLight2Opas, ref message))
						{
							if (message == null)
							{
								MsgQ.Delete(EnumQueueName.PickToLight2Opas, message);
							}
							else if (this.SendToClient(message.Label, message.Body.ToString()))
							{
								MsgQ.Delete(EnumQueueName.PickToLight2Opas, message);
							}
						}
						flag = (MsgQ.GetMessageCount(EnumQueueName.PickToLight2Opas) <= 0 ? false : true);
					}
					catch (MessageQueueException messageQueueException)
					{
						SysLog.WriteLog(3, messageQueueException.Message);
					}
					catch (Exception exception)
					{
						SysLog.WriteLog(3, exception.Message);
					}
				}
				finally
				{
					if (!flag)
					{
						Thread.Sleep(5000);
					}
				}
			}
		}

		private void PtlCommandManager()
		{
			while (!AppSysGlobal.exitFlag)
			{
				try
				{
					try
					{
						foreach (PtlBinLicEntity list in (new PtlBinLic()).GetList())
						{
							PtlControllerEntity ptlControllerEntity = (new PtlController()).Get(list.SystemName);
							if ((ptlControllerEntity == null ? false : ptlControllerEntity.OverwriteEnabled))
							{
								if (list.LightOnOffCommand.Trim().ToUpper().Equals("ON"))
								{
									this.TurnOnLight(list);
								}
								else if (!list.LightOnOffCommand.Trim().ToUpper().Equals("OFF"))
								{
									SysLog.WriteLog(4, "Invalid Light On/Off Command");
								}
								else
								{
									this.TurnOffLight(list);
								}
								list.IsLightCommandIssued = true;
								list.UpdatedBy = AppSysGlobal.SERVICE_NAME;
								list.UpdatedOn = DateTime.Now;
								(new PtlBinLic()).Save(list);
							}
							else if ((ptlControllerEntity == null ? false : !ptlControllerEntity.OverwriteEnabled))
							{
								EnumLightPower enumLightPower = EnumLightPower.ON;
								List<PtlBinLicEntity> sERVICENAME = (new PtlBinLic()).GetList(list.SRC, enumLightPower.ToString());
								if ((sERVICENAME.Count <= 0 ? true : sERVICENAME[0].IsLightCommandIssued))
								{
									SysLog.WriteLog(7, string.Format("Skip On Request [{0}], SRC: [{1}], Total Request : [{2}]", list.RequestID, list.SRC, sERVICENAME.Count));
								}
								else
								{
									sERVICENAME[0].IsLightCommandIssued = true;
									sERVICENAME[0].UpdatedBy = AppSysGlobal.SERVICE_NAME;
									sERVICENAME[0].UpdatedOn = DateTime.Now;
									(new PtlBinLic()).Save(sERVICENAME[0]);
									this.TurnOnLight(sERVICENAME[0]);
								}
								enumLightPower = EnumLightPower.OFF;
								List<PtlBinLicEntity> now = (new PtlBinLic()).GetList(list.SRC, enumLightPower.ToString());
								if (now.Count > 0)
								{
									now[0].IsLightCommandIssued = true;
									now[0].UpdatedBy = AppSysGlobal.SERVICE_NAME;
									now[0].UpdatedOn = DateTime.Now;
									(new PtlBinLic()).Save(now[0]);
									this.TurnOffLight(now[0]);
								}
							}
						}
					}
					catch (MessageQueueException messageQueueException)
					{
						SysLog.WriteLog(3, messageQueueException.Message);
					}
					catch (Exception exception)
					{
						SysLog.WriteLog(3, exception.Message);
					}
				}
				finally
				{
					Thread.Sleep(2000);
				}
			}
		}

		private void PtlConnectionManager()
		{
			MSPtlControl.StartClients();
			int num = 3;
			int messageCount = 0;
			int messageCount1 = 0;
			int num1 = 0;
			bool flag = false;
			while (!AppSysGlobal.exitFlag)
			{
				try
				{
					try
					{
						if (!MSPtlControl.GetStatus())
						{
							MSPtlControl.Reconnect();
						}
						Message message = new Message();
						messageCount = MsgQ.GetMessageCount(EnumQueueName.Opas2PickToLightQHigh);
						messageCount1 = MsgQ.GetMessageCount(EnumQueueName.Opas2PickToLightQNormal);
						num1 = MsgQ.GetMessageCount(EnumQueueName.Opas2PickToLightQLow);
						if (AppSysGlobal.SERVICE_DEBUG)
						{
							if (this.IsRxClientConnected(this.opasClient))
							{
								SysLog.WriteLog(7, string.Format("Rx-Client connection exists...", new object[0]));
							}
							else
							{
								SysLog.WriteLog(7, string.Format("Rx-Client connection not exists...", new object[0]));
							}
							SysLog.WriteLog(7, string.Format("HighQ:{0}, NormalQ:{1}, LowQ:{2}", messageCount, messageCount1, num1));
						}
						if (messageCount > 0)
						{
							int num2 = 0;
							while (num2 < messageCount)
							{
								if (num2 < num)
								{
									MsgQ.Read(EnumQueueName.Opas2PickToLightQHigh, ref message);
									if (this.HandlePtlRequest(message))
									{
										MsgQ.Delete(EnumQueueName.Opas2PickToLightQHigh, message);
									}
									num2++;
								}
								else
								{
									break;
								}
							}
						}
						if (messageCount1 > 0)
						{
							int num3 = 0;
							while (num3 < messageCount1)
							{
								if (num3 < num)
								{
									MsgQ.Read(EnumQueueName.Opas2PickToLightQNormal, ref message);
									if (this.HandlePtlRequest(message))
									{
										MsgQ.Delete(EnumQueueName.Opas2PickToLightQNormal, message);
									}
									num3++;
								}
								else
								{
									break;
								}
							}
						}
						if (num1 > 0)
						{
							int num4 = 0;
							while (num4 < num1)
							{
								if (num4 < num)
								{
									MsgQ.Read(EnumQueueName.Opas2PickToLightQLow, ref message);
									if (this.HandlePtlRequest(message))
									{
										MsgQ.Delete(EnumQueueName.Opas2PickToLightQLow, message);
									}
									num4++;
								}
								else
								{
									break;
								}
							}
						}
						messageCount = MsgQ.GetMessageCount(EnumQueueName.Opas2PickToLightQHigh);
						messageCount1 = MsgQ.GetMessageCount(EnumQueueName.Opas2PickToLightQNormal);
						num1 = MsgQ.GetMessageCount(EnumQueueName.Opas2PickToLightQLow);
						flag = ((messageCount > 0 || messageCount1 > 0 ? false : num1 <= 0) ? false : true);
					}
					catch (MessageQueueException messageQueueException)
					{
						SysLog.WriteLog(3, messageQueueException.Message);
					}
					catch (Exception exception)
					{
						SysLog.WriteLog(3, exception.Message);
					}
				}
				finally
				{
					if (!flag)
					{
						if (Convert.ToInt32((DateTime.Now - AppSysGlobal.DailyTaskTimeStamp).TotalDays) >= 1)
						{
							AppSysGlobal.DailyTaskTimeStamp = DateTime.Now;
							foreach (DailyTaskEntity list in (new DailyTask()).GetList())
							{
								list.LastExecutionTimeStamp = DateTime.Now;
								(new DailyTask()).Save(list);
							}
						}
					}
					Thread.Sleep(2000);
				}
			}
		}

		private bool SendToClient(string label, string data)
		{
			bool flag = false;
			int num = 0;
			SysLog.WriteLog(9, data);
			while (true)
			{
				if ((AppSysGlobal.exitFlag ? true : num >= AppSysGlobal.NO_OF_TRIES))
				{
					break;
				}
				try
				{
					try
					{
						if ((this.opasStream == null ? true : !this.opasStream.CanWrite))
						{
							SysLog.WriteLog(9, string.Format("Error!, {0} trying to resend data to OPAS", AppSysGlobal.SERVICE_NAME));
							Thread.Sleep(5000);
						}
						else
						{
							SysLog.WriteLog(9, string.Format("{0} try to send data to OPAS", AppSysGlobal.SERVICE_NAME));
							byte[] bytes = Encoding.ASCII.GetBytes(data);
							this.opasStream.Write(bytes, 0, (int)bytes.Length);
							this.opasStream.Flush();
							SysLog.WriteLog(9, string.Format("{0} --> OPAS", AppSysGlobal.SERVICE_NAME));
							flag = true;
							break;
						}
					}
					catch (ArgumentNullException argumentNullException)
					{
						SysLog.WriteLog(3, argumentNullException.Message);
						flag = false;
					}
					catch (SocketException socketException)
					{
						SysLog.WriteLog(3, socketException.Message);
						flag = false;
					}
					catch (Exception exception)
					{
						SysLog.WriteLog(3, exception.Message);
						flag = false;
					}
				}
				finally
				{
					this.lastMsgSent = label;
					num++;
					Thread.Sleep(250);
				}
			}
			return flag;
		}

		public void Start()
		{
			AppSysGlobal.exitFlag = false;
			SysLog.WriteLog(6, string.Concat("Start Listening @ ", AppSysGlobal.SERVICE_PORT.ToString()));
			this.tcpListener = new TcpListener(IPAddress.Any, AppSysGlobal.SERVICE_PORT);
			PtlSystemEntity hostName = (new PtlSystem()).Get(AppSysGlobal.SERVICE_NAME);
			if (hostName == null)
			{
				SysLog.WriteLog(3, string.Format("{0} Service failed to retrive the configuration from the database!", AppSysGlobal.SERVICE_NAME));
			}
			else
			{
				AppSysGlobal.SERVICE_CONFIGURATION = hostName;
				if (!hostName.IsActive)
				{
					SysLog.WriteLog(4, string.Format("{0} Service disabled by System Administrator from the database", AppSysGlobal.SERVICE_NAME));
				}
				else
				{
					try
					{
						SysLog.WriteLog(6, "Connecting to input manager");
						this.imThread = new Thread(new ThreadStart(this.OpasConnectionInputManager));
						this.imThread.Start();
						SysLog.WriteLog(6, "Connecting to output manager");
						this.omThread = new Thread(new ThreadStart(this.OpasConnectionOutputManager));
						this.omThread.Start();
						SysLog.WriteLog(6, "Connecting to Ptl Controller");
						this.coThread = new Thread(new ThreadStart(this.PtlConnectionManager));
						this.coThread.Start();
						SysLog.WriteLog(6, "Connecting to Command Controller");
						this.ccThread = new Thread(new ThreadStart(this.PtlCommandManager));
						this.ccThread.Start();
						if (hostName != null)
						{
							hostName.IPAddress = Dns.GetHostName();
							hostName.IPPort = AppSysGlobal.SERVICE_PORT;
							hostName.UnitNo = AppSysGlobal.SERVICE_ID;
							hostName.Status = 3;
							hostName.StatusMessage = EnumAppStatus.Running.ToString();
							hostName.StartDateTime = DateTime.Now;
							hostName.UpdatedBy = AppSysGlobal.SERVICE_NAME;
							hostName.UpdatedOn = DateTime.Now;
							(new PtlSystem()).Save(hostName);
						}
					}
					catch (Exception exception)
					{
						SysLog.WriteLog(3, exception.Message);
					}
				}
			}
		}

		public void Stop()
		{
			AppSysGlobal.exitFlag = true;
			try
			{
				try
				{
					this.tcpListener.Stop();
					this.imThread.Join(1000);
					if (this.imThread.IsAlive)
					{
						this.imThread.Abort();
					}
					this.imThread = null;
					this.omThread.Join(1000);
					if (this.omThread.IsAlive)
					{
						this.omThread.Abort();
					}
					this.omThread = null;
					this.coThread.Join(1000);
					if (this.coThread.IsAlive)
					{
						this.coThread.Abort();
					}
					this.coThread = null;
					this.ccThread.Join(1000);
					if (this.ccThread.IsAlive)
					{
						this.ccThread.Abort();
					}
					this.ccThread = null;
				}
				catch
				{
				}
			}
			finally
			{
				AppSysGlobal.UpdateAppStatus(AppSysGlobal.SERVICE_NAME, EnumAppStatus.Stopped, string.Empty, DateTime.MinValue, DateTime.Now);
			}
		}

		private void TurnOffLight(PtlBinLicEntity entity)
		{
			DateTime now;
			SysLog.WriteLog(6, string.Format("Message Request ID ( {0} ) - Light Off.", entity.RequestID));
			PtlControllerEntity ptlControllerEntity = (new PtlController()).Get(entity.SystemName);
			PtlXpressMappingEntity sRC = (new PtlXpressMapping()).GetSRC(entity.SRC);
			MSPtlControl.SendTurnOffCommand(ptlControllerEntity.SystemName, entity.ModuleIDString);
			MSPtlControl.SendBlockP5Command(ptlControllerEntity.SystemName, PickOrStock.Pick, string.Concat(entity.ModuleIDString, "00000"));
			string empty = string.Empty;
			PtlBinLicEntity lIC = (new PtlBinLic()).GetLIC(entity.SRC);
			if (lIC != null)
			{
				object[] requestID = new object[] { lIC.RequestID, null, null, null };
				now = DateTime.Now;
				requestID[1] = now.ToString("yyyy-MM-ddHH:mm:ss.fff");
				requestID[2] = "[RESPONSE]";
				requestID[3] = sRC.PigeonHoleID;
				empty = string.Format("{0},{1},[COLORINFO],{2},0,{3},|OFF|", requestID);
				PtlCommon.Instance.SendToOpas(empty);
				lIC.TurnOffByButtonPress = false;
				entity.TurnOffByCommand = true;
				lIC.OffTimeStamp = DateTime.Now;
				lIC.UpdatedBy = AppSysGlobal.SERVICE_NAME;
				lIC.UpdatedOn = DateTime.Now;
				(new PtlBinLic()).Save(lIC);
				(new PtlBinLic()).Delete(lIC);
			}
			lIC.TurnOffByButtonPress = false;
			entity.TurnOffByCommand = true;
			entity.OffTimeStamp = DateTime.Now;
			(new PtlBinLic()).Save(entity);
			(new PtlBinLic()).Delete(entity);
			if (string.IsNullOrEmpty(empty))
			{
				object[] str = new object[] { entity.RequestID, null, null, null };
				now = DateTime.Now;
				str[1] = now.ToString("yyyy-MM-ddHH:mm:ss.fff");
				str[2] = "[RESPONSE]";
				str[3] = sRC.PigeonHoleID;
				empty = string.Format("{0},{1},[COLORINFO],{2},0,{3},|OFF|", str);
				PtlCommon.Instance.SendToOpas(empty);
			}
		}

		private void TurnOnLight(PtlBinLicEntity entity)
		{
			object[] requestID = new object[] { entity.RequestID, entity.SRC, null, null };
			requestID[2] = entity.ModuleColor.ToString();
			requestID[3] = entity.DisplayContent;
			SysLog.WriteLog(6, string.Format("Message Request ID ( {0} ), Light {1} On (Color: {2}), Quantity: {3}", requestID));
			EnumXpressColor moduleColor = (EnumXpressColor)entity.ModuleColor;
			PtlControllerEntity ptlControllerEntity = (new PtlController()).Get(entity.SystemName);
			if (moduleColor == EnumXpressColor.RED)
			{
				if (!entity.IsDisplayContent)
				{
					MSPtlControl.SendColorOnlyCommand(ptlControllerEntity.SystemName, EnumLEDColor.Red, entity.ModuleIDString);
					MSPtlControl.SendBlockP5Command(ptlControllerEntity.SystemName, PickOrStock.Pick, string.Concat(entity.ModuleIDString, "00000"));
				}
				else
				{
					MSPtlControl.SendColorComboCommand(ptlControllerEntity.SystemName, EnumLEDColor.Red, entity.ModuleIDString);
					MSPtlControl.SendBlockP5Command(ptlControllerEntity.SystemName, PickOrStock.Pick, string.Concat(entity.ModuleIDString, entity.FormatedDisplayContent));
				}
			}
			else if (moduleColor == EnumXpressColor.BLUE)
			{
				if (!entity.IsDisplayContent)
				{
					MSPtlControl.SendColorOnlyCommand(ptlControllerEntity.SystemName, EnumLEDColor.Blue, entity.ModuleIDString);
					MSPtlControl.SendBlockP5Command(ptlControllerEntity.SystemName, PickOrStock.Pick, string.Concat(entity.ModuleIDString, "00000"));
				}
				else
				{
					MSPtlControl.SendColorComboCommand(ptlControllerEntity.SystemName, EnumLEDColor.Blue, entity.ModuleIDString);
					MSPtlControl.SendBlockP5Command(ptlControllerEntity.SystemName, PickOrStock.Pick, string.Concat(entity.ModuleIDString, entity.FormatedDisplayContent));
				}
			}
			else if (moduleColor == EnumXpressColor.GREEN)
			{
				if (!entity.IsDisplayContent)
				{
					MSPtlControl.SendColorOnlyCommand(ptlControllerEntity.SystemName, EnumLEDColor.Green, entity.ModuleIDString);
					MSPtlControl.SendBlockP5Command(ptlControllerEntity.SystemName, PickOrStock.Pick, string.Concat(entity.ModuleIDString, "00000"));
				}
				else
				{
					MSPtlControl.SendColorComboCommand(ptlControllerEntity.SystemName, EnumLEDColor.Green, entity.ModuleIDString);
					MSPtlControl.SendBlockP5Command(ptlControllerEntity.SystemName, PickOrStock.Pick, string.Concat(entity.ModuleIDString, entity.FormatedDisplayContent));
				}
			}
			else if (moduleColor == EnumXpressColor.WHITE)
			{
				if (!entity.IsDisplayContent)
				{
					MSPtlControl.SendColorOnlyCommand(ptlControllerEntity.SystemName, EnumLEDColor.White, entity.ModuleIDString);
					MSPtlControl.SendBlockP5Command(ptlControllerEntity.SystemName, PickOrStock.Pick, string.Concat(entity.ModuleIDString, "00000"));
				}
				else
				{
					MSPtlControl.SendColorComboCommand(ptlControllerEntity.SystemName, EnumLEDColor.White, entity.ModuleIDString);
					MSPtlControl.SendBlockP5Command(ptlControllerEntity.SystemName, PickOrStock.Pick, string.Concat(entity.ModuleIDString, entity.FormatedDisplayContent));
				}
			}
			else if (moduleColor == EnumXpressColor.AQUA)
			{
				if (!entity.IsDisplayContent)
				{
					MSPtlControl.SendColorOnlyCommand(ptlControllerEntity.SystemName, EnumLEDColor.LightBlue, entity.ModuleIDString);
					MSPtlControl.SendBlockP5Command(ptlControllerEntity.SystemName, PickOrStock.Pick, string.Concat(entity.ModuleIDString, "00000"));
				}
				else
				{
					MSPtlControl.SendColorComboCommand(ptlControllerEntity.SystemName, EnumLEDColor.LightBlue, entity.ModuleIDString);
					MSPtlControl.SendBlockP5Command(ptlControllerEntity.SystemName, PickOrStock.Pick, string.Concat(entity.ModuleIDString, entity.FormatedDisplayContent));
				}
			}
			else if (moduleColor == EnumXpressColor.ORANGE)
			{
				if (!entity.IsDisplayContent)
				{
					MSPtlControl.SendColorOnlyCommand(ptlControllerEntity.SystemName, EnumLEDColor.Orange, entity.ModuleIDString);
					MSPtlControl.SendBlockP5Command(ptlControllerEntity.SystemName, PickOrStock.Pick, string.Concat(entity.ModuleIDString, "00000"));
				}
				else
				{
					MSPtlControl.SendColorComboCommand(ptlControllerEntity.SystemName, EnumLEDColor.Orange, entity.ModuleIDString);
					MSPtlControl.SendBlockP5Command(ptlControllerEntity.SystemName, PickOrStock.Pick, string.Concat(entity.ModuleIDString, entity.FormatedDisplayContent));
				}
			}
			else if (moduleColor != EnumXpressColor.PURPLE)
			{
				entity.Remarks = "Unknown Color";
				entity.UpdatedBy = AppSysGlobal.SERVICE_NAME;
				entity.UpdatedOn = DateTime.Now;
				(new PtlBinLic()).Delete(entity);
				object[] str = new object[] { entity.RequestID, null, null, null };
				str[1] = DateTime.Now.ToString("yyyy-MM-ddHH:mm:ss.fff");
				str[2] = "[REJECTED]^[UNKNOWN_COLOR]";
				str[3] = string.Empty;
				string str1 = string.Format("{0},{1},[COLORINFO],{2},0,{3},|OFF|", str);
				PtlCommon.Instance.SendToOpas(str1);
			}
			else if (!entity.IsDisplayContent)
			{
				MSPtlControl.SendColorOnlyCommand(ptlControllerEntity.SystemName, EnumLEDColor.Purple, entity.ModuleIDString);
				MSPtlControl.SendBlockP5Command(ptlControllerEntity.SystemName, PickOrStock.Pick, string.Concat(entity.ModuleIDString, "00000"));
			}
			else
			{
				MSPtlControl.SendColorComboCommand(ptlControllerEntity.SystemName, EnumLEDColor.Purple, entity.ModuleIDString);
				MSPtlControl.SendBlockP5Command(ptlControllerEntity.SystemName, PickOrStock.Pick, string.Concat(entity.ModuleIDString, entity.FormatedDisplayContent));
			}
		}
	}
}