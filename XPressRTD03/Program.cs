using MSClassLibrary.Log;
using MSMsgQ;
using OPtlDAL.Common;
using System;
using System.Collections.Specialized;
using System.Configuration;
using System.Net.NetworkInformation;
using System.ServiceProcess;

namespace XPressPTL
{
	internal static class Program
	{
		private static void Main()
		{
			try
			{
				AppDomain.CurrentDomain.ProcessExit += new EventHandler(Program.OnProcessExit);
				SysConfig.ConnectionString = ConfigurationManager.ConnectionStrings["XPRESSPTL"].ConnectionString;
				AppSysGlobal.SERVICE_NAME = ConfigurationManager.AppSettings["SERVICE_NAME"];
				AppSysGlobal.SERVICE_PORT = int.Parse(string.Concat("0", ConfigurationManager.AppSettings["SERVICE_PORT"]));
				AppSysGlobal.SERVICE_ID = int.Parse(string.Concat("0", ConfigurationManager.AppSettings["SERVICE_ID"]));
				AppSysGlobal.SERVICE_DEBUG = bool.Parse(ConfigurationManager.AppSettings["SERVICE_DEBUG"]);
				MsgQ.CALLING_SERVICE_NAME = string.Format("MS-{0}-{1}", AppSysGlobal.SERVICE_NAME, AppSysGlobal.SERVICE_ID);
				SysLog.SERVICE_NAME = AppSysGlobal.SERVICE_NAME;
				NetworkChange.NetworkAddressChanged += new NetworkAddressChangedEventHandler(Program.NetworkChange_NetworkAddressChanged);
				ServiceBase.Run(new ServiceBase[] { new XPressPtlService() });
			}
			catch (Exception exception1)
			{
				Exception exception = exception1;
				SysLog.WriteLog(3, string.Concat("Exception..: ", exception.Message));
			}
		}

		private static void NetworkChange_NetworkAddressChanged(object sender, EventArgs e)
		{
			NetworkInterface[] allNetworkInterfaces = NetworkInterface.GetAllNetworkInterfaces();
			for (int i = 0; i < (int)allNetworkInterfaces.Length; i++)
			{
				NetworkInterface networkInterface = allNetworkInterfaces[i];
				SysLog.WriteLog(6, string.Format("{0} is {1}", networkInterface.Name, networkInterface.OperationalStatus));
			}
		}

		private static void OnProcessExit(object sender, EventArgs e)
		{
			SysLog.WriteLog(6, string.Format("{0} service exited.", AppSysGlobal.SERVICE_NAME));
		}
	}
}