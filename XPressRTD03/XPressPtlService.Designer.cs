using MSClassLibrary.Log;
using System;
using System.ComponentModel;
using System.ServiceProcess;

namespace XPressPTL
{
	public class XPressPtlService : ServiceBase
	{
		private IContainer components = null;

		public XPressPtlService()
		{
			this.InitializeComponent();
		}

		protected override void Dispose(bool disposing)
		{
			if ((!disposing ? false : this.components != null))
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			base.ServiceName = AppSysGlobal.SERVICE_NAME;
		}

		protected override void OnContinue()
		{
			base.OnContinue();
		}

		protected override void OnPause()
		{
			base.OnPause();
		}

		protected override void OnStart(string[] args)
		{
			MXPress.Start();
			SysLog.WriteEvent(base.ServiceName, string.Format("{0} service started.", base.ServiceName));
		}

		protected override void OnStop()
		{
			MXPress.Stop();
			SysLog.WriteEvent(base.ServiceName, string.Format("{0} service stopped.", base.ServiceName));
		}
	}
}