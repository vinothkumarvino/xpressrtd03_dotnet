using System;

namespace XPressPTL
{
	public enum EnumAppStatus
	{
		Unknown,
		Starting,
		Started,
		Running,
		Paused,
		Continued,
		Stopped
	}
}