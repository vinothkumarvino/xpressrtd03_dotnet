using MSClassLibrary.Log;
using OPtlDAL.BO;
using OPtlDAL.Entity;
using System;
using System.IO;
using System.Reflection;

namespace XPressPTL
{
	public static class AppSysGlobal
	{
		public static string SERVICE_NAME;

		public static string SENDING_APPLICATION;

		public static string SENDING_FACILITY;

		public static string PTL_CONTROLLER_IP;

		public static bool exitFlag;

		public readonly static string MARAICHOL;

		public static int SERVICE_ID;

		public static int SERVICE_PORT;

		public static bool SERVICE_DEBUG;

		public static int MAXIMUM_BYTES;

		public static int NO_OF_TRIES;

		public static int NETWORK_TIMEOUT;

		public static int PTL_CONTROLLER_PORT;

		public static DateTime DailyTaskTimeStamp;

		public static PtlSystemEntity SERVICE_CONFIGURATION;

		static AppSysGlobal()
		{
			AppSysGlobal.SERVICE_NAME = "XPressPTL";
			AppSysGlobal.SENDING_APPLICATION = "OPAS";
			AppSysGlobal.SENDING_FACILITY = EnumModules.XPRESSPTL.ToString();
			AppSysGlobal.PTL_CONTROLLER_IP = "127.0.0.1";
			AppSysGlobal.exitFlag = false;
			AppSysGlobal.MARAICHOL = "";
			AppSysGlobal.SERVICE_ID = 999;
			AppSysGlobal.SERVICE_PORT = 6050;
			AppSysGlobal.SERVICE_DEBUG = false;
			AppSysGlobal.MAXIMUM_BYTES = 8192;
			AppSysGlobal.NO_OF_TRIES = 3;
			AppSysGlobal.NETWORK_TIMEOUT = 30000;
			AppSysGlobal.PTL_CONTROLLER_PORT = 6050;
			AppSysGlobal.DailyTaskTimeStamp = DateTime.MinValue;
			AppSysGlobal.SERVICE_CONFIGURATION = null;
		}

		public static string GetBuildDate()
		{
			FileInfo fileInfo = new FileInfo(Assembly.GetExecutingAssembly().Location);
			return fileInfo.LastWriteTime.ToString("yyyy-MM-dd");
		}

		public static string GetLabel(string key)
		{
			DateTime now;
			string str;
			if (string.IsNullOrEmpty(key))
			{
				now = DateTime.Now;
				str = string.Format("M-{0}-{1}", now.ToString("yyyyMMddHHmmssfff"), "NORMAL");
			}
			else
			{
				now = DateTime.Now;
				str = string.Format("M-{0}-{1}", now.ToString("yyyyMMddHHmmssfff"), key);
			}
			return str;
		}

		public static string GetVersion()
		{
			return Assembly.GetExecutingAssembly().GetName().Version.ToString();
		}

		public static void UpdateAppStatus(string systemName, EnumAppStatus appStatus, string notes, DateTime dtStart, DateTime dtStopped)
		{
			PtlSystemEntity str = (new PtlSystem()).Get(systemName);
			if (str == null)
			{
				SysLog.WriteLog(4, "System name not found in the database!");
			}
			else
			{
				str.Status = (int)appStatus;
				str.StatusMessage = appStatus.ToString();
				str.Notes = notes;
				if (!dtStart.Equals(DateTime.MinValue))
				{
					str.StartDateTime = dtStart;
				}
				if (!dtStopped.Equals(DateTime.MinValue))
				{
					str.LastStopDateTime = dtStopped;
				}
				str.UpdatedBy = AppSysGlobal.SERVICE_NAME;
				str.UpdatedOn = DateTime.Now;
				(new PtlSystem()).Save(str);
			}
		}
	}
}