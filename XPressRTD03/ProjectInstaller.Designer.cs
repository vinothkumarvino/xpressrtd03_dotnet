using System;
using System.ComponentModel;
using System.Configuration;
using System.Configuration.Install;
using System.Reflection;
using System.ServiceProcess;

namespace XPressPTL
{
	[RunInstaller(true)]
	public class ProjectInstaller : Installer
	{
		private IContainer components = null;

		private ServiceProcessInstaller spi;

		private ServiceInstaller si;

		public ProjectInstaller()
		{
			this.InitializeComponent();
			AppSysGlobal.SERVICE_NAME = ProjectInstaller.GetServiceNameAppConfig("SERVICE_NAME");
			if (string.IsNullOrEmpty(AppSysGlobal.SERVICE_NAME))
			{
				AppSysGlobal.SERVICE_NAME = "XPressPTL";
				this.si.ServiceName = AppSysGlobal.SERVICE_NAME;
				this.si.DisplayName = string.Format("OmniHealth {0} Service", AppSysGlobal.SERVICE_NAME);
				this.si.Description = string.Format("{0} Service act as bridge interface between RxPress Workflow Engine and Pick to Light.", AppSysGlobal.SERVICE_NAME);
			}
			else
			{
				this.si.ServiceName = AppSysGlobal.SERVICE_NAME;
				this.si.DisplayName = string.Format("OmniHealth {0} Service", AppSysGlobal.SERVICE_NAME);
				this.si.Description = string.Format("{0} Service act as bridge interface between RxPress Workflow Engine and Pick to Light.", AppSysGlobal.SERVICE_NAME);
			}
			base.Installers.AddRange(new Installer[] { this.spi, this.si });
		}

		protected override void Dispose(bool disposing)
		{
			if ((!disposing ? false : this.components != null))
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		public static string GetServiceNameAppConfig(string serviceName)
		{
			Configuration configuration = ConfigurationManager.OpenExeConfiguration(Assembly.GetAssembly(typeof(ProjectInstaller)).Location);
			return configuration.AppSettings.Settings[serviceName].Value;
		}

		private void InitializeComponent()
		{
			this.spi = new ServiceProcessInstaller();
			this.si = new ServiceInstaller();
			this.spi.Account = ServiceAccount.LocalSystem;
			this.spi.Password = null;
			this.spi.Username = null;
			this.si.DelayedAutoStart = true;
			this.si.Description = "This application process  and translate RxPress Message for Consis";
			this.si.DisplayName = "OmniHealth XPressBM Service";
			this.si.ServiceName = "XPressBM";
			this.si.StartType = ServiceStartMode.Automatic;
		}
	}
}