using MSClassLibrary;
using MSClassLibrary.License;
using MSClassLibrary.Log;
using System;
using System.Collections.Specialized;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Reflection;

namespace XPressPTL
{
	public static class MXPress
	{
		private static MSTCPServer tcpServer;

		static MXPress()
		{
			MXPress.tcpServer = null;
		}

		public static void Start()
		{
			SysLog.WriteLog(6, string.Format("Starting {0} Service [{1}][{2}].", AppSysGlobal.SERVICE_NAME, AppSysGlobal.GetVersion(), AppSysGlobal.GetBuildDate()));
			AppSysGlobal.exitFlag = false;
			bool flag = true;
			string empty = string.Empty;
			string item = string.Empty;
			if (!ConfigurationManager.AppSettings.AllKeys.Contains<string>("CustomerName"))
			{
				SysLog.WriteLog(6, "Configuration Error!, App Key (CustomerName) not found.");
			}
			else
			{
				empty = ConfigurationManager.AppSettings["CustomerName"];
				if (!ConfigurationManager.AppSettings.AllKeys.Contains<string>("CustomerSerialId"))
				{
					SysLog.WriteLog(6, "Configuration Error!, App Key (CustomerSerialId) not found.");
				}
				else
				{
					item = ConfigurationManager.AppSettings["CustomerSerialId"];
					SysLog.WriteLog(6, string.Format("Customer Name: {0}", empty));
					SysLog.WriteLog(6, string.Format("Serial ID....: {0}", item));
					SysLog.WriteLog(6, "Verifying license detail");
					using (MSProductLicense mSProductLicense = new MSProductLicense())
					{
						FileVersionInfo versionInfo = FileVersionInfo.GetVersionInfo(Assembly.GetEntryAssembly().Location);
						mSProductLicense.set_SoftwareProductLicenseInfo(new MSProductLicenseInfo(versionInfo.ProductName, versionInfo.ProductVersion, versionInfo.LegalCopyright, versionInfo.CompanyName, empty, item));
						if (!mSProductLicense.Verify(item))
						{
							SysLog.WriteLog(4, "License information not found!, Please contact OmniHealth Pte Ltd, Singapore.");
							mSProductLicense.DumpLicenseRequest();
						}
						else
						{
							flag = true;
							SysLog.WriteLog(6, "Found valid license.");
						}
					}
				}
			}
			if (!flag)
			{
				SysLog.WriteLog(6, string.Format("Terminating the {0} service.", AppSysGlobal.SERVICE_NAME));
			}
			else
			{
				MXPress.tcpServer = new MSTCPServer();
				MXPress.tcpServer.Start();
				SysLog.WriteLog(6, string.Format("{0} service started.", AppSysGlobal.SERVICE_NAME));
			}
		}

		public static void Stop()
		{
			AppSysGlobal.exitFlag = true;
			SysLog.WriteLog(6, string.Format("Stopping the {0} service.", AppSysGlobal.SERVICE_NAME));
			if (MXPress.tcpServer != null)
			{
				MXPress.tcpServer.Stop();
			}
			MXPress.tcpServer = null;
			SysLog.WriteLog(6, string.Format("{0} service stopped.", AppSysGlobal.SERVICE_NAME));
			GC.Collect();
		}
	}
}