using MSClassLibrary.Log;
using OPickToLightLibrary;
using OPickToLightLibrary.DisplayStatus;
using OPtlDAL.BO;
using OPtlDAL.Entity;
using System;
using System.Collections.Generic;
using XPressPTL.PickToLight.Common;

namespace XPressPTL
{
	public static class MSPtlControl
	{
		private static PTLCommunicator[] clientList;

		private static PTLCommunicator GetClient(string systemName)
		{
			PTLCommunicator pTLCommunicator = null;
			PTLCommunicator[] pTLCommunicatorArray = MSPtlControl.clientList;
			int num = 0;
			while (num < (int)pTLCommunicatorArray.Length)
			{
				PTLCommunicator pTLCommunicator1 = pTLCommunicatorArray[num];
				if (!pTLCommunicator1.SystemName.Equals(systemName))
				{
					num++;
				}
				else
				{
					pTLCommunicator = pTLCommunicator1;
					break;
				}
			}
			return pTLCommunicator;
		}

		public static bool GetStatus()
		{
			bool flag = false;
			int num = 0;
			PTLCommunicator[] pTLCommunicatorArray = MSPtlControl.clientList;
			for (int i = 0; i < (int)pTLCommunicatorArray.Length; i++)
			{
				PTLCommunicator pTLCommunicator = pTLCommunicatorArray[i];
				if (!pTLCommunicator.GetNetworkStatus())
				{
					string str = string.Format("{0} is down.", pTLCommunicator.SystemName);
					SysLog.WriteLog(4, str);
					AppSysGlobal.UpdateAppStatus(pTLCommunicator.SystemName, EnumAppStatus.Stopped, str, DateTime.MinValue, DateTime.Now);
				}
				else
				{
					num++;
				}
			}
			if (num == (int)MSPtlControl.clientList.Length)
			{
				flag = true;
			}
			return flag;
		}

		private static void MSPickControl_DataReceived(Guid Id, string systemName, bool IsStock, string data)
		{
			string[] strArrays = data.Split(new char[] { '\u0002', '\u0003' }, StringSplitOptions.RemoveEmptyEntries);
			for (int i = 0; i < (int)strArrays.Length; i++)
			{
				string str = strArrays[i];
				SysLog.WriteLog(6, str);
				using (PTLResponse pTLResponse = new PTLResponse())
				{
					pTLResponse.Parse(str);
					if (pTLResponse.Data.StartsWith("@"))
					{
						SysLog.WriteLog(6, "Version Request.");
					}
					else if (pTLResponse.Data.StartsWith("o"))
					{
						SysLog.WriteLog(6, "Command Accepted.");
					}
					else if (pTLResponse.Data.StartsWith("n"))
					{
						SysLog.WriteLog(6, "Command Rejected.");
					}
					else if (pTLResponse.Data.StartsWith("t"))
					{
						MSPtlControl.SendAcknowledge(systemName, pTLResponse.SeqNo);
						int num = int.Parse(pTLResponse.Data.Substring(1, 4));
						SysLog.WriteLog(6, string.Format("{0} Light Module {1} Button Pressed", systemName, num));
						PtlBinsEntity lightModule = (new PtlBins()).GetLightModule(num);
						PtlBinLicEntity lIC = (new PtlBinLic()).GetLIC(lightModule.SRC);
						PtlXpressMappingEntity sRC = (new PtlXpressMapping()).GetSRC(lightModule.SRC);
						if ((sRC == null ? false : lIC != null))
						{
							object[] requestID = new object[] { lIC.RequestID, null, null, null };
							requestID[1] = DateTime.Now.ToString("yyyy-MM-ddHH:mm:ss.fff");
							requestID[2] = "[RESPONSE]";
							requestID[3] = sRC.PigeonHoleID;
							string str1 = string.Format("{0},{1},[COLORINFO],{2},0,{3},|OFF|", requestID);
							PtlCommon.Instance.SendToOpas(str1);
							lIC.TurnOffByButtonPress = true;
							lIC.TurnOffByCommand = false;
							lIC.OffTimeStamp = DateTime.Now;
							lIC.UpdatedBy = AppSysGlobal.SERVICE_NAME;
							lIC.UpdatedOn = DateTime.Now;
							(new PtlBinLic()).Save(lIC);
							(new PtlBinLic()).Delete(lIC);
						}
						PtlControllerEntity ptlControllerEntity = (new PtlController()).Get(Id);
						if (!IsStock)
						{
							PtlBinsEntity availableQuantity = (new PtlBins()).Get(ptlControllerEntity.SystemName, num);
							if (availableQuantity != null)
							{
								availableQuantity.AvailableQuantity = availableQuantity.AvailableQuantity - availableQuantity.PickQuantity;
								availableQuantity.PickQuantity = 0;
								availableQuantity.PickLightStatus = false;
								(new PtlBins()).Save(availableQuantity);
							}
						}
						else
						{
							PtlBinsEntity ptlBinsEntity = (new PtlBins()).Get(ptlControllerEntity.SystemName, num);
							if (ptlBinsEntity != null)
							{
								ptlBinsEntity.AvailableQuantity = ptlBinsEntity.AvailableQuantity + ptlBinsEntity.StockQuantity;
								ptlBinsEntity.StockQuantity = 0;
								ptlBinsEntity.StockLightStatus = false;
								(new PtlBins()).Save(ptlBinsEntity);
							}
						}
					}
				}
			}
		}

		public static void Reconnect()
		{
			PTLCommunicator[] pTLCommunicatorArray = MSPtlControl.clientList;
			for (int i = 0; i < (int)pTLCommunicatorArray.Length; i++)
			{
				PTLCommunicator pTLCommunicator = pTLCommunicatorArray[i];
				if (!pTLCommunicator.GetNetworkStatus())
				{
					pTLCommunicator.Reconnect();
				}
			}
		}

		private static void SendAcknowledge(string systemName, string seqNo)
		{
			PTLCommunicator client = MSPtlControl.GetClient(systemName);
			using (PTLRequest pTLRequest = new PTLRequest())
			{
				pTLRequest.DpsCommand = "O";
				pTLRequest.SequenceNo = int.Parse(seqNo);
				client.Send(pTLRequest.ToString());
			}
		}

		public static void SendBlockP1Command(string systemName, PickOrStock pos, string channelList)
		{
			PTLCommunicator client = MSPtlControl.GetClient(systemName);
			using (PTLRequest pTLRequest = new PTLRequest())
			{
				if (pos != PickOrStock.Pick)
				{
					client.IsStock = true;
				}
				else
				{
					client.IsStock = false;
				}
				pTLRequest.DpsCommand = string.Concat("P101", channelList);
				pTLRequest.SequenceNo = client.GetMessageSequenceNo();
				client.Send(pTLRequest.ToString());
			}
		}

		public static void SendBlockP5Command(string systemName, PickOrStock pos, string channelList)
		{
			PTLCommunicator client = MSPtlControl.GetClient(systemName);
			using (PTLRequest pTLRequest = new PTLRequest())
			{
				if (pos != PickOrStock.Pick)
				{
					client.IsStock = true;
				}
				else
				{
					client.IsStock = false;
				}
				pTLRequest.DpsCommand = string.Concat("P501", LEDColor.GetP5Settings(), channelList);
				pTLRequest.SequenceNo = client.GetMessageSequenceNo();
				client.Send(pTLRequest.ToString());
			}
		}

		public static void SendColorComboCommand(string systemName, EnumLEDColor enColor, string channelList)
		{
			PTLCommunicator client = MSPtlControl.GetClient(systemName);
			using (PTLRequest pTLRequest = new PTLRequest())
			{
				string str = LEDColor.LightCombo(enColor, client.FlashOn, client.BuzzerOn);
				pTLRequest.DpsCommand = string.Concat("Mm1", str, channelList);
				pTLRequest.SequenceNo = client.GetMessageSequenceNo();
				client.Send(pTLRequest.ToString());
			}
		}

		public static void SendColorOnlyCommand(string systemName, EnumLEDColor enColor, string channelList)
		{
			PTLCommunicator client = MSPtlControl.GetClient(systemName);
			using (PTLRequest pTLRequest = new PTLRequest())
			{
				string str = LEDColor.LightOnly(enColor, client.FlashOn, client.BuzzerOn);
				pTLRequest.DpsCommand = string.Concat("Mm1", str, channelList);
				pTLRequest.SequenceNo = client.GetMessageSequenceNo();
				client.Send(pTLRequest.ToString());
			}
		}

		public static void SendTurnOffCommand(string systemName, string channelList)
		{
			PTLCommunicator client = MSPtlControl.GetClient(systemName);
			using (PTLRequest pTLRequest = new PTLRequest())
			{
				string str = LEDColor.LightOff();
				pTLRequest.DpsCommand = string.Concat("Mm1", str, channelList);
				pTLRequest.SequenceNo = client.GetMessageSequenceNo();
				client.Send(pTLRequest.ToString());
			}
		}

		public static void StartClients()
		{
			SysLog.WriteLog(6, "PTL Init (Starting all Clients).");
			List<PtlSystemEntity> controllerList = (new PtlSystem()).GetControllerList("PTL");
			MSPtlControl.clientList = new PTLCommunicator[controllerList.Count];
			int num = 0;
			foreach (PtlSystemEntity ptlSystemEntity in controllerList)
			{
				if (ptlSystemEntity.IsActive)
				{
					PtlControllerEntity ptlControllerEntity = (new PtlController()).Get(ptlSystemEntity.SystemName);
					MSPtlControl.clientList[num] = new PTLCommunicator(ptlSystemEntity.SystemName);
					MSPtlControl.clientList[num].BuzzerOn = ptlControllerEntity.AudioEnabled;
					MSPtlControl.clientList[num].FlashOn = ptlControllerEntity.FlashEnabled;
					MSPtlControl.clientList[num].DataReceived += new DataReceivedEventHandler(MSPtlControl.MSPickControl_DataReceived);
					AppSysGlobal.UpdateAppStatus(ptlSystemEntity.SystemName, EnumAppStatus.Starting, string.Empty, DateTime.Now, DateTime.MinValue);
					MSPtlControl.clientList[num].Connect(ptlSystemEntity.IPAddress, ptlSystemEntity.IPPort);
					AppSysGlobal.UpdateAppStatus(ptlSystemEntity.SystemName, EnumAppStatus.Started, string.Empty, DateTime.Now, DateTime.MinValue);
					num++;
				}
			}
			MSPtlControl.GetStatus();
		}
	}
}