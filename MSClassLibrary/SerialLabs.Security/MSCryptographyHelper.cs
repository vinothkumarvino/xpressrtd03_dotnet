using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace SerialLabs.Security
{
	public static class MSCryptographyHelper
	{
		public static string ByteToHex(byte[] bits)
		{
			string str;
			if (bits != null)
			{
				StringBuilder stringBuilder = new StringBuilder((int)bits.Length * 2);
				for (int i = 0; i < (int)bits.Length; i++)
				{
					stringBuilder.Append(bits[i].ToString("x2"));
				}
				str = stringBuilder.ToString();
			}
			else
			{
				str = null;
			}
			return str;
		}

		public static string ComputeCRC16Hash(string value)
		{
			MSCryptographyHelper.Crc16Ccitt crc16Ccitt = new MSCryptographyHelper.Crc16Ccitt();
			string hex = MSCryptographyHelper.ByteToHex(crc16Ccitt.ComputeHashBytes(Encoding.UTF8.GetBytes(value)));
			return hex;
		}

		public static byte[] ComputeCRC16HashByte(string value)
		{
			MSCryptographyHelper.Crc16Ccitt crc16Ccitt = new MSCryptographyHelper.Crc16Ccitt();
			return crc16Ccitt.ComputeHashBytes(Encoding.UTF8.GetBytes(value));
		}

		public static string ComputeCRC32Hash(string value)
		{
			MSCryptographyHelper.Crc32 crc32 = new MSCryptographyHelper.Crc32();
			string hex = MSCryptographyHelper.ByteToHex(crc32.ComputeHash(Encoding.UTF8.GetBytes(value)));
			return hex;
		}

		public static byte[] ComputeCRC32HashByte(string value)
		{
			MSCryptographyHelper.Crc32 crc32 = new MSCryptographyHelper.Crc32();
			return crc32.ComputeHash(Encoding.UTF8.GetBytes(value));
		}

		public static string ComputeMD5Hash(string input)
		{
			string hex;
			if (!string.IsNullOrEmpty(input))
			{
				MD5 mD5 = null;
				byte[] numArray = null;
				try
				{
					mD5 = MD5.Create();
					numArray = mD5.ComputeHash(Encoding.UTF8.GetBytes(input));
				}
				finally
				{
					if (mD5 != null)
					{
						mD5.Dispose();
						mD5 = null;
					}
				}
				hex = MSCryptographyHelper.ByteToHex(numArray);
			}
			else
			{
				hex = null;
			}
			return hex;
		}

		public static string ComputeMD5HashFromFile(string fileName)
		{
			return MSCryptographyHelper.ComputeMD5HashFromFile(fileName, false);
		}

		public static string ComputeMD5HashFromFile(string fileName, bool base64)
		{
			string str;
			FileStream fileStream = null;
			MD5 mD5 = null;
			try
			{
				fileStream = File.OpenRead(fileName);
				mD5 = MD5.Create();
				str = (!base64 ? BitConverter.ToString(mD5.ComputeHash(fileStream)).Replace("-", "") : Convert.ToBase64String(mD5.ComputeHash(fileStream)));
			}
			finally
			{
				if (mD5 != null)
				{
					mD5.Dispose();
					mD5 = null;
				}
				if (fileStream != null)
				{
					fileStream.Dispose();
					fileStream = null;
				}
			}
			return str;
		}

		public static string ComputeSHA1Hash(string input)
		{
			string hex;
			if (!string.IsNullOrEmpty(input))
			{
				SHA1 sHA1 = null;
				byte[] numArray = null;
				try
				{
					sHA1 = SHA1.Create();
					numArray = sHA1.ComputeHash(Encoding.UTF8.GetBytes(input));
				}
				finally
				{
					if (sHA1 != null)
					{
						sHA1.Dispose();
						sHA1 = null;
					}
				}
				hex = MSCryptographyHelper.ByteToHex(numArray);
			}
			else
			{
				hex = null;
			}
			return hex;
		}

		public static string ComputeSHA1HashFromFile(string fileName)
		{
			return MSCryptographyHelper.ComputeSHA1HashFromFile(fileName, false);
		}

		public static string ComputeSHA1HashFromFile(string fileName, bool base64)
		{
			string str;
			FileStream fileStream = null;
			SHA1 sHA1 = null;
			try
			{
				fileStream = File.OpenRead(fileName);
				sHA1 = SHA1.Create();
				str = (!base64 ? BitConverter.ToString(sHA1.ComputeHash(fileStream)).Replace("-", "") : Convert.ToBase64String(sHA1.ComputeHash(fileStream)));
			}
			finally
			{
				if (sHA1 != null)
				{
					sHA1.Dispose();
					sHA1 = null;
				}
				if (fileStream != null)
				{
					fileStream.Dispose();
					fileStream = null;
				}
			}
			return str;
		}

		public static string ComputeSHA256Hash(string input)
		{
			string hex;
			if (!string.IsNullOrEmpty(input))
			{
				SHA256 sHA256 = null;
				byte[] numArray = null;
				try
				{
					sHA256 = SHA256.Create();
					numArray = sHA256.ComputeHash(Encoding.UTF8.GetBytes(input));
				}
				finally
				{
					if (sHA256 != null)
					{
						sHA256.Dispose();
						sHA256 = null;
					}
				}
				hex = MSCryptographyHelper.ByteToHex(numArray);
			}
			else
			{
				hex = null;
			}
			return hex;
		}

		public static string ComputeSHA256HashFromFile(string fileName)
		{
			return MSCryptographyHelper.ComputeSHA256HashFromFile(fileName, false);
		}

		public static string ComputeSHA256HashFromFile(string fileName, bool base64)
		{
			string str;
			FileStream fileStream = null;
			SHA256 sHA256 = null;
			try
			{
				fileStream = File.OpenRead(fileName);
				sHA256 = SHA256.Create();
				str = (!base64 ? BitConverter.ToString(sHA256.ComputeHash(fileStream)).Replace("-", "") : Convert.ToBase64String(sHA256.ComputeHash(fileStream)));
			}
			finally
			{
				if (sHA256 != null)
				{
					sHA256.Dispose();
					sHA256 = null;
				}
				if (fileStream != null)
				{
					fileStream.Dispose();
					fileStream = null;
				}
			}
			return str;
		}

		public static string DecodeBase64(string encodedValue)
		{
			byte[] numArray = Convert.FromBase64String(encodedValue);
			return Encoding.UTF8.GetString(numArray);
		}

		public static string EncodeToBase64(string value)
		{
			return Convert.ToBase64String(Encoding.UTF8.GetBytes(value));
		}

		internal sealed class Crc16Ccitt
		{
			private readonly uint DefaultPolynomial;

			private ushort[] _table;

			public Crc16Ccitt()
			{
				this.InitializeTable();
			}

			public ushort ComputeHash(byte[] buffer)
			{
				ushort num = 0;
				for (int i = 0; i < (int)buffer.Length; i++)
				{
					num = (ushort)(this._table[(num ^ buffer[i]) & 255] ^ num >> 8);
				}
				return num;
			}

			public byte[] ComputeHashBytes(byte[] buffer)
			{
				return BitConverter.GetBytes(this.ComputeHash(buffer));
			}

			private void InitializeTable()
			{
				this._table = new ushort[256];
				for (ushort i = 0; i < 256; i = (ushort)(i + 1))
				{
					ushort num = 0;
					ushort num1 = i;
					for (int j = 0; j < 8; j++)
					{
						num = (((num ^ num1) & 1) <= 0 ? (ushort)(num >> 1) : (ushort)((long)(num >> 1) ^ (ulong)this.DefaultPolynomial));
						num1 = (ushort)(num1 >> 1);
					}
					this._table[i] = num;
				}
			}
		}

		internal sealed class Crc32 : HashAlgorithm
		{
			public const uint DefaultPolynomial = 3988292384;

			public const uint DefaultSeed = 4294967295;

			private static uint[] defaultTable;

			private readonly uint seed;

			private readonly uint[] table;

			private uint hash;

			public override int HashSize
			{
				get
				{
					return 32;
				}
			}

			public Crc32() : this(-306674912, -1)
			{
			}

			public Crc32(uint polynomial, uint seed)
			{
				this.table = MSCryptographyHelper.Crc32.InitializeTable(polynomial);
				uint num = seed;
				uint num1 = num;
				this.hash = num;
				this.seed = num1;
			}

			private static uint CalculateHash(uint[] table, uint seed, IList<byte> buffer, int start, int size)
			{
				uint num = seed;
				for (int i = start; i < size - start; i++)
				{
					num = num >> 8 ^ table[buffer[i] ^ num & 255];
				}
				return num;
			}

			public static uint Compute(byte[] buffer)
			{
				return MSCryptographyHelper.Crc32.Compute(-1, buffer);
			}

			public static uint Compute(uint seed, byte[] buffer)
			{
				return MSCryptographyHelper.Crc32.Compute(-306674912, seed, buffer);
			}

			public static uint Compute(uint polynomial, uint seed, byte[] buffer)
			{
				uint num = ~MSCryptographyHelper.Crc32.CalculateHash(MSCryptographyHelper.Crc32.InitializeTable(polynomial), seed, buffer, 0, (int)buffer.Length);
				return num;
			}

			protected override void HashCore(byte[] buffer, int start, int length)
			{
				this.hash = MSCryptographyHelper.Crc32.CalculateHash(this.table, this.hash, buffer, start, length);
			}

			protected override byte[] HashFinal()
			{
				byte[] bigEndianBytes = MSCryptographyHelper.Crc32.UInt32ToBigEndianBytes(~this.hash);
				this.HashValue = bigEndianBytes;
				return bigEndianBytes;
			}

			public override void Initialize()
			{
				this.hash = this.seed;
			}

			private static uint[] InitializeTable(uint polynomial)
			{
				uint[] numArray;
				if ((polynomial != -306674912 ? true : MSCryptographyHelper.Crc32.defaultTable == null))
				{
					uint[] numArray1 = new uint[256];
					for (int i = 0; i < 256; i++)
					{
						uint num = (uint)i;
						for (int j = 0; j < 8; j++)
						{
							if ((num & 1) != 1)
							{
								num >>= 1;
							}
							else
							{
								num = num >> 1 ^ polynomial;
							}
						}
						numArray1[i] = num;
					}
					if (polynomial == -306674912)
					{
						MSCryptographyHelper.Crc32.defaultTable = numArray1;
					}
					numArray = numArray1;
				}
				else
				{
					numArray = MSCryptographyHelper.Crc32.defaultTable;
				}
				return numArray;
			}

			private static byte[] UInt32ToBigEndianBytes(uint uint32)
			{
				byte[] bytes = BitConverter.GetBytes(uint32);
				if (BitConverter.IsLittleEndian)
				{
					Array.Reverse(bytes);
				}
				return bytes;
			}
		}
	}
}