using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security;
using System.Security.Cryptography;
using System.Text;

namespace MSClassLibrary.Security
{
	public class MSAESEncryption
	{
		public MSAESEncryption()
		{
		}

		public static string ConvertToRawString(SecureString ss)
		{
			string stringUni;
			IntPtr zero = IntPtr.Zero;
			try
			{
				zero = Marshal.SecureStringToGlobalAllocUnicode(ss);
				stringUni = Marshal.PtrToStringUni(zero);
			}
			finally
			{
				Marshal.ZeroFreeGlobalAllocUnicode(zero);
			}
			return stringUni;
		}

		public static byte[] DecryptBytes(byte[] bytesToBeDecrypted, byte[] passwordBytes)
		{
			byte[] array = null;
			byte[] numArray = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6 };
			using (MemoryStream memoryStream = new MemoryStream())
			{
				using (RijndaelManaged rijndaelManaged = new RijndaelManaged())
				{
					rijndaelManaged.KeySize = 256;
					rijndaelManaged.BlockSize = 128;
					Rfc2898DeriveBytes rfc2898DeriveByte = new Rfc2898DeriveBytes(passwordBytes, numArray, 1000);
					rijndaelManaged.Key = rfc2898DeriveByte.GetBytes(rijndaelManaged.KeySize / 8);
					rijndaelManaged.IV = rfc2898DeriveByte.GetBytes(rijndaelManaged.BlockSize / 8);
					rijndaelManaged.Mode = CipherMode.CBC;
					using (CryptoStream cryptoStream = new CryptoStream(memoryStream, rijndaelManaged.CreateDecryptor(), CryptoStreamMode.Write))
					{
						cryptoStream.Write(bytesToBeDecrypted, 0, (int)bytesToBeDecrypted.Length);
						cryptoStream.Close();
					}
					array = memoryStream.ToArray();
				}
			}
			return array;
		}

		public static void DecryptFile(string fileNameWithFullPath, string key)
		{
			if (File.Exists(fileNameWithFullPath))
			{
				byte[] numArray = File.ReadAllBytes(fileNameWithFullPath);
				byte[] bytes = Encoding.UTF8.GetBytes(key);
				bytes = SHA256.Create().ComputeHash(bytes);
				File.WriteAllBytes(fileNameWithFullPath, MSAESEncryption.DecryptBytes(numArray, bytes));
			}
		}

		public static T DecryptObject<T>(string fileNameWithFullPath, T obj, string password)
		{
			T t = obj;
			if (File.Exists(fileNameWithFullPath))
			{
				byte[] bytes = Encoding.UTF8.GetBytes(password);
				byte[] numArray = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6 };
				using (FileStream fileStream = new FileStream(fileNameWithFullPath, FileMode.Open, FileAccess.Read))
				{
					using (RijndaelManaged rijndaelManaged = new RijndaelManaged())
					{
						rijndaelManaged.KeySize = 256;
						rijndaelManaged.BlockSize = 128;
						Rfc2898DeriveBytes rfc2898DeriveByte = new Rfc2898DeriveBytes(bytes, numArray, 1000);
						rijndaelManaged.Key = rfc2898DeriveByte.GetBytes(rijndaelManaged.KeySize / 8);
						rijndaelManaged.IV = rfc2898DeriveByte.GetBytes(rijndaelManaged.BlockSize / 8);
						rijndaelManaged.Mode = CipherMode.CBC;
						using (CryptoStream cryptoStream = new CryptoStream(fileStream, rijndaelManaged.CreateDecryptor(), CryptoStreamMode.Read))
						{
							t = (T)(new BinaryFormatter()).Deserialize(cryptoStream);
							cryptoStream.Close();
						}
					}
				}
			}
			return t;
		}

		public static string DecryptText(string input, string password)
		{
			byte[] numArray = Convert.FromBase64String(input);
			byte[] bytes = Encoding.UTF8.GetBytes(password);
			bytes = SHA256.Create().ComputeHash(bytes);
			byte[] numArray1 = MSAESEncryption.DecryptBytes(numArray, bytes);
			return Encoding.UTF8.GetString(numArray1);
		}

		public static byte[] EncryptBytes(byte[] bytesToBeEncrypted, byte[] passwordBytes)
		{
			byte[] array = null;
			byte[] numArray = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6 };
			using (MemoryStream memoryStream = new MemoryStream())
			{
				using (RijndaelManaged rijndaelManaged = new RijndaelManaged())
				{
					rijndaelManaged.KeySize = 256;
					rijndaelManaged.BlockSize = 128;
					Rfc2898DeriveBytes rfc2898DeriveByte = new Rfc2898DeriveBytes(passwordBytes, numArray, 1000);
					rijndaelManaged.Key = rfc2898DeriveByte.GetBytes(rijndaelManaged.KeySize / 8);
					rijndaelManaged.IV = rfc2898DeriveByte.GetBytes(rijndaelManaged.BlockSize / 8);
					rijndaelManaged.Mode = CipherMode.CBC;
					using (CryptoStream cryptoStream = new CryptoStream(memoryStream, rijndaelManaged.CreateEncryptor(), CryptoStreamMode.Write))
					{
						cryptoStream.Write(bytesToBeEncrypted, 0, (int)bytesToBeEncrypted.Length);
						cryptoStream.Close();
					}
					array = memoryStream.ToArray();
				}
			}
			return array;
		}

		public static void EncryptFile(string fileNameWithFullPath, string key)
		{
			if (File.Exists(fileNameWithFullPath))
			{
				byte[] numArray = File.ReadAllBytes(fileNameWithFullPath);
				byte[] bytes = Encoding.UTF8.GetBytes(key);
				bytes = SHA256.Create().ComputeHash(bytes);
				File.WriteAllBytes(fileNameWithFullPath, MSAESEncryption.EncryptBytes(numArray, bytes));
			}
		}

		public static void EncryptObject<T>(string fileNameWithFullPath, T obj, string password)
		{
			byte[] array = null;
			byte[] bytes = Encoding.UTF8.GetBytes(password);
			byte[] numArray = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6 };
			using (MemoryStream memoryStream = new MemoryStream())
			{
				using (RijndaelManaged rijndaelManaged = new RijndaelManaged())
				{
					rijndaelManaged.KeySize = 256;
					rijndaelManaged.BlockSize = 128;
					Rfc2898DeriveBytes rfc2898DeriveByte = new Rfc2898DeriveBytes(bytes, numArray, 1000);
					rijndaelManaged.Key = rfc2898DeriveByte.GetBytes(rijndaelManaged.KeySize / 8);
					rijndaelManaged.IV = rfc2898DeriveByte.GetBytes(rijndaelManaged.BlockSize / 8);
					rijndaelManaged.Mode = CipherMode.CBC;
					using (CryptoStream cryptoStream = new CryptoStream(memoryStream, rijndaelManaged.CreateEncryptor(), CryptoStreamMode.Write))
					{
						(new BinaryFormatter()).Serialize(cryptoStream, obj);
						cryptoStream.Close();
					}
					array = memoryStream.ToArray();
				}
				File.WriteAllBytes(fileNameWithFullPath, array);
			}
		}

		public static string EncryptText(string input, string password)
		{
			byte[] bytes = Encoding.UTF8.GetBytes(input);
			byte[] numArray = Encoding.UTF8.GetBytes(password);
			numArray = SHA256.Create().ComputeHash(numArray);
			return Convert.ToBase64String(MSAESEncryption.EncryptBytes(bytes, numArray));
		}
	}
}