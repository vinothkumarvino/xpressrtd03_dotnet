using MSClassLibrary.Common;
using System;
using System.Security.Cryptography;
using System.Text;

namespace MSClassLibrary.Security
{
	public class MSRSACrypto
	{
		public MSRSACrypto()
		{
		}

		public static Tuple<string, string> CreateKeyPair()
		{
			string empty = string.Empty;
			string base64String = string.Empty;
			using (RSACryptoServiceProvider rSACryptoServiceProvider = new RSACryptoServiceProvider(1024, new CspParameters()
			{
				ProviderType = 1
			}))
			{
				empty = Convert.ToBase64String(rSACryptoServiceProvider.ExportCspBlob(false));
				base64String = Convert.ToBase64String(rSACryptoServiceProvider.ExportCspBlob(true));
			}
			return new Tuple<string, string>(base64String, empty);
		}

		public static string PrivateKeyDecrypt(string privateKey, string encodedString)
		{
			string empty;
			if ((string.IsNullOrEmpty(privateKey) ? false : !string.IsNullOrEmpty(encodedString)))
			{
				string str = string.Empty;
				byte[] numArray = Convert.FromBase64String(encodedString);
				CspParameters cspParameter = new CspParameters()
				{
					ProviderType = 1,
					KeyPassword = SysGlobal.MARAICHOL
				};
				using (RSACryptoServiceProvider rSACryptoServiceProvider = new RSACryptoServiceProvider(cspParameter))
				{
					rSACryptoServiceProvider.ImportCspBlob(Convert.FromBase64String(privateKey));
					byte[] numArray1 = rSACryptoServiceProvider.Decrypt(numArray, false);
					str = Encoding.UTF8.GetString(numArray1, 0, (int)numArray1.Length);
				}
				empty = str;
			}
			else
			{
				empty = string.Empty;
			}
			return empty;
		}

		public static string PublicKeyEncrypt(string publicKey, string rawText)
		{
			byte[] numArray;
			string base64String;
			if ((string.IsNullOrEmpty(publicKey) ? false : !string.IsNullOrEmpty(rawText)))
			{
				CspParameters cspParameter = new CspParameters()
				{
					ProviderType = 1,
					KeyPassword = SysGlobal.MARAICHOL
				};
				using (RSACryptoServiceProvider rSACryptoServiceProvider = new RSACryptoServiceProvider(cspParameter))
				{
					rSACryptoServiceProvider.ImportCspBlob(Convert.FromBase64String(publicKey));
					byte[] bytes = Encoding.UTF8.GetBytes(rawText);
					numArray = rSACryptoServiceProvider.Encrypt(bytes, false);
				}
				base64String = Convert.ToBase64String(numArray);
			}
			else
			{
				base64String = string.Empty;
			}
			return base64String;
		}
	}
}