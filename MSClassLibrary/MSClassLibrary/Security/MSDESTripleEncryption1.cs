using System;
using System.Security.Cryptography;
using System.Text;

namespace MSClassLibrary.Security
{
	public class MSDESTripleEncryption1
	{
		public MSDESTripleEncryption1()
		{
		}

		public static string Decrypt(string encData, string key, bool useHashing)
		{
			string empty;
			byte[] bytes;
			if (string.IsNullOrEmpty(encData))
			{
				empty = string.Empty;
			}
			else if (!string.IsNullOrEmpty(key))
			{
				try
				{
					byte[] numArray = Convert.FromBase64String(encData);
					if (!useHashing)
					{
						bytes = Encoding.UTF8.GetBytes(key);
					}
					else
					{
						using (MD5CryptoServiceProvider mD5CryptoServiceProvider = new MD5CryptoServiceProvider())
						{
							bytes = mD5CryptoServiceProvider.ComputeHash(Encoding.UTF8.GetBytes(key));
							mD5CryptoServiceProvider.Clear();
						}
					}
					byte[] numArray1 = null;
					using (TripleDESCryptoServiceProvider tripleDESCryptoServiceProvider = new TripleDESCryptoServiceProvider()
					{
						KeySize = 128,
						Mode = CipherMode.ECB,
						Padding = PaddingMode.PKCS7
					})
					{
						tripleDESCryptoServiceProvider.Key = bytes;
						ICryptoTransform cryptoTransform = tripleDESCryptoServiceProvider.CreateDecryptor();
						numArray1 = cryptoTransform.TransformFinalBlock(numArray, 0, (int)numArray.Length);
						tripleDESCryptoServiceProvider.Clear();
					}
					empty = Encoding.UTF8.GetString(numArray1);
				}
				catch
				{
					throw;
				}
			}
			else
			{
				empty = encData;
			}
			return empty;
		}

		public static string Encrypt(string rawData, string key, bool useHashing)
		{
			string empty;
			byte[] bytes;
			if (string.IsNullOrEmpty(rawData))
			{
				empty = string.Empty;
			}
			else if (!string.IsNullOrEmpty(key))
			{
				try
				{
					byte[] numArray = Encoding.UTF8.GetBytes(rawData);
					if (!useHashing)
					{
						bytes = Encoding.UTF8.GetBytes(key);
					}
					else
					{
						using (MD5CryptoServiceProvider mD5CryptoServiceProvider = new MD5CryptoServiceProvider())
						{
							bytes = mD5CryptoServiceProvider.ComputeHash(Encoding.UTF8.GetBytes(key));
							mD5CryptoServiceProvider.Clear();
						}
					}
					byte[] numArray1 = null;
					using (TripleDESCryptoServiceProvider tripleDESCryptoServiceProvider = new TripleDESCryptoServiceProvider()
					{
						KeySize = 128,
						Mode = CipherMode.ECB,
						Padding = PaddingMode.PKCS7
					})
					{
						tripleDESCryptoServiceProvider.Key = bytes;
						ICryptoTransform cryptoTransform = tripleDESCryptoServiceProvider.CreateEncryptor();
						numArray1 = cryptoTransform.TransformFinalBlock(numArray, 0, (int)numArray.Length);
						tripleDESCryptoServiceProvider.Clear();
					}
					empty = Convert.ToBase64String(numArray1, 0, (int)numArray1.Length);
				}
				catch
				{
					throw;
				}
			}
			else
			{
				empty = rawData;
			}
			return empty;
		}
	}
}