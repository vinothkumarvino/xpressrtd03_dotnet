using System;
using System.Security.Cryptography;
using System.Text;

namespace MSClassLibrary.Security
{
	public class MSMD5Hash
	{
		public MSMD5Hash()
		{
		}

		public static string GetHash(string rawData, bool hexOut)
		{
			string str;
			if (!string.IsNullOrEmpty(rawData))
			{
				StringBuilder stringBuilder = new StringBuilder();
				using (MD5 mD5 = MD5.Create())
				{
					byte[] numArray = mD5.ComputeHash(Encoding.UTF8.GetBytes(rawData));
					if (!hexOut)
					{
						for (int i = 0; i < (int)numArray.Length; i++)
						{
							stringBuilder.Append(numArray[i]);
						}
					}
					else
					{
						for (int j = 0; j < (int)numArray.Length; j++)
						{
							stringBuilder.Append(numArray[j].ToString("X2"));
						}
					}
				}
				str = stringBuilder.ToString();
			}
			else
			{
				str = string.Empty;
			}
			return str;
		}
	}
}