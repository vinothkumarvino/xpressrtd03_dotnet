using System;
using System.Security.Cryptography;
using System.Text;

namespace MSClassLibrary.Security
{
	public class MSDESTriple
	{
		public MSDESTriple()
		{
		}

		public static string Decrypt(string encData, string key, bool useHashing)
		{
			byte[] bytes;
			byte[] numArray = Convert.FromBase64String(encData);
			if (!useHashing)
			{
				bytes = Encoding.UTF8.GetBytes(key);
			}
			else
			{
				MD5CryptoServiceProvider mD5CryptoServiceProvider = new MD5CryptoServiceProvider();
				bytes = mD5CryptoServiceProvider.ComputeHash(Encoding.UTF8.GetBytes(key));
				mD5CryptoServiceProvider.Clear();
			}
			TripleDESCryptoServiceProvider tripleDESCryptoServiceProvider = new TripleDESCryptoServiceProvider()
			{
				Key = bytes,
				Mode = CipherMode.ECB,
				Padding = PaddingMode.PKCS7
			};
			ICryptoTransform cryptoTransform = tripleDESCryptoServiceProvider.CreateDecryptor();
			byte[] numArray1 = cryptoTransform.TransformFinalBlock(numArray, 0, (int)numArray.Length);
			tripleDESCryptoServiceProvider.Clear();
			return Encoding.UTF8.GetString(numArray1);
		}

		public static string Encrypt(string rawData, string key, bool useHashing)
		{
			byte[] bytes;
			byte[] numArray = Encoding.UTF8.GetBytes(rawData);
			if (!useHashing)
			{
				bytes = Encoding.UTF8.GetBytes(key);
			}
			else
			{
				MD5CryptoServiceProvider mD5CryptoServiceProvider = new MD5CryptoServiceProvider();
				bytes = mD5CryptoServiceProvider.ComputeHash(Encoding.UTF8.GetBytes(key));
				mD5CryptoServiceProvider.Clear();
			}
			TripleDESCryptoServiceProvider tripleDESCryptoServiceProvider = new TripleDESCryptoServiceProvider()
			{
				Key = bytes,
				Mode = CipherMode.ECB,
				Padding = PaddingMode.PKCS7
			};
			ICryptoTransform cryptoTransform = tripleDESCryptoServiceProvider.CreateEncryptor();
			byte[] numArray1 = cryptoTransform.TransformFinalBlock(numArray, 0, (int)numArray.Length);
			tripleDESCryptoServiceProvider.Clear();
			return Convert.ToBase64String(numArray1, 0, (int)numArray1.Length);
		}
	}
}