using System;

namespace MSClassLibrary.Log
{
	public enum MSLogLevel
	{
		EMERGENCY,
		ALERT,
		CRITICAL,
		ERROR,
		WARNING,
		NOTICE,
		INFORMATIONAL,
		DEBUG,
		INPUT,
		OUTPUT
	}
}