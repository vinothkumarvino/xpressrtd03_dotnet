using System;

namespace MSClassLibrary.Log
{
	public static class SysLog
	{
		private readonly static object monitor;

		public readonly static string LOGQNAME;

		public static bool QLOGGING;

		public static bool NETLOGGING;

		public static string SERVICE_NAME;

		public static string NET_LOG_SERVER_IP;

		public static int NET_LOG_SERVER_PORT;

		static SysLog()
		{
			SysLog.monitor = new object();
			SysLog.LOGQNAME = string.Format(".\\Private$\\{0}.{1}", "MSLogAgent", "MSLogger");
			SysLog.QLOGGING = false;
			SysLog.NETLOGGING = false;
			SysLog.SERVICE_NAME = "";
			SysLog.NET_LOG_SERVER_IP = "127.0.0.1";
			SysLog.NET_LOG_SERVER_PORT = 514;
		}

		public static void WriteEvent(string eventSource, string eventMessage)
		{
			lock (SysLog.monitor)
			{
				using (MSEventLogger mSEventLogger = new MSEventLogger())
				{
					mSEventLogger.Log(eventSource, eventMessage);
				}
			}
		}

		public static void WriteHL7Log(string log)
		{
			lock (SysLog.monitor)
			{
				using (MSHL7FileLogger mSHL7FileLogger = new MSHL7FileLogger())
				{
					mSHL7FileLogger.Log(log);
				}
			}
		}

		public static void WriteLog(MSLogLevel logLevel, string log)
		{
			lock (SysLog.monitor)
			{
				using (MSFileLogger mSFileLogger = new MSFileLogger())
				{
					mSFileLogger.Log(logLevel, log, SysLog.SERVICE_NAME);
				}
				if (SysLog.QLOGGING)
				{
					using (MSQueueLogger mSQueueLogger = new MSQueueLogger())
					{
						mSQueueLogger.Log(logLevel, log, SysLog.SERVICE_NAME);
					}
				}
				if ((!SysLog.NETLOGGING ? false : !string.IsNullOrEmpty(SysLog.NET_LOG_SERVER_IP)))
				{
					using (MSNetLogger mSNetLogger = new MSNetLogger())
					{
						mSNetLogger.Log(logLevel, log, SysLog.SERVICE_NAME);
					}
				}
			}
		}

		public static void WriteLog(string activePointer)
		{
			lock (SysLog.monitor)
			{
				using (MSFileLogger mSFileLogger = new MSFileLogger())
				{
					mSFileLogger.Log(activePointer, SysLog.SERVICE_NAME);
				}
			}
		}
	}
}