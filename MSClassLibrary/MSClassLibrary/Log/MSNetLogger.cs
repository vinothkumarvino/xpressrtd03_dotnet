using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Sockets;
using System.Reflection;
using System.Text;

namespace MSClassLibrary.Log
{
	internal class MSNetLogger : MSLogBase
	{
		public MSNetLogger()
		{
		}

		public void Log(MSLogLevel logLevel, string logMessage, string serviceName)
		{
			if (!string.IsNullOrEmpty(logMessage))
			{
				StackTrace stackTrace = new StackTrace();
				string empty = string.Empty;
				string name = string.Empty;
				if (stackTrace.FrameCount <= 1)
				{
					empty = string.Concat(stackTrace.GetFrame(1).GetMethod().DeclaringType.Namespace, ".", stackTrace.GetFrame(1).GetMethod().DeclaringType.Name);
					name = stackTrace.GetFrame(1).GetMethod().Name;
				}
				else
				{
					empty = string.Concat(stackTrace.GetFrame(2).GetMethod().DeclaringType.Namespace, ".", stackTrace.GetFrame(2).GetMethod().DeclaringType.Name);
					name = stackTrace.GetFrame(2).GetMethod().Name;
				}
				string str = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");
				this.LogToNet(str, logLevel, logMessage, empty, name, serviceName);
			}
		}

		private void LogToNet(string logTimeStamp, MSLogLevel logLevel, string logMessage, string callingClassName, string callingMethodName, string serviceName)
		{
			try
			{
				List<string> strs = new List<string>()
				{
					logTimeStamp,
					Environment.MachineName,
					Environment.UserName,
					serviceName,
					logLevel.ToString(),
					string.Format("{0}.{1}", callingClassName, callingMethodName),
					logMessage
				};
				string str = string.Join('\u001F'.ToString(), strs);
				using (UdpClient udpClient = new UdpClient(SysLog.NET_LOG_SERVER_IP, SysLog.NET_LOG_SERVER_PORT))
				{
					byte[] bytes = Encoding.ASCII.GetBytes(str);
					udpClient.Send(bytes, (int)bytes.Length);
					udpClient.Close();
				}
			}
			catch
			{
			}
		}
	}
}