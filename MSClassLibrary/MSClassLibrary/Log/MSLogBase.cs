using System;

namespace MSClassLibrary.Log
{
	public abstract class MSLogBase : IDisposable
	{
		private bool disposed = false;

		protected MSLogBase()
		{
		}

		public void Dispose()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		protected virtual void Dispose(bool disposing)
		{
			if (!this.disposed)
			{
				if (disposing)
				{
				}
				this.disposed = true;
			}
		}

		~MSLogBase()
		{
			this.Dispose(false);
		}
	}
}