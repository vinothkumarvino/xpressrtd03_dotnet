using System;
using System.Diagnostics;

namespace MSClassLibrary.Log
{
	internal class MSEventLogger : MSLogBase
	{
		public MSEventLogger()
		{
		}

		public void Log(string eventSource, string eventMessage)
		{
			if (!EventLog.SourceExists(eventSource))
			{
				EventLog.CreateEventSource(eventSource, "Application");
			}
			using (EventLog eventLog = new EventLog("Application", Environment.MachineName, eventSource))
			{
				eventLog.WriteEntry(eventMessage);
			}
		}
	}
}