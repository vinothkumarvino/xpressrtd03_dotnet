using MSClassLibrary.Common;
using System;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace MSClassLibrary.Log
{
	internal class MSHL7FileLogger : MSLogBase
	{
		public MSHL7FileLogger()
		{
		}

		public void Log(string logMessage)
		{
			if (!string.IsNullOrEmpty(logMessage))
			{
				string str = Process.GetCurrentProcess().ProcessName.ToLower().Replace(".", "");
				DateTime now = DateTime.Now;
				string str1 = string.Format("oh_hl7_{0}_{1}.log", str, now.ToString("yyyyMMdd"));
				string str2 = Path.Combine(SysGlobal.DIR_LOG, str1);
				if (!Directory.Exists(SysGlobal.DIR_LOG))
				{
					Directory.CreateDirectory(SysGlobal.DIR_LOG);
				}
				object[] newLine = new object[5];
				now = DateTime.Now;
				newLine[0] = now.ToString("yyyy-MM-dd HH:mm:ss.fff");
				newLine[1] = Environment.NewLine;
				newLine[2] = logMessage;
				newLine[3] = Environment.NewLine;
				newLine[4] = Environment.NewLine;
				logMessage = string.Format("{0}{1}{2}{3}{4}", newLine);
				try
				{
					using (FileStream fileStream = File.Open(str2, FileMode.Append, FileAccess.Write, FileShare.Read))
					{
						fileStream.Write(Encoding.ASCII.GetBytes(logMessage), 0, logMessage.Length);
						fileStream.Close();
					}
				}
				catch
				{
				}
			}
		}
	}
}