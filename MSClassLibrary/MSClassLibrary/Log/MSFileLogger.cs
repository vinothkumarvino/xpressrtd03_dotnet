using MSClassLibrary.Common;
using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Text;

namespace MSClassLibrary.Log
{
	internal class MSFileLogger : MSLogBase
	{
		public MSFileLogger()
		{
		}

		public void Log(MSLogLevel logLevel, string logMessage, string serviceName = default(string))
		{
			if (!string.IsNullOrEmpty(logMessage))
			{
				string str = Process.GetCurrentProcess().ProcessName.Replace(".", "");
				DateTime now = DateTime.Now;
				string str1 = string.Format("OHTX_{0}_{1}.log", str, now.ToString("yyyyMMdd"));
				serviceName = serviceName ?? string.Empty;
				string empty = string.Empty;
				empty = (string.IsNullOrEmpty(serviceName) ? SysGlobal.DIR_LOG : Path.Combine(SysGlobal.DIR_LOG, serviceName));
				if (!Directory.Exists(empty))
				{
					Directory.CreateDirectory(empty);
				}
				string str2 = Path.Combine(empty, str1);
				StackTrace stackTrace = new StackTrace();
				string empty1 = string.Empty;
				string name = string.Empty;
				if (stackTrace.FrameCount <= 1)
				{
					empty1 = string.Concat(stackTrace.GetFrame(1).GetMethod().DeclaringType.Namespace, ".", stackTrace.GetFrame(1).GetMethod().DeclaringType.Name);
					name = stackTrace.GetFrame(1).GetMethod().Name;
				}
				else
				{
					empty1 = string.Concat(stackTrace.GetFrame(2).GetMethod().DeclaringType.Namespace, ".", stackTrace.GetFrame(2).GetMethod().DeclaringType.Name);
					name = stackTrace.GetFrame(2).GetMethod().Name;
				}
				if ((logLevel == MSLogLevel.WARNING || logLevel == MSLogLevel.CRITICAL || logLevel == MSLogLevel.EMERGENCY || logLevel == MSLogLevel.ALERT ? true : logLevel == MSLogLevel.ERROR))
				{
					Console.BackgroundColor = ConsoleColor.Black;
					Console.ForegroundColor = ConsoleColor.Red;
				}
				else if (logLevel == MSLogLevel.DEBUG)
				{
					Console.BackgroundColor = ConsoleColor.Black;
					Console.ForegroundColor = ConsoleColor.Cyan;
				}
				else if ((logLevel == MSLogLevel.INPUT ? false : logLevel != MSLogLevel.OUTPUT))
				{
					Console.BackgroundColor = ConsoleColor.Black;
					Console.ForegroundColor = ConsoleColor.Green;
				}
				else
				{
					Console.BackgroundColor = ConsoleColor.Black;
					Console.ForegroundColor = ConsoleColor.Yellow;
				}
				Console.WriteLine(logMessage);
				string str3 = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");
				string str4 = string.Format("{0}, {1}, [{2}].[{3}], {4}{5}", new object[] { str3, logLevel.ToString(), empty1, name, logMessage, Environment.NewLine });
				try
				{
					using (FileStream fileStream = File.Open(str2, FileMode.Append, FileAccess.Write, FileShare.Read))
					{
						fileStream.Write(Encoding.ASCII.GetBytes(str4), 0, str4.Length);
						fileStream.Close();
					}
				}
				catch
				{
				}
				if (logLevel == MSLogLevel.INPUT)
				{
					string str5 = Process.GetCurrentProcess().ProcessName.Replace(".", "");
					now = DateTime.Now;
					str1 = string.Format("OHIP_{0}_{1}.log", str5, now.ToString("yyyyMMdd"));
					str2 = Path.Combine(empty, str1);
					try
					{
						using (FileStream fileStream1 = File.Open(str2, FileMode.Append, FileAccess.Write, FileShare.Read))
						{
							fileStream1.Write(Encoding.ASCII.GetBytes(str4), 0, str4.Length);
							fileStream1.Close();
						}
					}
					catch
					{
					}
				}
				else if (logLevel == MSLogLevel.OUTPUT)
				{
					string str6 = Process.GetCurrentProcess().ProcessName.Replace(".", "");
					now = DateTime.Now;
					str1 = string.Format("OHOP_{0}_{1}.log", str6, now.ToString("yyyyMMdd"));
					str2 = Path.Combine(empty, str1);
					try
					{
						using (FileStream fileStream2 = File.Open(str2, FileMode.Append, FileAccess.Write, FileShare.Read))
						{
							fileStream2.Write(Encoding.ASCII.GetBytes(str4), 0, str4.Length);
							fileStream2.Close();
						}
					}
					catch
					{
					}
				}
			}
		}

		public void Log(string logMessage, string serviceName = default(string))
		{
			if (!string.IsNullOrEmpty(logMessage))
			{
				string str = Process.GetCurrentProcess().ProcessName.ToLower().Replace(".", "");
				DateTime now = DateTime.Now;
				string str1 = string.Format("oh_{0}_{1}.log", str, now.ToString("yyyyMMdd"));
				serviceName = serviceName ?? string.Empty;
				string empty = string.Empty;
				empty = (string.IsNullOrEmpty(serviceName) ? SysGlobal.DIR_LOG : Path.Combine(SysGlobal.DIR_LOG, serviceName));
				if (!Directory.Exists(empty))
				{
					Directory.CreateDirectory(empty);
				}
				string str2 = Path.Combine(empty, str1);
				Console.BackgroundColor = ConsoleColor.Black;
				Console.ForegroundColor = ConsoleColor.Cyan;
				Console.WriteLine(logMessage);
				try
				{
					using (FileStream fileStream = File.Open(str2, FileMode.Append, FileAccess.Write, FileShare.Read))
					{
						fileStream.Write(Encoding.ASCII.GetBytes(logMessage), 0, logMessage.Length);
						fileStream.Close();
					}
				}
				catch
				{
				}
			}
		}
	}
}