using System;
using System.Diagnostics;
using System.Messaging;
using System.Reflection;
using System.Threading;

namespace MSClassLibrary.Log
{
	internal class MSQueueLogger : MSLogBase
	{
		public MSQueueLogger()
		{
		}

		public void Log(MSLogLevel logLevel, string logMessage, string serviceName)
		{
			if (!string.IsNullOrEmpty(logMessage))
			{
				StackTrace stackTrace = new StackTrace();
				string empty = string.Empty;
				string name = string.Empty;
				if (stackTrace.FrameCount <= 1)
				{
					empty = string.Concat(stackTrace.GetFrame(1).GetMethod().DeclaringType.Namespace, ".", stackTrace.GetFrame(1).GetMethod().DeclaringType.Name);
					name = stackTrace.GetFrame(1).GetMethod().Name;
				}
				else
				{
					empty = string.Concat(stackTrace.GetFrame(2).GetMethod().DeclaringType.Namespace, ".", stackTrace.GetFrame(2).GetMethod().DeclaringType.Name);
					name = stackTrace.GetFrame(2).GetMethod().Name;
				}
				string str = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");
				this.LogToQ(str, logLevel, logMessage, empty, name, serviceName);
			}
		}

		private void LogToQ(string logTimeStamp, MSLogLevel logLevel, string logMessage, string callingClassName, string callingMethodName, string serviceName)
		{
			try
			{
				logMessage = string.Format("{0}|{1}|{2}|{3}|{4}.{5}|{6}", new object[] { logTimeStamp, Environment.MachineName, Environment.UserName, serviceName, logLevel.ToString(), callingClassName, callingMethodName, logMessage });
				if (!MessageQueue.Exists(SysLog.LOGQNAME))
				{
					using (MessageQueue messageQueues = MessageQueue.Create(SysLog.LOGQNAME))
					{
						messageQueues.Label = string.Format("MSLogAgent->MSLogger", new object[0]);
						messageQueues.SetPermissions("Everyone", MessageQueueAccessRights.FullControl, AccessControlEntryType.Allow);
						messageQueues.Close();
						Thread.Sleep(1000);
					}
				}
				using (MessageQueue messageQueues1 = new MessageQueue(SysLog.LOGQNAME))
				{
					using (Message message = new Message(logMessage, new BinaryMessageFormatter()))
					{
						DateTime now = DateTime.Now;
						message.Label = string.Format("M{0}S", now.ToString("yyyyMMddHHmmssfff"));
						message.Priority = MessagePriority.Normal;
						messageQueues1.Send(message);
						messageQueues1.Close();
					}
				}
			}
			catch
			{
			}
		}
	}
}