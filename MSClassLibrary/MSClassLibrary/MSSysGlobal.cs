using System;
using System.Security;

namespace MSClassLibrary
{
	public static class MSSysGlobal
	{
		public const string LOG_DIRECTORY_NAME = "C:\\OMNIHEALTH\\LOG\\";

		public readonly static string MARAICHOL;

		private static SecureString ss;

		public static string MSMaraiChol
		{
			get
			{
				return MSSysGlobal.ss.ToString();
			}
		}

		static MSSysGlobal()
		{
			MSSysGlobal.MARAICHOL = "O M N I 1 2 3 ! @ #";
			MSSysGlobal.ss = new SecureString();
			MSSysGlobal.ss.AppendChar('m');
			MSSysGlobal.ss.AppendChar('a');
			MSSysGlobal.ss.AppendChar('d');
			MSSysGlobal.ss.AppendChar('h');
			MSSysGlobal.ss.AppendChar('a');
			MSSysGlobal.ss.AppendChar('v');
			MSSysGlobal.ss.AppendChar('a');
			MSSysGlobal.ss.AppendChar('n');
		}
	}
}