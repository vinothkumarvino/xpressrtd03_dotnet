using MSClassLibrary.Log;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Data.OleDb;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Net.Mail;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Linq;

namespace MSClassLibrary
{
	public static class MSSysUtilities
	{
		public static Image ByteArrayToImage(byte[] byteArrayIn)
		{
			return Image.FromStream(new MemoryStream(byteArrayIn));
		}

		public static double CalculateAge(DateTime dateOfBirth, DateTime calculationDate)
		{
			bool flag;
			int year = calculationDate.Year - dateOfBirth.Year;
			if (calculationDate.Month < dateOfBirth.Month)
			{
				flag = true;
			}
			else
			{
				flag = (calculationDate.Month != dateOfBirth.Month ? false : calculationDate.Day < dateOfBirth.Day);
			}
			if (flag)
			{
				year--;
			}
			return (double)year;
		}

		public static string ConvertHexStringToNormal(string hexData)
		{
			StringBuilder stringBuilder = new StringBuilder();
			if (!string.IsNullOrEmpty(hexData))
			{
				byte[] num = new byte[hexData.Length / 2];
				for (int i = 0; i < (int)num.Length; i++)
				{
					num[i] = Convert.ToByte(hexData.Substring(i * 2, 2), 16);
				}
				stringBuilder.Append((new ASCIIEncoding()).GetString(num));
			}
			return stringBuilder.ToString();
		}

		public static string ConvertNormalStringToHex(string stringData)
		{
			StringBuilder stringBuilder = new StringBuilder();
			if (!string.IsNullOrEmpty(stringData))
			{
				byte[] bytes = (new ASCIIEncoding()).GetBytes(stringData);
				stringBuilder.Append(BitConverter.ToString(bytes).Replace("-", ""));
			}
			return stringBuilder.ToString();
		}

		public static bool ConvertXLSToDataTable(string filePath, ref DataTable dataTable)
		{
			bool flag;
			if (filePath.Trim().EndsWith(".xls"))
			{
				string str = string.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=1\";", filePath);
				OleDbConnection oleDbConnection = null;
				OleDbCommand oleDbCommand = null;
				OleDbDataAdapter oleDbDataAdapter = null;
				dataTable = new DataTable();
				try
				{
					try
					{
						OleDbConnection oleDbConnection1 = new OleDbConnection(str);
						oleDbConnection = oleDbConnection1;
						using (oleDbConnection1)
						{
							oleDbConnection.Open();
							OleDbCommand oleDbCommand1 = new OleDbCommand("SELECT * FROM [Sheet1$]", oleDbConnection);
							oleDbCommand = oleDbCommand1;
							using (oleDbCommand1)
							{
								oleDbCommand.CommandType = CommandType.Text;
								oleDbDataAdapter = new OleDbDataAdapter(oleDbCommand);
								oleDbDataAdapter.Fill(dataTable);
							}
						}
					}
					catch (Exception exception)
					{
						SysLog.WriteLog(MSLogLevel.ERROR, exception.Message);
					}
				}
				finally
				{
					if (oleDbConnection.State == ConnectionState.Open)
					{
						oleDbConnection.Close();
					}
					oleDbConnection.Dispose();
					oleDbDataAdapter.Dispose();
				}
				flag = true;
			}
			else
			{
				flag = false;
			}
			return flag;
		}

		public static int CountDayOfWeek(DayOfWeek dow, DateTime startDate, DateTime endDate)
		{
			int num = 0;
			for (DateTime i = startDate; i < endDate.AddDays(1); i = i.AddDays(1))
			{
				if (i.DayOfWeek == dow)
				{
					num++;
				}
			}
			return num;
		}

		public static IEnumerable<T> EnumToList<T>()
		{
			Type type = typeof(T);
			if (type.BaseType != typeof(Enum))
			{
				throw new ArgumentException("T must be of type System.Enum");
			}
			Array values = Enum.GetValues(type);
			List<T> ts = new List<T>(values.Length);
			foreach (int value in values)
			{
				ts.Add((T)Enum.Parse(type, value.ToString()));
			}
			return ts;
		}

		public static string FormatXml(string xml)
		{
			string str;
			try
			{
				str = XDocument.Parse(xml).ToString();
			}
			catch (Exception exception)
			{
				str = xml;
			}
			return str;
		}

		public static string GetEnumDescription(Enum value)
		{
			string str;
			FieldInfo field = value.GetType().GetField(value.ToString());
			DescriptionAttribute[] customAttributes = (DescriptionAttribute[])field.GetCustomAttributes(typeof(DescriptionAttribute), false);
			str = ((customAttributes == null ? true : customAttributes.Length == 0) ? value.ToString() : customAttributes[0].Description);
			return str;
		}

		public static string GetID()
		{
			return Guid.NewGuid().ToString();
		}

		public static string GetLabel(string keyIdentifier)
		{
			DateTime now;
			string str;
			if (string.IsNullOrEmpty(keyIdentifier))
			{
				now = DateTime.Now;
				str = string.Format("M{0}", now.ToString("yyyyMMddHHmmssfff"));
			}
			else
			{
				now = DateTime.Now;
				str = string.Format("M{1}{0}", now.ToString("yyyyMMddHHmmssfff"), keyIdentifier);
			}
			return str;
		}

		public static Image GrayScale(Bitmap Bmp)
		{
			for (int i = 0; i < Bmp.Height; i++)
			{
				for (int j = 0; j < Bmp.Width; j++)
				{
					Color pixel = Bmp.GetPixel(j, i);
					int r = (pixel.R + pixel.G + pixel.B) / 3;
					Bmp.SetPixel(j, i, Color.FromArgb(r, r, r));
				}
			}
			return Bmp;
		}

		public static byte[] ImageToByteArray(Image imageIn)
		{
			MemoryStream memoryStream = new MemoryStream();
			imageIn.Save(memoryStream, ImageFormat.Jpeg);
			return memoryStream.ToArray();
		}

		public static bool IsValidDate(string inputDate)
		{
			DateTime dateTime;
			return DateTime.TryParse(inputDate, out dateTime);
		}

		public static bool IsValidEmail(string email)
		{
			bool flag = false;
			try
			{
				if (!string.IsNullOrEmpty(email))
				{
					email = (new MailAddress(email)).Address;
					flag = true;
				}
			}
			catch (FormatException formatException)
			{
			}
			return flag;
		}

		public static bool IsValidSgNRIC(string nric)
		{
			bool flag;
			nric = nric.ToUpper().Trim();
			if ((new Regex("^(s|t)[0-9]{7}[a-jz]{1}$", RegexOptions.IgnoreCase)).IsMatch(nric))
			{
				string str = nric.Substring(1, 7);
				int num = 0;
				int num1 = 0;
				string lower = "";
				num = Convert.ToUInt16(str.Substring(0, 1)) * 2;
				num = num + Convert.ToUInt16(str.Substring(1, 1)) * 7;
				num = num + Convert.ToUInt16(str.Substring(2, 1)) * 6;
				num = num + Convert.ToUInt16(str.Substring(3, 1)) * 5;
				num = num + Convert.ToUInt16(str.Substring(4, 1)) * 4;
				num = num + Convert.ToUInt16(str.Substring(5, 1)) * 3;
				num = num + Convert.ToUInt16(str.Substring(6, 1)) * 2;
				if (nric.Substring(0, 1).ToLower() == "t")
				{
					num += 4;
				}
				num1 = 11 - num % 11;
				lower = nric.Substring(8, 1).ToLower();
				if ((num1 != 1 ? false : lower == "a"))
				{
					flag = true;
				}
				else if ((num1 != 2 ? false : lower == "b"))
				{
					flag = true;
				}
				else if ((num1 != 3 ? false : lower == "c"))
				{
					flag = true;
				}
				else if ((num1 != 4 ? false : lower == "d"))
				{
					flag = true;
				}
				else if ((num1 != 5 ? false : lower == "e"))
				{
					flag = true;
				}
				else if ((num1 != 6 ? false : lower == "f"))
				{
					flag = true;
				}
				else if ((num1 != 7 ? false : lower == "g"))
				{
					flag = true;
				}
				else if ((num1 != 8 ? false : lower == "h"))
				{
					flag = true;
				}
				else if ((num1 != 9 ? false : lower == "i"))
				{
					flag = true;
				}
				else if ((num1 != 10 ? true : lower != "z"))
				{
					flag = ((num1 != 11 ? true : lower != "j") ? false : true);
				}
				else
				{
					flag = true;
				}
			}
			else
			{
				flag = false;
			}
			return flag;
		}

		public static Image ResizeBitmap(Image fullSizeImage, int NewWidth, int MaxHeight)
		{
			fullSizeImage.RotateFlip(RotateFlipType.Rotate180FlipNone);
			fullSizeImage.RotateFlip(RotateFlipType.Rotate180FlipNone);
			int height = fullSizeImage.Height * NewWidth / fullSizeImage.Width;
			if (height > MaxHeight)
			{
				NewWidth = fullSizeImage.Width * MaxHeight / fullSizeImage.Height;
				height = MaxHeight;
			}
			return fullSizeImage.GetThumbnailImage(NewWidth, height, null, IntPtr.Zero);
		}

		public static string ToCamelCase(string input)
		{
			StringBuilder stringBuilder = new StringBuilder();
			if (!string.IsNullOrEmpty(input))
			{
				string[] strArrays = input.Split(new char[] { ' ' });
				for (int i = 0; i < (int)strArrays.Length; i++)
				{
					string str = strArrays[i];
					string str1 = str.Substring(0, 1);
					string str2 = str.Substring(1);
					stringBuilder.Append(string.Concat(str1.ToUpper(), str2));
					stringBuilder.Append(" ");
				}
			}
			return stringBuilder.ToString().Trim();
		}

		public static string XorCheckSum(string inpString)
		{
			int num = 0;
			char[] charArray = inpString.ToCharArray();
			for (int i = 0; i < (int)charArray.Length; i++)
			{
				num ^= charArray[i];
			}
			return num.ToString("X2");
		}
	}
}