using System;
using System.Security;

namespace MSClassLibrary.Common
{
	public static class SysGlobal
	{
		public readonly static SecureString MARAICHOL;

		public readonly static string DIR_LOG;

		static SysGlobal()
		{
			SysGlobal.MARAICHOL = new SecureString();
			SysGlobal.DIR_LOG = "C:\\OMNIHEALTH\\LOG\\";
			SysGlobal.MARAICHOL.AppendChar('O');
			SysGlobal.MARAICHOL.AppendChar('M');
			SysGlobal.MARAICHOL.AppendChar('N');
			SysGlobal.MARAICHOL.AppendChar('I');
			SysGlobal.MARAICHOL.AppendChar('1');
			SysGlobal.MARAICHOL.AppendChar('2');
			SysGlobal.MARAICHOL.AppendChar('3');
			SysGlobal.MARAICHOL.AppendChar('!');
			SysGlobal.MARAICHOL.AppendChar('@');
			SysGlobal.MARAICHOL.AppendChar('#');
			SysGlobal.MARAICHOL.MakeReadOnly();
		}
	}
}