using MSClassLibrary;
using System;

namespace MSClassLibrary.Entity
{
	public class RefEmailServerEntity : MSDisposableManager
	{
		private Guid iD = Guid.Empty;

		private string email = string.Empty;

		private string userName = string.Empty;

		private string password = string.Empty;

		private string sMTPServer = string.Empty;

		private int sMTPPortNo = -1;

		private bool authenticationRequired = false;

		private bool sSL = false;

		private string domain = string.Empty;

		private bool isActive = false;

		public bool AuthenticationRequired
		{
			get
			{
				return this.authenticationRequired;
			}
			set
			{
				this.authenticationRequired = value;
			}
		}

		public string Domain
		{
			get
			{
				return this.domain;
			}
			set
			{
				this.domain = value;
			}
		}

		public string Email
		{
			get
			{
				return this.email;
			}
			set
			{
				this.email = value;
			}
		}

		public Guid ID
		{
			get
			{
				return this.iD;
			}
			set
			{
				this.iD = value;
			}
		}

		public bool IsActive
		{
			get
			{
				return this.isActive;
			}
			set
			{
				this.isActive = value;
			}
		}

		public string Password
		{
			get
			{
				return this.password;
			}
			set
			{
				this.password = value;
			}
		}

		public int SMTPPortNo
		{
			get
			{
				return this.sMTPPortNo;
			}
			set
			{
				this.sMTPPortNo = value;
			}
		}

		public string SMTPServer
		{
			get
			{
				return this.sMTPServer;
			}
			set
			{
				this.sMTPServer = value;
			}
		}

		public bool SSL
		{
			get
			{
				return this.sSL;
			}
			set
			{
				this.sSL = value;
			}
		}

		public string UserName
		{
			get
			{
				return this.userName;
			}
			set
			{
				this.userName = value;
			}
		}

		public RefEmailServerEntity()
		{
		}
	}
}