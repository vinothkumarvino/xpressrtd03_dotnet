using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace MSClassLibrary.License
{
	[Serializable]
	public class MSProductLicenseInfo
	{
		public string CompanyName
		{
			get;
			set;
		}

		public string CopyRight
		{
			get;
			set;
		}

		public string CustomerName
		{
			get;
			set;
		}

		public string CustomerSerialIdentification
		{
			get;
			set;
		}

		public string LicenseData
		{
			get;
			set;
		}

		public string LicenseFile
		{
			get;
			set;
		}

		public string LicenseKey
		{
			get;
			set;
		}

		public string ProductID
		{
			get;
			set;
		}

		public string ProductVersion
		{
			get;
			set;
		}

		public string SystemID
		{
			get;
			set;
		}

		public MSProductLicenseInfo()
		{
			this.LicenseFile = string.Format("omnihealth_{0}_license.dat", Process.GetCurrentProcess().ProcessName.ToLower().Replace(".", ""));
		}

		public MSProductLicenseInfo(string productID, string productVersion, string copyRight, string companyName, string customerName, string customerSerialIdentifcation)
		{
			this.ProductID = productID;
			this.ProductVersion = productVersion;
			this.CopyRight = copyRight;
			this.CompanyName = companyName;
			this.CustomerName = customerName;
			this.CustomerSerialIdentification = customerSerialIdentifcation;
			this.SystemID = string.Empty;
			this.LicenseKey = string.Empty;
			this.LicenseFile = string.Format("omnihealth_{0}_license.dat", Process.GetCurrentProcess().ProcessName.ToLower().Replace(".", ""));
		}
	}
}