using MSClassLibrary.Entity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;

namespace MSClassLibrary.Mail
{
	public class MSMail
	{
		public MSMail()
		{
		}

		public static string Compress(FileInfo fi)
		{
			string str = string.Concat(fi.FullName, ".gz");
			using (FileStream fileStream = fi.OpenRead())
			{
				if ((File.GetAttributes(fi.FullName) & FileAttributes.Hidden) != FileAttributes.Hidden & (fi.Extension != ".gz"))
				{
					using (FileStream fileStream1 = File.Create(str))
					{
						using (GZipStream gZipStream = new GZipStream(fileStream1, CompressionMode.Compress))
						{
							byte[] numArray = new byte[4096];
							while (true)
							{
								int num = fileStream.Read(numArray, 0, (int)numArray.Length);
								int num1 = num;
								if (num == 0)
								{
									break;
								}
								gZipStream.Write(numArray, 0, num1);
							}
							string name = fi.Name;
							string str1 = fi.Length.ToString();
							long length = fileStream1.Length;
							Console.WriteLine("Compressed {0} from {1} to {2} bytes.", name, str1, length.ToString());
						}
					}
				}
			}
			return str;
		}

		public static void SendMail(RefEmailServerEntity entity, List<string> toAddress, List<string> ccAddress, string messageSubject, string messageData, List<string> attachmentFileList)
		{
			try
			{
				SmtpClient smtpClient = new SmtpClient()
				{
					Host = entity.SMTPServer,
					Port = entity.SMTPPortNo,
					EnableSsl = entity.SSL,
					Timeout = 10000,
					DeliveryMethod = SmtpDeliveryMethod.Network,
					UseDefaultCredentials = true,
					Credentials = new NetworkCredential(entity.UserName, entity.Password)
				};
				using (MailMessage mailMessage = new MailMessage())
				{
					mailMessage.From = new MailAddress(entity.Email);
					foreach (string str in toAddress)
					{
						try
						{
							mailMessage.To.Add(new MailAddress(str));
						}
						catch
						{
						}
					}
					foreach (string str1 in ccAddress)
					{
						try
						{
							mailMessage.CC.Add(new MailAddress(str1));
						}
						catch
						{
						}
					}
					mailMessage.Priority = MailPriority.High;
					mailMessage.Subject = messageSubject;
					mailMessage.IsBodyHtml = true;
					mailMessage.Body = messageData;
					if (attachmentFileList.Count > 0)
					{
						MailMessage mailMessage1 = mailMessage;
						mailMessage1.Body = string.Concat(mailMessage1.Body, "<HR/><OL>");
						foreach (string str2 in attachmentFileList)
						{
							MailMessage mailMessage2 = mailMessage;
							mailMessage2.Body = string.Concat(mailMessage2.Body, "<LI>", Path.GetFileName(str2), "</LI>");
							ContentType contentType = new ContentType()
							{
								MediaType = "application/pdf",
								Name = Path.GetFileName(str2)
							};
							Attachment attachment = new Attachment(str2, new ContentType());
							attachment.ContentDisposition.Inline = false;
							mailMessage.Attachments.Add(attachment);
						}
						MailMessage mailMessage3 = mailMessage;
						mailMessage3.Body = string.Concat(mailMessage3.Body, "</OL>");
					}
					MailMessage mailMessage4 = mailMessage;
					mailMessage4.Body = string.Concat(mailMessage4.Body, "<HR/>");
					MailMessage mailMessage5 = mailMessage;
					mailMessage5.Body = string.Concat(mailMessage5.Body, "Note: This is a computer generated email. Please do not reply to it. If you require any further assistance, contact system administrator");
					smtpClient.Send(mailMessage);
				}
			}
			catch (Exception exception)
			{
				Console.WriteLine(exception.Message);
			}
		}
	}
}