using MSClassLibrary.Common;
using MSClassLibrary.License;
using MSClassLibrary.Log;
using MSClassLibrary.Security;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Management;
using System.Runtime.CompilerServices;
using System.Text;
using System.Xml.Serialization;

namespace MSClassLibrary
{
	public class MSProductLicense : IDisposable
	{
		public const string LicenseDirectory = "C:\\OMNIHEALTH\\LICENSE\\";

		private bool disposed = false;

		public MSProductLicenseInfo SoftwareProductLicenseInfo
		{
			get;
			set;
		}

		public MSProductLicense()
		{
			try
			{
				if (!Directory.Exists("C:\\OMNIHEALTH\\LICENSE\\"))
				{
					Directory.CreateDirectory("C:\\OMNIHEALTH\\LICENSE\\");
				}
			}
			catch
			{
				SysLog.WriteLog(MSLogLevel.CRITICAL, "License folder unable to create. Please contact system administrator.");
			}
		}

		public T Deserialize<T>(string path, T obj)
		{
			T t;
			XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
			using (GZipStream gZipStream = new GZipStream(File.OpenRead(path), CompressionMode.Decompress))
			{
				t = (T)xmlSerializer.Deserialize(gZipStream);
			}
			return t;
		}

		public void Dispose()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		protected virtual void Dispose(bool disposing)
		{
			if (!this.disposed)
			{
				if (disposing)
				{
					this.SoftwareProductLicenseInfo = null;
				}
				this.SoftwareProductLicenseInfo = null;
				this.disposed = true;
			}
		}

		public void DumpLicenseRequest()
		{
			if (this.SoftwareProductLicenseInfo == null)
			{
				SysLog.WriteLog(MSLogLevel.CRITICAL, "An error occured in getting license request.");
			}
			else
			{
				this.SoftwareProductLicenseInfo.SystemID = this.GetSystemDetail();
				string str = string.Format("{0}omnihealth_{1}_license_request.dat", "C:\\OMNIHEALTH\\LICENSE\\", Process.GetCurrentProcess().ProcessName.ToLower().Replace(".", ""));
				MSAESEncryption.EncryptObject<MSProductLicenseInfo>(str, this.SoftwareProductLicenseInfo, MSAESEncryption.ConvertToRawString(SysGlobal.MARAICHOL));
			}
		}

		~MSProductLicense()
		{
			this.Dispose(false);
		}

		public string GetSystemDetail()
		{
			StringBuilder stringBuilder = new StringBuilder();
			List<string> strs = new List<string>()
			{
				"MADHAVAN"
			};
			SysLog.WriteLog(MSLogLevel.INFORMATIONAL, "Reading System Information.");
			try
			{
				using (ManagementObjectCollection.ManagementObjectEnumerator enumerator = (new ManagementClass("win32_processor")).GetInstances().GetEnumerator())
				{
					if (enumerator.MoveNext())
					{
						ManagementObject current = (ManagementObject)enumerator.Current;
						strs.Add(current.Properties["processorID"].Value.ToString());
					}
				}
			}
			catch (Exception exception1)
			{
				Exception exception = exception1;
				SysLog.WriteLog(MSLogLevel.CRITICAL, "An error occured in getting System Information.");
				SysLog.WriteLog(MSLogLevel.ERROR, exception.Message);
			}
			strs.Sort();
			stringBuilder.Append(string.Join("|", strs.ToArray()));
			return stringBuilder.ToString();
		}

		public void Serialize<T>(string path, T obj)
		{
			XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
			using (GZipStream gZipStream = new GZipStream(File.Create(path), CompressionMode.Compress))
			{
				xmlSerializer.Serialize(gZipStream, obj);
			}
		}

		public bool Verify(string serialId)
		{
			bool flag = false;
			string str = string.Format("{0}omnihealth_{1}_license.dat", "C:\\OMNIHEALTH\\LICENSE\\", Process.GetCurrentProcess().ProcessName.ToLower().Replace(".", ""));
			if (!File.Exists(str))
			{
				SysLog.WriteLog(MSLogLevel.CRITICAL, "An error occured in getting license verification.");
			}
			else
			{
				this.SoftwareProductLicenseInfo = MSAESEncryption.DecryptObject<MSProductLicenseInfo>(str, new MSProductLicenseInfo(), MSAESEncryption.ConvertToRawString(SysGlobal.MARAICHOL));
				try
				{
					string str1 = MSRSACrypto.PrivateKeyDecrypt(this.SoftwareProductLicenseInfo.LicenseKey, this.SoftwareProductLicenseInfo.LicenseData);
					if (!string.Format("{0}|{1}", this.GetSystemDetail(), serialId).Equals(str1))
					{
						SysLog.WriteLog(MSLogLevel.CRITICAL, "An error occured in getting license verification.");
					}
					else
					{
						flag = true;
						SysLog.WriteLog(MSLogLevel.INFORMATIONAL, "License Verified.");
					}
				}
				catch
				{
					SysLog.WriteLog(MSLogLevel.CRITICAL, "An error occured in getting license verification.");
				}
			}
			return flag;
		}
	}
}