using System;

namespace MSClassLibrary
{
	public class MSDisposableManager : IDisposable
	{
		private bool disposed = false;

		public MSDisposableManager()
		{
		}

		public void Dispose()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		protected virtual void Dispose(bool disposing)
		{
			if (!this.disposed)
			{
				if (disposing)
				{
				}
				this.disposed = true;
			}
		}

		~MSDisposableManager()
		{
			this.Dispose(false);
		}
	}
}