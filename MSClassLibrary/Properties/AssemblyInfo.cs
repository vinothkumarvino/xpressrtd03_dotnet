﻿using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Runtime.Versioning;

[assembly: AssemblyCompany("OmniHealth Pte Ltd, Singapore.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCopyright("Copyright © 2016 OmniHealth Pte Ltd, Singapore.")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: AssemblyProduct("MSClassLibrary")]
[assembly: AssemblyTitle("MSClassLibrary")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyVersion("1.0.6600.18973")]
[assembly: CompilationRelaxations(8)]
[assembly: ComVisible(false)]
[assembly: Debuggable(DebuggableAttribute.DebuggingModes.Default | DebuggableAttribute.DebuggingModes.DisableOptimizations | DebuggableAttribute.DebuggingModes.IgnoreSymbolStoreSequencePoints | DebuggableAttribute.DebuggingModes.EnableEditAndContinue)]
[assembly: Guid("f69a450f-1e83-4b22-b6ad-713aa3c4347e")]
[assembly: RuntimeCompatibility(WrapNonExceptionThrows=true)]
