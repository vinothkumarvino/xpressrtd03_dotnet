using OPtlDAL.DataFactory;
using OPtlDAL.Entity;
using System;
using System.Collections.Generic;

namespace OPtlDAL.BO
{
	public class PtlBins
	{
		public PtlBins()
		{
		}

		public bool Add(ref PtlBinsEntity entity)
		{
			return (new PtlBinsFactory()).Add(ref entity);
		}

		public bool Delete(PtlBinsEntity entity)
		{
			return (new PtlBinsFactory()).Delete(entity);
		}

		public PtlBinsEntity Get(Guid ID)
		{
			return (new PtlBinsFactory()).Get(ID);
		}

		public PtlBinsEntity Get(string srcReference)
		{
			return (new PtlBinsFactory()).Get(srcReference);
		}

		public PtlBinsEntity Get(string systemName, int moduleID)
		{
			return (new PtlBinsFactory()).Get(systemName, moduleID);
		}

		public PtlBinsEntity GetLightModule(int moduleID)
		{
			return (new PtlBinsFactory()).GetLightModule(moduleID);
		}

		public List<PtlBinsEntity> GetList(Guid ID)
		{
			return (new PtlBinsFactory()).GetList(ID);
		}

		public bool Save(PtlBinsEntity entity)
		{
			return (new PtlBinsFactory()).Save(entity);
		}
	}
}