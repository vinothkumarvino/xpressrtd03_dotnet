using OPtlDAL.DataFactory;
using OPtlDAL.Entity;
using System;
using System.Collections.Generic;

namespace OPtlDAL.BO
{
	public class PtlOrders
	{
		public PtlOrders()
		{
		}

		public bool Add(ref PtlOrdersEntity entity)
		{
			return (new PtlOrdersFactory()).Add(ref entity);
		}

		public bool Delete(PtlOrdersEntity entity)
		{
			return (new PtlOrdersFactory()).Delete(entity);
		}

		public PtlOrdersEntity Get(Guid ID)
		{
			return (new PtlOrdersFactory()).Get(ID);
		}

		public List<PtlOrdersEntity> GetList(Guid ID)
		{
			return (new PtlOrdersFactory()).GetList(ID);
		}

		public bool Save(PtlOrdersEntity entity)
		{
			return (new PtlOrdersFactory()).Save(entity);
		}
	}
}