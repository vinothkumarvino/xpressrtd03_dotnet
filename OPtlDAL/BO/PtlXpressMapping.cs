using OPtlDAL.DataFactory;
using OPtlDAL.Entity;
using System;
using System.Collections.Generic;

namespace OPtlDAL.BO
{
	public class PtlXpressMapping
	{
		public PtlXpressMapping()
		{
		}

		public bool Add(ref PtlXpressMappingEntity entity)
		{
			return (new PtlXpressMappingFactory()).Add(ref entity);
		}

		public bool Delete(PtlXpressMappingEntity entity)
		{
			return (new PtlXpressMappingFactory()).Delete(entity);
		}

		public PtlXpressMappingEntity Get(string pigeonHoleID)
		{
			return (new PtlXpressMappingFactory()).Get(pigeonHoleID);
		}

		public List<PtlXpressMappingEntity> GetList()
		{
			return (new PtlXpressMappingFactory()).GetList();
		}

		public List<PtlXpressMappingEntity> GetList(string pigeonHoleID)
		{
			return (new PtlXpressMappingFactory()).GetList(pigeonHoleID);
		}

		public PtlXpressMappingEntity GetSRC(string SRC)
		{
			return (new PtlXpressMappingFactory()).GetSRC(SRC);
		}

		public bool Save(PtlXpressMappingEntity entity)
		{
			return (new PtlXpressMappingFactory()).Save(entity);
		}
	}
}