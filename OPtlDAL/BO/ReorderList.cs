using OPtlDAL.DataFactory;
using OPtlDAL.Entity;
using System;
using System.Collections.Generic;

namespace OPtlDAL.BO
{
	public class ReorderList
	{
		public ReorderList()
		{
		}

		public bool Add(ref ReorderListEntity entity)
		{
			return (new ReorderListFactory()).Add(ref entity);
		}

		public bool Delete(ReorderListEntity entity)
		{
			return (new ReorderListFactory()).Delete(entity);
		}

		public ReorderListEntity Get(Guid ID)
		{
			return (new ReorderListFactory()).Get(ID);
		}

		public List<ReorderListEntity> GetList()
		{
			return (new ReorderListFactory()).GetList();
		}

		public bool Save(ReorderListEntity entity)
		{
			return (new ReorderListFactory()).Save(entity);
		}
	}
}