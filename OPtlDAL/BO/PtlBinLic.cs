using OPtlDAL.DataFactory;
using OPtlDAL.Entity;
using System;
using System.Collections.Generic;

namespace OPtlDAL.BO
{
	public class PtlBinLic
	{
		public PtlBinLic()
		{
		}

		public bool Add(ref PtlBinLicEntity entity)
		{
			return (new PtlBinLicFactory()).Add(ref entity);
		}

		public bool Delete(PtlBinLicEntity entity)
		{
			bool flag = false;
			flag = (new PtlBinLicFactory()).Delete(entity);
			PtlBinHistoryEntity ptlBinHistoryEntity = new PtlBinHistoryEntity()
			{
				ID = Guid.NewGuid(),
				SystemName = entity.SystemName,
				BinID = entity.BinID,
				SRC = entity.SRC,
				IsLightCommandIssued = entity.IsLightCommandIssued,
				ModuleID = entity.ModuleID,
				ModuleColor = entity.ModuleColor,
				ModuleFlash = entity.ModuleFlash,
				ModuleBuzzer = entity.ModuleBuzzer,
				DisplayContent = entity.DisplayContent,
				ArticleID = entity.ArticleID,
				StockOut = entity.StockOut,
				StockIn = entity.StockIn,
				Adjustment = entity.Adjustment,
				Remarks = entity.Remarks,
				RequestID = entity.RequestID,
				RequestCode = entity.RequestCode,
				LightOnOffCommand = entity.LightOnOffCommand,
				OnTimeStamp = entity.OnTimeStamp,
				TurnOffByButtonPress = entity.TurnOffByButtonPress,
				TurnOffByCommand = entity.TurnOffByCommand,
				OffTimeStamp = entity.OffTimeStamp
			};
			(new PtlBinHistory()).Add(ref ptlBinHistoryEntity);
			return flag;
		}

		public PtlBinLicEntity Get(Guid ID)
		{
			return (new PtlBinLicFactory()).Get(ID);
		}

		public PtlBinLicEntity Get(string srcReference)
		{
			return (new PtlBinLicFactory()).Get(srcReference);
		}

		public PtlBinLicEntity GetLIC(string srcReference)
		{
			return (new PtlBinLicFactory()).GetLIC(srcReference);
		}

		public List<PtlBinLicEntity> GetList()
		{
			return (new PtlBinLicFactory()).GetList();
		}

		public List<PtlBinLicEntity> GetList(string srcReference)
		{
			return (new PtlBinLicFactory()).GetList(srcReference);
		}

		public List<PtlBinLicEntity> GetList(string srcReference, string onOff)
		{
			return (new PtlBinLicFactory()).GetList(srcReference, onOff);
		}

		public bool Save(PtlBinLicEntity entity)
		{
			return (new PtlBinLicFactory()).Save(entity);
		}
	}
}