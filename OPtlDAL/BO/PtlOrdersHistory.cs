using OPtlDAL.DataFactory;
using OPtlDAL.Entity;
using System;
using System.Collections.Generic;

namespace OPtlDAL.BO
{
	public class PtlOrdersHistory
	{
		public PtlOrdersHistory()
		{
		}

		public bool Add(ref PtlOrdersHistoryEntity entity)
		{
			return (new PtlOrdersHistoryFactory()).Add(ref entity);
		}

		public bool Delete(PtlOrdersHistoryEntity entity)
		{
			return (new PtlOrdersHistoryFactory()).Delete(entity);
		}

		public PtlOrdersHistoryEntity Get(Guid ID)
		{
			return (new PtlOrdersHistoryFactory()).Get(ID);
		}

		public List<PtlOrdersHistoryEntity> GetList(Guid ID)
		{
			return (new PtlOrdersHistoryFactory()).GetList(ID);
		}

		public bool Save(PtlOrdersHistoryEntity entity)
		{
			return (new PtlOrdersHistoryFactory()).Save(entity);
		}
	}
}