using OPtlDAL.DataFactory;
using OPtlDAL.Entity;
using System;
using System.Collections.Generic;

namespace OPtlDAL.BO
{
	public class PtlSystem
	{
		public PtlSystem()
		{
		}

		public bool Add(ref PtlSystemEntity entity)
		{
			return (new PtlSystemFactory()).Add(ref entity);
		}

		public bool Delete(PtlSystemEntity entity)
		{
			return (new PtlSystemFactory()).Delete(entity);
		}

		public PtlSystemEntity Get(string systemName)
		{
			return (new PtlSystemFactory()).Get(systemName);
		}

		public List<PtlSystemEntity> GetControllerList(string groupName)
		{
			return (new PtlSystemFactory()).GetControllerList(groupName);
		}

		public List<PtlSystemEntity> GetList()
		{
			return (new PtlSystemFactory()).GetList();
		}

		public bool Save(PtlSystemEntity entity)
		{
			return (new PtlSystemFactory()).Save(entity);
		}
	}
}