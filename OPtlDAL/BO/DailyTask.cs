using OPtlDAL.DataFactory;
using OPtlDAL.Entity;
using System;
using System.Collections.Generic;

namespace OPtlDAL.BO
{
	public class DailyTask
	{
		public DailyTask()
		{
		}

		public bool Add(ref DailyTaskEntity entity)
		{
			return (new DailyTaskFactory()).Add(ref entity);
		}

		public bool Delete(DailyTaskEntity entity)
		{
			return (new DailyTaskFactory()).Delete(entity);
		}

		public DailyTaskEntity Get(Guid ID)
		{
			return (new DailyTaskFactory()).Get(ID);
		}

		public List<DailyTaskEntity> GetList()
		{
			return (new DailyTaskFactory()).GetList();
		}

		public bool Save(DailyTaskEntity entity)
		{
			return (new DailyTaskFactory()).Save(entity);
		}
	}
}