using OPtlDAL.DataFactory;
using OPtlDAL.Entity;
using System;
using System.Collections.Generic;

namespace OPtlDAL.BO
{
	public class ArticleMaster
	{
		public ArticleMaster()
		{
		}

		public bool Add(ref ArticleMasterEntity entity)
		{
			return (new ArticleMasterFactory()).Add(ref entity);
		}

		public bool Delete(ArticleMasterEntity entity)
		{
			return (new ArticleMasterFactory()).Delete(entity);
		}

		public ArticleMasterEntity Get(Guid ID)
		{
			return (new ArticleMasterFactory()).Get(ID);
		}

		public List<ArticleMasterEntity> GetList()
		{
			return (new ArticleMasterFactory()).GetList();
		}

		public bool Save(ArticleMasterEntity entity)
		{
			return (new ArticleMasterFactory()).Save(entity);
		}
	}
}