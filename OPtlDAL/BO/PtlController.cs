using OPtlDAL.DataFactory;
using OPtlDAL.Entity;
using System;
using System.Collections.Generic;

namespace OPtlDAL.BO
{
	public class PtlController
	{
		public PtlController()
		{
		}

		public bool Add(ref PtlControllerEntity entity)
		{
			return (new PtlControllerFactory()).Add(ref entity);
		}

		public bool Delete(PtlControllerEntity entity)
		{
			return (new PtlControllerFactory()).Delete(entity);
		}

		public PtlControllerEntity Get(Guid ID)
		{
			return (new PtlControllerFactory()).Get(ID);
		}

		public PtlControllerEntity Get(string systemName)
		{
			return (new PtlControllerFactory()).Get(systemName);
		}

		public List<PtlControllerEntity> GetList()
		{
			return (new PtlControllerFactory()).GetList();
		}

		public bool Save(PtlControllerEntity entity)
		{
			return (new PtlControllerFactory()).Save(entity);
		}
	}
}