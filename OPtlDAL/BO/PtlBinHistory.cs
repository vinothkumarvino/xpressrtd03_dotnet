using OPtlDAL.DataFactory;
using OPtlDAL.Entity;
using System;
using System.Collections.Generic;

namespace OPtlDAL.BO
{
	public class PtlBinHistory
	{
		public PtlBinHistory()
		{
		}

		public bool Add(ref PtlBinHistoryEntity entity)
		{
			return (new PtlBinHistoryFactory()).Add(ref entity);
		}

		public bool Delete(PtlBinHistoryEntity entity)
		{
			return (new PtlBinHistoryFactory()).Delete(entity);
		}

		public PtlBinHistoryEntity Get(Guid ID)
		{
			return (new PtlBinHistoryFactory()).Get(ID);
		}

		public List<PtlBinHistoryEntity> GetList(int topRecords)
		{
			return (new PtlBinHistoryFactory()).GetList(topRecords);
		}

		public bool Save(PtlBinHistoryEntity entity)
		{
			return (new PtlBinHistoryFactory()).Save(entity);
		}
	}
}