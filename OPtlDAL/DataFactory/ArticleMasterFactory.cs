using MSClassLibrary.Log;
using OPtlDAL.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;

namespace OPtlDAL.DataFactory
{
	internal class ArticleMasterFactory
	{
		public ArticleMasterFactory()
		{
		}

		public bool Add(ref ArticleMasterEntity entity)
		{
			bool flag = false;
			try
			{
				if (ConnectorFactory.Connect())
				{
					string str = "INSERT INTO tbl_article_master (ArticleID, ArticleName, ArticleItemCode, ArticleItemBarCode, AbbreviatedDosageForm, PackageUnit, Width, Length, Height, Diameter, Weight, PackageSize, TabOrCapPerBlister, NoOfBlister, ExpiryDate, IsActive, Notes, ReOrderLevel, TotalQuantity, CreatedBy, CreatedOn, UpdatedBy, UpdatedOn) VALUES (@ArticleID, @ArticleName, @ArticleItemCode, @ArticleItemBarCode, @AbbreviatedDosageForm, @PackageUnit, @Width, @Length, @Height, @Diameter, @Weight, @PackageSize, @TabOrCapPerBlister, @NoOfBlister, @ExpiryDate, @IsActive, @Notes, @ReOrderLevel, @TotalQuantity, @CreatedBy, @CreatedOn, @UpdatedBy, @UpdatedOn);";
					using (SqlCommand sqlCommand = new SqlCommand())
					{
						sqlCommand.Connection = ConnectorFactory.Connection;
						sqlCommand.CommandText = str;
						sqlCommand.Prepare();
						sqlCommand.Parameters.AddWithValue("@ArticleID", entity.ArticleID);
						sqlCommand.Parameters.AddWithValue("@ArticleName", entity.ArticleName);
						sqlCommand.Parameters.AddWithValue("@ArticleItemCode", entity.ArticleItemCode);
						sqlCommand.Parameters.AddWithValue("@ArticleItemBarCode", entity.ArticleItemBarCode);
						sqlCommand.Parameters.AddWithValue("@AbbreviatedDosageForm", entity.AbbreviatedDosageForm);
						sqlCommand.Parameters.AddWithValue("@PackageUnit", entity.PackageUnit);
						sqlCommand.Parameters.AddWithValue("@Width", entity.Width);
						sqlCommand.Parameters.AddWithValue("@Length", entity.Length);
						sqlCommand.Parameters.AddWithValue("@Height", entity.Height);
						sqlCommand.Parameters.AddWithValue("@Diameter", entity.Diameter);
						sqlCommand.Parameters.AddWithValue("@Weight", entity.Weight);
						sqlCommand.Parameters.AddWithValue("@PackageSize", entity.PackageSize);
						sqlCommand.Parameters.AddWithValue("@TabOrCapPerBlister", entity.TabOrCapPerBlister);
						sqlCommand.Parameters.AddWithValue("@NoOfBlister", entity.NoOfBlister);
						if (entity.ExpiryDate != DateTime.MinValue)
						{
							sqlCommand.Parameters.AddWithValue("@ExpiryDate", entity.ExpiryDate);
						}
						else
						{
							sqlCommand.Parameters.AddWithValue("@ExpiryDate", DateTime.Now);
						}
						sqlCommand.Parameters.AddWithValue("@IsActive", entity.IsActive);
						sqlCommand.Parameters.AddWithValue("@Notes", entity.Notes);
						sqlCommand.Parameters.AddWithValue("@ReOrderLevel", entity.ReOrderLevel);
						sqlCommand.Parameters.AddWithValue("@TotalQuantity", entity.TotalQuantity);
						sqlCommand.Parameters.AddWithValue("@CreatedBy", entity.CreatedBy);
						if (entity.CreatedOn != DateTime.MinValue)
						{
							sqlCommand.Parameters.AddWithValue("@CreatedOn", entity.CreatedOn);
						}
						else
						{
							sqlCommand.Parameters.AddWithValue("@CreatedOn", DateTime.Now);
						}
						sqlCommand.Parameters.AddWithValue("@UpdatedBy", entity.UpdatedBy);
						if (entity.UpdatedOn != DateTime.MinValue)
						{
							sqlCommand.Parameters.AddWithValue("@UpdatedOn", entity.UpdatedOn);
						}
						else
						{
							sqlCommand.Parameters.AddWithValue("@UpdatedOn", DateTime.Now);
						}
						sqlCommand.ExecuteScalar();
						flag = true;
						sqlCommand.Dispose();
					}
				}
			}
			catch (Exception exception)
			{
				SysLog.WriteLog(3, exception.Message);
			}
			return flag;
		}

		public bool Delete(ArticleMasterEntity entity)
		{
			bool flag = false;
			try
			{
				if (ConnectorFactory.Connect())
				{
					string str = "DELETE FROM tbl_article_master WHERE ArticleID=@ArticleID";
					using (SqlCommand sqlCommand = new SqlCommand())
					{
						sqlCommand.Connection = ConnectorFactory.Connection;
						sqlCommand.CommandText = str;
						sqlCommand.Prepare();
						sqlCommand.Parameters.AddWithValue("@ArticleID", entity.ArticleID);
						object obj = sqlCommand.ExecuteNonQuery();
						flag = true;
						sqlCommand.Dispose();
					}
				}
			}
			catch (Exception exception)
			{
				SysLog.WriteLog(3, exception.Message);
			}
			return flag;
		}

		public ArticleMasterEntity Get(Guid articleID)
		{
			ArticleMasterEntity articleMasterEntity = null;
			try
			{
				if (ConnectorFactory.Connect())
				{
					string str = "SELECT ArticleID, ArticleName, ArticleItemCode, ArticleItemBarCode, AbbreviatedDosageForm, PackageUnit, Width, Length, Height, Diameter, Weight, PackageSize, TabOrCapPerBlister, NoOfBlister, ExpiryDate, IsActive, Notes, ReOrderLevel, TotalQuantity, CreatedBy, CreatedOn, UpdatedBy, UpdatedOn FROM tbl_article_master";
					str = string.Concat(str, " WHERE ArticleID=@ArticleID;");
					using (SqlCommand sqlCommand = new SqlCommand())
					{
						sqlCommand.Connection = ConnectorFactory.Connection;
						sqlCommand.CommandText = str;
						sqlCommand.Prepare();
						sqlCommand.Parameters.AddWithValue("@ArticleID", articleID);
						using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
						{
							try
							{
								try
								{
									if (sqlDataReader.Read())
									{
										articleMasterEntity = new ArticleMasterEntity();
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ArticleID")))
										{
											articleMasterEntity.ArticleID = sqlDataReader.GetString(sqlDataReader.GetOrdinal("ArticleID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ArticleName")))
										{
											articleMasterEntity.ArticleName = sqlDataReader.GetString(sqlDataReader.GetOrdinal("ArticleName"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ArticleItemCode")))
										{
											articleMasterEntity.ArticleItemCode = sqlDataReader.GetString(sqlDataReader.GetOrdinal("ArticleItemCode"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ArticleItemBarCode")))
										{
											articleMasterEntity.ArticleItemBarCode = sqlDataReader.GetString(sqlDataReader.GetOrdinal("ArticleItemBarCode"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("AbbreviatedDosageForm")))
										{
											articleMasterEntity.AbbreviatedDosageForm = sqlDataReader.GetString(sqlDataReader.GetOrdinal("AbbreviatedDosageForm"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("PackageUnit")))
										{
											articleMasterEntity.PackageUnit = sqlDataReader.GetString(sqlDataReader.GetOrdinal("PackageUnit"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("Width")))
										{
											articleMasterEntity.Width = sqlDataReader.GetDecimal(sqlDataReader.GetOrdinal("Width"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("Length")))
										{
											articleMasterEntity.Length = sqlDataReader.GetDecimal(sqlDataReader.GetOrdinal("Length"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("Height")))
										{
											articleMasterEntity.Height = sqlDataReader.GetDecimal(sqlDataReader.GetOrdinal("Height"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("Diameter")))
										{
											articleMasterEntity.Diameter = sqlDataReader.GetDecimal(sqlDataReader.GetOrdinal("Diameter"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("Weight")))
										{
											articleMasterEntity.Weight = sqlDataReader.GetDecimal(sqlDataReader.GetOrdinal("Weight"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("PackageSize")))
										{
											articleMasterEntity.PackageSize = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("PackageSize"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("TabOrCapPerBlister")))
										{
											articleMasterEntity.TabOrCapPerBlister = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("TabOrCapPerBlister"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("NoOfBlister")))
										{
											articleMasterEntity.NoOfBlister = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("NoOfBlister"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ExpiryDate")))
										{
											articleMasterEntity.ExpiryDate = sqlDataReader.GetDateTime(sqlDataReader.GetOrdinal("ExpiryDate"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("IsActive")))
										{
											articleMasterEntity.IsActive = sqlDataReader.GetBoolean(sqlDataReader.GetOrdinal("IsActive"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("Notes")))
										{
											articleMasterEntity.Notes = sqlDataReader.GetString(sqlDataReader.GetOrdinal("Notes"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ReOrderLevel")))
										{
											articleMasterEntity.ReOrderLevel = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("ReOrderLevel"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("TotalQuantity")))
										{
											articleMasterEntity.TotalQuantity = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("TotalQuantity"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("CreatedBy")))
										{
											articleMasterEntity.CreatedBy = sqlDataReader.GetString(sqlDataReader.GetOrdinal("CreatedBy"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("CreatedOn")))
										{
											articleMasterEntity.CreatedOn = sqlDataReader.GetDateTime(sqlDataReader.GetOrdinal("CreatedOn"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("UpdatedBy")))
										{
											articleMasterEntity.UpdatedBy = sqlDataReader.GetString(sqlDataReader.GetOrdinal("UpdatedBy"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("UpdatedOn")))
										{
											articleMasterEntity.UpdatedOn = sqlDataReader.GetDateTime(sqlDataReader.GetOrdinal("UpdatedOn"));
										}
									}
								}
								catch (Exception exception)
								{
									SysLog.WriteLog(3, exception.Message);
								}
							}
							finally
							{
								sqlDataReader.Close();
							}
						}
						sqlCommand.Dispose();
					}
				}
			}
			catch (Exception exception1)
			{
				SysLog.WriteLog(3, exception1.Message);
			}
			return articleMasterEntity;
		}

		public List<ArticleMasterEntity> GetList()
		{
			List<ArticleMasterEntity> articleMasterEntities = new List<ArticleMasterEntity>();
			try
			{
				if (ConnectorFactory.Connect())
				{
					string str = "SELECT ArticleID, ArticleName, ArticleItemCode, ArticleItemBarCode, AbbreviatedDosageForm, PackageUnit, Width, Length, Height, Diameter, Weight, PackageSize, TabOrCapPerBlister, NoOfBlister, ExpiryDate, IsActive, Notes, ReOrderLevel, TotalQuantity, CreatedBy, CreatedOn, UpdatedBy, UpdatedOn FROM tbl_article_master";
					using (SqlCommand sqlCommand = new SqlCommand())
					{
						sqlCommand.Connection = ConnectorFactory.Connection;
						sqlCommand.CommandText = str;
						sqlCommand.Prepare();
						using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
						{
							try
							{
								try
								{
									while (sqlDataReader.Read())
									{
										ArticleMasterEntity articleMasterEntity = new ArticleMasterEntity();
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ArticleID")))
										{
											articleMasterEntity.ArticleID = sqlDataReader.GetString(sqlDataReader.GetOrdinal("ArticleID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ArticleName")))
										{
											articleMasterEntity.ArticleName = sqlDataReader.GetString(sqlDataReader.GetOrdinal("ArticleName"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ArticleItemCode")))
										{
											articleMasterEntity.ArticleItemCode = sqlDataReader.GetString(sqlDataReader.GetOrdinal("ArticleItemCode"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ArticleItemBarCode")))
										{
											articleMasterEntity.ArticleItemBarCode = sqlDataReader.GetString(sqlDataReader.GetOrdinal("ArticleItemBarCode"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("AbbreviatedDosageForm")))
										{
											articleMasterEntity.AbbreviatedDosageForm = sqlDataReader.GetString(sqlDataReader.GetOrdinal("AbbreviatedDosageForm"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("PackageUnit")))
										{
											articleMasterEntity.PackageUnit = sqlDataReader.GetString(sqlDataReader.GetOrdinal("PackageUnit"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("Width")))
										{
											articleMasterEntity.Width = sqlDataReader.GetDecimal(sqlDataReader.GetOrdinal("Width"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("Length")))
										{
											articleMasterEntity.Length = sqlDataReader.GetDecimal(sqlDataReader.GetOrdinal("Length"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("Height")))
										{
											articleMasterEntity.Height = sqlDataReader.GetDecimal(sqlDataReader.GetOrdinal("Height"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("Diameter")))
										{
											articleMasterEntity.Diameter = sqlDataReader.GetDecimal(sqlDataReader.GetOrdinal("Diameter"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("Weight")))
										{
											articleMasterEntity.Weight = sqlDataReader.GetDecimal(sqlDataReader.GetOrdinal("Weight"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("PackageSize")))
										{
											articleMasterEntity.PackageSize = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("PackageSize"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("TabOrCapPerBlister")))
										{
											articleMasterEntity.TabOrCapPerBlister = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("TabOrCapPerBlister"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("NoOfBlister")))
										{
											articleMasterEntity.NoOfBlister = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("NoOfBlister"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ExpiryDate")))
										{
											articleMasterEntity.ExpiryDate = sqlDataReader.GetDateTime(sqlDataReader.GetOrdinal("ExpiryDate"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("IsActive")))
										{
											articleMasterEntity.IsActive = sqlDataReader.GetBoolean(sqlDataReader.GetOrdinal("IsActive"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("Notes")))
										{
											articleMasterEntity.Notes = sqlDataReader.GetString(sqlDataReader.GetOrdinal("Notes"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ReOrderLevel")))
										{
											articleMasterEntity.ReOrderLevel = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("ReOrderLevel"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("TotalQuantity")))
										{
											articleMasterEntity.TotalQuantity = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("TotalQuantity"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("CreatedBy")))
										{
											articleMasterEntity.CreatedBy = sqlDataReader.GetString(sqlDataReader.GetOrdinal("CreatedBy"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("CreatedOn")))
										{
											articleMasterEntity.CreatedOn = sqlDataReader.GetDateTime(sqlDataReader.GetOrdinal("CreatedOn"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("UpdatedBy")))
										{
											articleMasterEntity.UpdatedBy = sqlDataReader.GetString(sqlDataReader.GetOrdinal("UpdatedBy"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("UpdatedOn")))
										{
											articleMasterEntity.UpdatedOn = sqlDataReader.GetDateTime(sqlDataReader.GetOrdinal("UpdatedOn"));
										}
										articleMasterEntities.Add(articleMasterEntity);
									}
								}
								catch (Exception exception)
								{
									SysLog.WriteLog(3, exception.Message);
								}
							}
							finally
							{
								sqlDataReader.Close();
							}
						}
						sqlCommand.Dispose();
					}
				}
			}
			catch (Exception exception1)
			{
				SysLog.WriteLog(3, exception1.Message);
			}
			return articleMasterEntities;
		}

		public bool Save(ArticleMasterEntity entity)
		{
			bool flag = false;
			try
			{
				if (ConnectorFactory.Connect())
				{
					string str = "UPDATE tbl_article_master SET ArticleName=@ArticleName, ArticleItemCode=@ArticleItemCode, ArticleItemBarCode=@ArticleItemBarCode, AbbreviatedDosageForm=@AbbreviatedDosageForm, PackageUnit=@PackageUnit, Width=@Width, Length=@Length, Height=@Height, Diameter=@Diameter, Weight=@Weight, PackageSize=@PackageSize, TabOrCapPerBlister=@TabOrCapPerBlister, NoOfBlister=@NoOfBlister, ExpiryDate=@ExpiryDate, IsActive=@IsActive, Notes=@Notes, ReOrderLevel=@ReOrderLevel, TotalQuantity=@TotalQuantity, CreatedBy=@CreatedBy, CreatedOn=@CreatedOn, UpdatedBy=@UpdatedBy, UpdatedOn=@UpdatedOn WHERE ArticleID=@ArticleID";
					using (SqlCommand sqlCommand = new SqlCommand())
					{
						sqlCommand.Connection = ConnectorFactory.Connection;
						sqlCommand.CommandText = str;
						sqlCommand.Prepare();
						sqlCommand.Parameters.AddWithValue("@ArticleID", entity.ArticleID);
						sqlCommand.Parameters.AddWithValue("@ArticleName", entity.ArticleName);
						sqlCommand.Parameters.AddWithValue("@ArticleItemCode", entity.ArticleItemCode);
						sqlCommand.Parameters.AddWithValue("@ArticleItemBarCode", entity.ArticleItemBarCode);
						sqlCommand.Parameters.AddWithValue("@AbbreviatedDosageForm", entity.AbbreviatedDosageForm);
						sqlCommand.Parameters.AddWithValue("@PackageUnit", entity.PackageUnit);
						sqlCommand.Parameters.AddWithValue("@Width", entity.Width);
						sqlCommand.Parameters.AddWithValue("@Length", entity.Length);
						sqlCommand.Parameters.AddWithValue("@Height", entity.Height);
						sqlCommand.Parameters.AddWithValue("@Diameter", entity.Diameter);
						sqlCommand.Parameters.AddWithValue("@Weight", entity.Weight);
						sqlCommand.Parameters.AddWithValue("@PackageSize", entity.PackageSize);
						sqlCommand.Parameters.AddWithValue("@TabOrCapPerBlister", entity.TabOrCapPerBlister);
						sqlCommand.Parameters.AddWithValue("@NoOfBlister", entity.NoOfBlister);
						if (entity.ExpiryDate != DateTime.MinValue)
						{
							sqlCommand.Parameters.AddWithValue("@ExpiryDate", entity.ExpiryDate);
						}
						else
						{
							sqlCommand.Parameters.AddWithValue("@ExpiryDate", DateTime.Now);
						}
						sqlCommand.Parameters.AddWithValue("@IsActive", entity.IsActive);
						sqlCommand.Parameters.AddWithValue("@Notes", entity.Notes);
						sqlCommand.Parameters.AddWithValue("@ReOrderLevel", entity.ReOrderLevel);
						sqlCommand.Parameters.AddWithValue("@TotalQuantity", entity.TotalQuantity);
						sqlCommand.Parameters.AddWithValue("@CreatedBy", entity.CreatedBy);
						if (entity.CreatedOn != DateTime.MinValue)
						{
							sqlCommand.Parameters.AddWithValue("@CreatedOn", entity.CreatedOn);
						}
						else
						{
							sqlCommand.Parameters.AddWithValue("@CreatedOn", DateTime.Now);
						}
						sqlCommand.Parameters.AddWithValue("@UpdatedBy", entity.UpdatedBy);
						if (entity.UpdatedOn != DateTime.MinValue)
						{
							sqlCommand.Parameters.AddWithValue("@UpdatedOn", entity.UpdatedOn);
						}
						else
						{
							sqlCommand.Parameters.AddWithValue("@UpdatedOn", DateTime.Now);
						}
						object obj = sqlCommand.ExecuteNonQuery();
						flag = true;
						sqlCommand.Dispose();
					}
				}
			}
			catch (Exception exception)
			{
				SysLog.WriteLog(3, exception.Message);
			}
			return flag;
		}
	}
}