using MSClassLibrary.Log;
using OPtlDAL.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;

namespace OPtlDAL.DataFactory
{
	internal class PtlOrdersFactory
	{
		public PtlOrdersFactory()
		{
		}

		public bool Add(ref PtlOrdersEntity entity)
		{
			bool flag = false;
			try
			{
				if (ConnectorFactory.Connect())
				{
					string str = "INSERT INTO tbl_ptl_orders (ID, TransactionType, PatientID, PatientName, MedicalRecordNo, ArticleID, Quantity, UserName, Area, TransactionDateTime) VALUES (@ID, @TransactionType, @PatientID, @PatientName, @MedicalRecordNo, @ArticleID, @Quantity, @UserName, @Area, @TransactionDateTime);";
					using (SqlCommand sqlCommand = new SqlCommand())
					{
						sqlCommand.Connection = ConnectorFactory.Connection;
						sqlCommand.CommandText = str;
						sqlCommand.Prepare();
						sqlCommand.Parameters.AddWithValue("@ID", entity.ID);
						sqlCommand.Parameters.AddWithValue("@TransactionType", entity.TransactionType);
						sqlCommand.Parameters.AddWithValue("@PatientID", entity.PatientID);
						sqlCommand.Parameters.AddWithValue("@PatientName", entity.PatientName);
						sqlCommand.Parameters.AddWithValue("@MedicalRecordNo", entity.MedicalRecordNo);
						sqlCommand.Parameters.AddWithValue("@ArticleID", entity.ArticleID);
						sqlCommand.Parameters.AddWithValue("@Quantity", entity.Quantity);
						sqlCommand.Parameters.AddWithValue("@UserName", entity.UserName);
						sqlCommand.Parameters.AddWithValue("@Area", entity.Area);
						if (entity.TransactionDateTime != DateTime.MinValue)
						{
							sqlCommand.Parameters.AddWithValue("@TransactionDateTime", entity.TransactionDateTime);
						}
						else
						{
							sqlCommand.Parameters.AddWithValue("@TransactionDateTime", DateTime.Now);
						}
						sqlCommand.ExecuteScalar();
						flag = true;
						sqlCommand.Dispose();
					}
				}
			}
			catch (Exception exception)
			{
				SysLog.WriteLog(3, exception.Message);
			}
			return flag;
		}

		public bool Delete(PtlOrdersEntity entity)
		{
			bool flag = false;
			try
			{
				if (ConnectorFactory.Connect())
				{
					string str = "DELETE FROM tbl_ptl_orders WHERE ID=@ID";
					using (SqlCommand sqlCommand = new SqlCommand())
					{
						sqlCommand.Connection = ConnectorFactory.Connection;
						sqlCommand.CommandText = str;
						sqlCommand.Prepare();
						sqlCommand.Parameters.AddWithValue("@ID", entity.ID);
						object obj = sqlCommand.ExecuteNonQuery();
						flag = true;
						sqlCommand.Dispose();
					}
				}
			}
			catch (Exception exception)
			{
				SysLog.WriteLog(3, exception.Message);
			}
			return flag;
		}

		public PtlOrdersEntity Get(Guid ID)
		{
			PtlOrdersEntity ptlOrdersEntity = null;
			try
			{
				if (ConnectorFactory.Connect())
				{
					string str = "SELECT ID, TransactionType, PatientID, PatientName, MedicalRecordNo, ArticleID, Quantity, UserName, Area, TransactionDateTime FROM tbl_ptl_orders";
					str = string.Concat(str, " WHERE ID=@ID;");
					using (SqlCommand sqlCommand = new SqlCommand())
					{
						sqlCommand.Connection = ConnectorFactory.Connection;
						sqlCommand.CommandText = str;
						sqlCommand.Prepare();
						sqlCommand.Parameters.AddWithValue("@ID", ID);
						using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
						{
							try
							{
								try
								{
									if (sqlDataReader.Read())
									{
										ptlOrdersEntity = new PtlOrdersEntity();
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ID")))
										{
											ptlOrdersEntity.ID = sqlDataReader.GetGuid(sqlDataReader.GetOrdinal("ID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("TransactionType")))
										{
											ptlOrdersEntity.TransactionType = sqlDataReader.GetString(sqlDataReader.GetOrdinal("TransactionType"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("PatientID")))
										{
											ptlOrdersEntity.PatientID = sqlDataReader.GetString(sqlDataReader.GetOrdinal("PatientID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("PatientName")))
										{
											ptlOrdersEntity.PatientName = sqlDataReader.GetString(sqlDataReader.GetOrdinal("PatientName"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("MedicalRecordNo")))
										{
											ptlOrdersEntity.MedicalRecordNo = sqlDataReader.GetString(sqlDataReader.GetOrdinal("MedicalRecordNo"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ArticleID")))
										{
											ptlOrdersEntity.ArticleID = sqlDataReader.GetString(sqlDataReader.GetOrdinal("ArticleID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("Quantity")))
										{
											ptlOrdersEntity.Quantity = sqlDataReader.GetDecimal(sqlDataReader.GetOrdinal("Quantity"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("UserName")))
										{
											ptlOrdersEntity.UserName = sqlDataReader.GetString(sqlDataReader.GetOrdinal("UserName"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("Area")))
										{
											ptlOrdersEntity.Area = sqlDataReader.GetString(sqlDataReader.GetOrdinal("Area"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("TransactionDateTime")))
										{
											ptlOrdersEntity.TransactionDateTime = sqlDataReader.GetDateTime(sqlDataReader.GetOrdinal("TransactionDateTime"));
										}
									}
								}
								catch (Exception exception)
								{
									SysLog.WriteLog(3, exception.Message);
								}
							}
							finally
							{
								sqlDataReader.Close();
							}
						}
						sqlCommand.Dispose();
					}
				}
			}
			catch (Exception exception1)
			{
				SysLog.WriteLog(3, exception1.Message);
			}
			return ptlOrdersEntity;
		}

		public List<PtlOrdersEntity> GetList(Guid ID)
		{
			List<PtlOrdersEntity> ptlOrdersEntities = new List<PtlOrdersEntity>();
			try
			{
				if (ConnectorFactory.Connect())
				{
					string str = "SELECT ID, TransactionType, PatientID, PatientName, MedicalRecordNo, ArticleID, Quantity, UserName, Area, TransactionDateTime FROM tbl_ptl_orders";
					str = string.Concat(str, " WHERE ID=@ID;");
					using (SqlCommand sqlCommand = new SqlCommand())
					{
						sqlCommand.Connection = ConnectorFactory.Connection;
						sqlCommand.CommandText = str;
						sqlCommand.Prepare();
						sqlCommand.Parameters.AddWithValue("@ID", ID);
						using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
						{
							try
							{
								try
								{
									while (sqlDataReader.Read())
									{
										PtlOrdersEntity ptlOrdersEntity = new PtlOrdersEntity();
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ID")))
										{
											ptlOrdersEntity.ID = sqlDataReader.GetGuid(sqlDataReader.GetOrdinal("ID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("TransactionType")))
										{
											ptlOrdersEntity.TransactionType = sqlDataReader.GetString(sqlDataReader.GetOrdinal("TransactionType"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("PatientID")))
										{
											ptlOrdersEntity.PatientID = sqlDataReader.GetString(sqlDataReader.GetOrdinal("PatientID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("PatientName")))
										{
											ptlOrdersEntity.PatientName = sqlDataReader.GetString(sqlDataReader.GetOrdinal("PatientName"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("MedicalRecordNo")))
										{
											ptlOrdersEntity.MedicalRecordNo = sqlDataReader.GetString(sqlDataReader.GetOrdinal("MedicalRecordNo"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ArticleID")))
										{
											ptlOrdersEntity.ArticleID = sqlDataReader.GetString(sqlDataReader.GetOrdinal("ArticleID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("Quantity")))
										{
											ptlOrdersEntity.Quantity = sqlDataReader.GetDecimal(sqlDataReader.GetOrdinal("Quantity"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("UserName")))
										{
											ptlOrdersEntity.UserName = sqlDataReader.GetString(sqlDataReader.GetOrdinal("UserName"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("Area")))
										{
											ptlOrdersEntity.Area = sqlDataReader.GetString(sqlDataReader.GetOrdinal("Area"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("TransactionDateTime")))
										{
											ptlOrdersEntity.TransactionDateTime = sqlDataReader.GetDateTime(sqlDataReader.GetOrdinal("TransactionDateTime"));
										}
										ptlOrdersEntities.Add(ptlOrdersEntity);
									}
								}
								catch (Exception exception)
								{
									SysLog.WriteLog(3, exception.Message);
								}
							}
							finally
							{
								sqlDataReader.Close();
							}
						}
						sqlCommand.Dispose();
					}
				}
			}
			catch (Exception exception1)
			{
				SysLog.WriteLog(3, exception1.Message);
			}
			return ptlOrdersEntities;
		}

		public bool Save(PtlOrdersEntity entity)
		{
			bool flag = false;
			try
			{
				if (ConnectorFactory.Connect())
				{
					string str = "UPDATE tbl_ptl_orders SET TransactionType=@TransactionType, PatientID=@PatientID, PatientName=@PatientName, MedicalRecordNo=@MedicalRecordNo, ArticleID=@ArticleID, Quantity=@Quantity, UserName=@UserName, Area=@Area, TransactionDateTime=@TransactionDateTime WHERE ID=@ID";
					using (SqlCommand sqlCommand = new SqlCommand())
					{
						sqlCommand.Connection = ConnectorFactory.Connection;
						sqlCommand.CommandText = str;
						sqlCommand.Prepare();
						sqlCommand.Parameters.AddWithValue("@ID", entity.ID);
						sqlCommand.Parameters.AddWithValue("@TransactionType", entity.TransactionType);
						sqlCommand.Parameters.AddWithValue("@PatientID", entity.PatientID);
						sqlCommand.Parameters.AddWithValue("@PatientName", entity.PatientName);
						sqlCommand.Parameters.AddWithValue("@MedicalRecordNo", entity.MedicalRecordNo);
						sqlCommand.Parameters.AddWithValue("@ArticleID", entity.ArticleID);
						sqlCommand.Parameters.AddWithValue("@Quantity", entity.Quantity);
						sqlCommand.Parameters.AddWithValue("@UserName", entity.UserName);
						sqlCommand.Parameters.AddWithValue("@Area", entity.Area);
						if (entity.TransactionDateTime != DateTime.MinValue)
						{
							sqlCommand.Parameters.AddWithValue("@TransactionDateTime", entity.TransactionDateTime);
						}
						else
						{
							sqlCommand.Parameters.AddWithValue("@TransactionDateTime", DateTime.Now);
						}
						object obj = sqlCommand.ExecuteNonQuery();
						flag = true;
						sqlCommand.Dispose();
					}
				}
			}
			catch (Exception exception)
			{
				SysLog.WriteLog(3, exception.Message);
			}
			return flag;
		}
	}
}