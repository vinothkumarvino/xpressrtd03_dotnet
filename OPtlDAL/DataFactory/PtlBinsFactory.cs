using MSClassLibrary.Log;
using OPtlDAL.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;

namespace OPtlDAL.DataFactory
{
	internal class PtlBinsFactory
	{
		public PtlBinsFactory()
		{
		}

		public bool Add(ref PtlBinsEntity entity)
		{
			bool flag = false;
			try
			{
				if (ConnectorFactory.Connect())
				{
					string str = "INSERT INTO tbl_ptl_bins (BinID, SystemName, ModuleID, ModuleColor, ModuleFlash, SRC, ArticleID, AvailableQuantity, ReOrderLevel, PickQuantity, PickLightStatus, StockQuantity, StockLightStatus) VALUES (@BinID, @SystemName, @ModuleID, @ModuleColor, @ModuleFlash, @SRC, @ArticleID, @AvailableQuantity, @ReOrderLevel, @PickQuantity, @PickLightStatus, @StockQuantity, @StockLightStatus);";
					using (SqlCommand sqlCommand = new SqlCommand())
					{
						sqlCommand.Connection = ConnectorFactory.Connection;
						sqlCommand.CommandText = str;
						sqlCommand.Prepare();
						sqlCommand.Parameters.AddWithValue("@BinID", entity.BinID);
						sqlCommand.Parameters.AddWithValue("@SystemName", entity.SystemName);
						sqlCommand.Parameters.AddWithValue("@ModuleID", entity.ModuleID);
						sqlCommand.Parameters.AddWithValue("@ModuleColor", entity.ModuleColor);
						sqlCommand.Parameters.AddWithValue("@ModuleFlash", entity.ModuleFlash);
						sqlCommand.Parameters.AddWithValue("@SRC", entity.SRC);
						sqlCommand.Parameters.AddWithValue("@ArticleID", entity.ArticleID);
						sqlCommand.Parameters.AddWithValue("@AvailableQuantity", entity.AvailableQuantity);
						sqlCommand.Parameters.AddWithValue("@ReOrderLevel", entity.ReOrderLevel);
						sqlCommand.Parameters.AddWithValue("@PickQuantity", entity.PickQuantity);
						sqlCommand.Parameters.AddWithValue("@PickLightStatus", entity.PickLightStatus);
						sqlCommand.Parameters.AddWithValue("@StockQuantity", entity.StockQuantity);
						sqlCommand.Parameters.AddWithValue("@StockLightStatus", entity.StockLightStatus);
						sqlCommand.ExecuteScalar();
						flag = true;
						sqlCommand.Dispose();
					}
				}
			}
			catch (Exception exception)
			{
				SysLog.WriteLog(3, exception.Message);
			}
			return flag;
		}

		public bool Delete(PtlBinsEntity entity)
		{
			bool flag = false;
			try
			{
				if (ConnectorFactory.Connect())
				{
					string str = "DELETE FROM tbl_ptl_bins WHERE BinID=@BinID";
					using (SqlCommand sqlCommand = new SqlCommand())
					{
						sqlCommand.Connection = ConnectorFactory.Connection;
						sqlCommand.CommandText = str;
						sqlCommand.Prepare();
						sqlCommand.Parameters.AddWithValue("@BinID", entity.BinID);
						object obj = sqlCommand.ExecuteNonQuery();
						flag = true;
						sqlCommand.Dispose();
					}
				}
			}
			catch (Exception exception)
			{
				SysLog.WriteLog(3, exception.Message);
			}
			return flag;
		}

		public PtlBinsEntity Get(Guid binID)
		{
			PtlBinsEntity ptlBinsEntity = null;
			try
			{
				if (ConnectorFactory.Connect())
				{
					string str = "SELECT BinID, SystemName, ModuleID, ModuleColor, ModuleFlash, SRC, ArticleID, AvailableQuantity, ReOrderLevel, PickQuantity, PickLightStatus, StockQuantity, StockLightStatus FROM tbl_ptl_bins";
					str = string.Concat(str, " WHERE BinID=@BinID;");
					using (SqlCommand sqlCommand = new SqlCommand())
					{
						sqlCommand.Connection = ConnectorFactory.Connection;
						sqlCommand.CommandText = str;
						sqlCommand.Prepare();
						sqlCommand.Parameters.AddWithValue("@BinID", binID);
						using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
						{
							try
							{
								try
								{
									if (sqlDataReader.Read())
									{
										ptlBinsEntity = new PtlBinsEntity();
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("BinID")))
										{
											ptlBinsEntity.BinID = sqlDataReader.GetGuid(sqlDataReader.GetOrdinal("BinID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("SystemName")))
										{
											ptlBinsEntity.SystemName = sqlDataReader.GetString(sqlDataReader.GetOrdinal("SystemName"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ModuleID")))
										{
											ptlBinsEntity.ModuleID = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("ModuleID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ModuleColor")))
										{
											ptlBinsEntity.ModuleColor = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("ModuleColor"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ModuleFlash")))
										{
											ptlBinsEntity.ModuleFlash = sqlDataReader.GetBoolean(sqlDataReader.GetOrdinal("ModuleFlash"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("SRC")))
										{
											ptlBinsEntity.SRC = sqlDataReader.GetString(sqlDataReader.GetOrdinal("SRC"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ArticleID")))
										{
											ptlBinsEntity.ArticleID = sqlDataReader.GetString(sqlDataReader.GetOrdinal("ArticleID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("AvailableQuantity")))
										{
											ptlBinsEntity.AvailableQuantity = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("AvailableQuantity"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ReOrderLevel")))
										{
											ptlBinsEntity.ReOrderLevel = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("ReOrderLevel"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("PickQuantity")))
										{
											ptlBinsEntity.PickQuantity = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("PickQuantity"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("PickLightStatus")))
										{
											ptlBinsEntity.PickLightStatus = sqlDataReader.GetBoolean(sqlDataReader.GetOrdinal("PickLightStatus"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("StockQuantity")))
										{
											ptlBinsEntity.StockQuantity = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("StockQuantity"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("StockLightStatus")))
										{
											ptlBinsEntity.StockLightStatus = sqlDataReader.GetBoolean(sqlDataReader.GetOrdinal("StockLightStatus"));
										}
									}
								}
								catch (Exception exception)
								{
									SysLog.WriteLog(3, exception.Message);
								}
							}
							finally
							{
								sqlDataReader.Close();
							}
						}
						sqlCommand.Dispose();
					}
				}
			}
			catch (Exception exception1)
			{
				SysLog.WriteLog(3, exception1.Message);
			}
			return ptlBinsEntity;
		}

		public PtlBinsEntity Get(string srcReference)
		{
			PtlBinsEntity ptlBinsEntity = null;
			try
			{
				if (ConnectorFactory.Connect())
				{
					string str = "SELECT BinID, SystemName, ModuleID, ModuleColor, ModuleFlash, SRC, ArticleID, AvailableQuantity, ReOrderLevel, PickQuantity, PickLightStatus, StockQuantity, StockLightStatus FROM tbl_ptl_bins";
					str = string.Concat(str, " WHERE SRC=@SRC;");
					using (SqlCommand sqlCommand = new SqlCommand())
					{
						sqlCommand.Connection = ConnectorFactory.Connection;
						sqlCommand.CommandText = str;
						sqlCommand.Prepare();
						sqlCommand.Parameters.AddWithValue("@SRC", srcReference);
						using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
						{
							try
							{
								try
								{
									if (sqlDataReader.Read())
									{
										ptlBinsEntity = new PtlBinsEntity();
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("BinID")))
										{
											ptlBinsEntity.BinID = sqlDataReader.GetGuid(sqlDataReader.GetOrdinal("BinID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("SystemName")))
										{
											ptlBinsEntity.SystemName = sqlDataReader.GetString(sqlDataReader.GetOrdinal("SystemName"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ModuleID")))
										{
											ptlBinsEntity.ModuleID = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("ModuleID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ModuleColor")))
										{
											ptlBinsEntity.ModuleColor = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("ModuleColor"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ModuleFlash")))
										{
											ptlBinsEntity.ModuleFlash = sqlDataReader.GetBoolean(sqlDataReader.GetOrdinal("ModuleFlash"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("SRC")))
										{
											ptlBinsEntity.SRC = sqlDataReader.GetString(sqlDataReader.GetOrdinal("SRC"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ArticleID")))
										{
											ptlBinsEntity.ArticleID = sqlDataReader.GetString(sqlDataReader.GetOrdinal("ArticleID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("AvailableQuantity")))
										{
											ptlBinsEntity.AvailableQuantity = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("AvailableQuantity"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ReOrderLevel")))
										{
											ptlBinsEntity.ReOrderLevel = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("ReOrderLevel"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("PickQuantity")))
										{
											ptlBinsEntity.PickQuantity = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("PickQuantity"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("PickLightStatus")))
										{
											ptlBinsEntity.PickLightStatus = sqlDataReader.GetBoolean(sqlDataReader.GetOrdinal("PickLightStatus"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("StockQuantity")))
										{
											ptlBinsEntity.StockQuantity = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("StockQuantity"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("StockLightStatus")))
										{
											ptlBinsEntity.StockLightStatus = sqlDataReader.GetBoolean(sqlDataReader.GetOrdinal("StockLightStatus"));
										}
									}
								}
								catch (Exception exception)
								{
									SysLog.WriteLog(3, exception.Message);
								}
							}
							finally
							{
								sqlDataReader.Close();
							}
						}
						sqlCommand.Dispose();
					}
				}
			}
			catch (Exception exception1)
			{
				SysLog.WriteLog(3, exception1.Message);
			}
			return ptlBinsEntity;
		}

		public PtlBinsEntity Get(string systemName, int moduleID)
		{
			PtlBinsEntity ptlBinsEntity = null;
			try
			{
				if (ConnectorFactory.Connect())
				{
					string str = "SELECT BinID, SystemName, ModuleID, ModuleColor, ModuleFlash, SRC, ArticleID, AvailableQuantity, ReOrderLevel, PickQuantity, PickLightStatus, StockQuantity, StockLightStatus FROM tbl_ptl_bins";
					str = string.Concat(str, " WHERE SystemName=@SystemName AND ModuleID=@ModuleID;");
					using (SqlCommand sqlCommand = new SqlCommand())
					{
						sqlCommand.Connection = ConnectorFactory.Connection;
						sqlCommand.CommandText = str;
						sqlCommand.Prepare();
						sqlCommand.Parameters.AddWithValue("@SystemName", systemName);
						sqlCommand.Parameters.AddWithValue("@ModuleID", moduleID);
						using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
						{
							try
							{
								try
								{
									if (sqlDataReader.Read())
									{
										ptlBinsEntity = new PtlBinsEntity();
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("BinID")))
										{
											ptlBinsEntity.BinID = sqlDataReader.GetGuid(sqlDataReader.GetOrdinal("BinID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("SystemName")))
										{
											ptlBinsEntity.SystemName = sqlDataReader.GetString(sqlDataReader.GetOrdinal("SystemName"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ModuleID")))
										{
											ptlBinsEntity.ModuleID = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("ModuleID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ModuleColor")))
										{
											ptlBinsEntity.ModuleColor = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("ModuleColor"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ModuleFlash")))
										{
											ptlBinsEntity.ModuleFlash = sqlDataReader.GetBoolean(sqlDataReader.GetOrdinal("ModuleFlash"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("SRC")))
										{
											ptlBinsEntity.SRC = sqlDataReader.GetString(sqlDataReader.GetOrdinal("SRC"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ArticleID")))
										{
											ptlBinsEntity.ArticleID = sqlDataReader.GetString(sqlDataReader.GetOrdinal("ArticleID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("AvailableQuantity")))
										{
											ptlBinsEntity.AvailableQuantity = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("AvailableQuantity"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ReOrderLevel")))
										{
											ptlBinsEntity.ReOrderLevel = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("ReOrderLevel"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("PickQuantity")))
										{
											ptlBinsEntity.PickQuantity = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("PickQuantity"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("PickLightStatus")))
										{
											ptlBinsEntity.PickLightStatus = sqlDataReader.GetBoolean(sqlDataReader.GetOrdinal("PickLightStatus"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("StockQuantity")))
										{
											ptlBinsEntity.StockQuantity = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("StockQuantity"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("StockLightStatus")))
										{
											ptlBinsEntity.StockLightStatus = sqlDataReader.GetBoolean(sqlDataReader.GetOrdinal("StockLightStatus"));
										}
									}
								}
								catch (Exception exception)
								{
									SysLog.WriteLog(3, exception.Message);
								}
							}
							finally
							{
								sqlDataReader.Close();
							}
						}
						sqlCommand.Dispose();
					}
				}
			}
			catch (Exception exception1)
			{
				SysLog.WriteLog(3, exception1.Message);
			}
			return ptlBinsEntity;
		}

		public PtlBinsEntity GetLightModule(int moduleID)
		{
			PtlBinsEntity ptlBinsEntity = null;
			try
			{
				if (ConnectorFactory.Connect())
				{
					string str = "SELECT BinID, SystemName, ModuleID, ModuleColor, ModuleFlash, SRC, ArticleID, AvailableQuantity, ReOrderLevel, PickQuantity, PickLightStatus, StockQuantity, StockLightStatus FROM tbl_ptl_bins";
					str = string.Concat(str, " WHERE ModuleID=@ModuleID;");
					using (SqlCommand sqlCommand = new SqlCommand())
					{
						sqlCommand.Connection = ConnectorFactory.Connection;
						sqlCommand.CommandText = str;
						sqlCommand.Prepare();
						sqlCommand.Parameters.AddWithValue("@ModuleID", moduleID);
						using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
						{
							try
							{
								try
								{
									if (sqlDataReader.Read())
									{
										ptlBinsEntity = new PtlBinsEntity();
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("BinID")))
										{
											ptlBinsEntity.BinID = sqlDataReader.GetGuid(sqlDataReader.GetOrdinal("BinID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("SystemName")))
										{
											ptlBinsEntity.SystemName = sqlDataReader.GetString(sqlDataReader.GetOrdinal("SystemName"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ModuleID")))
										{
											ptlBinsEntity.ModuleID = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("ModuleID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ModuleColor")))
										{
											ptlBinsEntity.ModuleColor = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("ModuleColor"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ModuleFlash")))
										{
											ptlBinsEntity.ModuleFlash = sqlDataReader.GetBoolean(sqlDataReader.GetOrdinal("ModuleFlash"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("SRC")))
										{
											ptlBinsEntity.SRC = sqlDataReader.GetString(sqlDataReader.GetOrdinal("SRC"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ArticleID")))
										{
											ptlBinsEntity.ArticleID = sqlDataReader.GetString(sqlDataReader.GetOrdinal("ArticleID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("AvailableQuantity")))
										{
											ptlBinsEntity.AvailableQuantity = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("AvailableQuantity"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ReOrderLevel")))
										{
											ptlBinsEntity.ReOrderLevel = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("ReOrderLevel"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("PickQuantity")))
										{
											ptlBinsEntity.PickQuantity = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("PickQuantity"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("PickLightStatus")))
										{
											ptlBinsEntity.PickLightStatus = sqlDataReader.GetBoolean(sqlDataReader.GetOrdinal("PickLightStatus"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("StockQuantity")))
										{
											ptlBinsEntity.StockQuantity = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("StockQuantity"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("StockLightStatus")))
										{
											ptlBinsEntity.StockLightStatus = sqlDataReader.GetBoolean(sqlDataReader.GetOrdinal("StockLightStatus"));
										}
									}
								}
								catch (Exception exception)
								{
									SysLog.WriteLog(3, exception.Message);
								}
							}
							finally
							{
								sqlDataReader.Close();
							}
						}
						sqlCommand.Dispose();
					}
				}
			}
			catch (Exception exception1)
			{
				SysLog.WriteLog(3, exception1.Message);
			}
			return ptlBinsEntity;
		}

		public List<PtlBinsEntity> GetList(Guid SystemName)
		{
			List<PtlBinsEntity> ptlBinsEntities = new List<PtlBinsEntity>();
			try
			{
				if (ConnectorFactory.Connect())
				{
					string str = "SELECT BinID, SystemName, ModuleID, ModuleColor, ModuleFlash, SRC, ArticleID, AvailableQuantity, ReOrderLevel, PickQuantity, PickLightStatus, StockQuantity, StockLightStatus FROM tbl_ptl_bins";
					str = string.Concat(str, " WHERE SystemName=@SystemName;");
					using (SqlCommand sqlCommand = new SqlCommand())
					{
						sqlCommand.Connection = ConnectorFactory.Connection;
						sqlCommand.CommandText = str;
						sqlCommand.Prepare();
						sqlCommand.Parameters.AddWithValue("@SystemName", SystemName);
						using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
						{
							try
							{
								try
								{
									while (sqlDataReader.Read())
									{
										PtlBinsEntity ptlBinsEntity = new PtlBinsEntity();
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("BinID")))
										{
											ptlBinsEntity.BinID = sqlDataReader.GetGuid(sqlDataReader.GetOrdinal("BinID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("SystemName")))
										{
											ptlBinsEntity.SystemName = sqlDataReader.GetString(sqlDataReader.GetOrdinal("SystemName"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ModuleID")))
										{
											ptlBinsEntity.ModuleID = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("ModuleID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ModuleColor")))
										{
											ptlBinsEntity.ModuleColor = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("ModuleColor"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ModuleFlash")))
										{
											ptlBinsEntity.ModuleFlash = sqlDataReader.GetBoolean(sqlDataReader.GetOrdinal("ModuleFlash"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("SRC")))
										{
											ptlBinsEntity.SRC = sqlDataReader.GetString(sqlDataReader.GetOrdinal("SRC"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ArticleID")))
										{
											ptlBinsEntity.ArticleID = sqlDataReader.GetString(sqlDataReader.GetOrdinal("ArticleID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("AvailableQuantity")))
										{
											ptlBinsEntity.AvailableQuantity = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("AvailableQuantity"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ReOrderLevel")))
										{
											ptlBinsEntity.ReOrderLevel = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("ReOrderLevel"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("PickQuantity")))
										{
											ptlBinsEntity.PickQuantity = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("PickQuantity"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("PickLightStatus")))
										{
											ptlBinsEntity.PickLightStatus = sqlDataReader.GetBoolean(sqlDataReader.GetOrdinal("PickLightStatus"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("StockQuantity")))
										{
											ptlBinsEntity.StockQuantity = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("StockQuantity"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("StockLightStatus")))
										{
											ptlBinsEntity.StockLightStatus = sqlDataReader.GetBoolean(sqlDataReader.GetOrdinal("StockLightStatus"));
										}
										ptlBinsEntities.Add(ptlBinsEntity);
									}
								}
								catch (Exception exception)
								{
									SysLog.WriteLog(3, exception.Message);
								}
							}
							finally
							{
								sqlDataReader.Close();
							}
						}
						sqlCommand.Dispose();
					}
				}
			}
			catch (Exception exception1)
			{
				SysLog.WriteLog(3, exception1.Message);
			}
			return ptlBinsEntities;
		}

		public bool Save(PtlBinsEntity entity)
		{
			bool flag = false;
			try
			{
				if (ConnectorFactory.Connect())
				{
					string str = "UPDATE tbl_ptl_bins SET SystemName=@SystemName, ModuleID=@ModuleID, ModuleColor=@ModuleColor, ModuleFlash=@ModuleFlash, SRC=@SRC, ArticleID=@ArticleID, AvailableQuantity=@AvailableQuantity, ReOrderLevel=@ReOrderLevel, PickQuantity=@PickQuantity, PickLightStatus=@PickLightStatus, StockQuantity=@StockQuantity, StockLightStatus=@StockLightStatus WHERE BinID=@BinID";
					using (SqlCommand sqlCommand = new SqlCommand())
					{
						sqlCommand.Connection = ConnectorFactory.Connection;
						sqlCommand.CommandText = str;
						sqlCommand.Prepare();
						sqlCommand.Parameters.AddWithValue("@BinID", entity.BinID);
						sqlCommand.Parameters.AddWithValue("@SystemName", entity.SystemName);
						sqlCommand.Parameters.AddWithValue("@ModuleID", entity.ModuleID);
						sqlCommand.Parameters.AddWithValue("@ModuleColor", entity.ModuleColor);
						sqlCommand.Parameters.AddWithValue("@ModuleFlash", entity.ModuleFlash);
						sqlCommand.Parameters.AddWithValue("@SRC", entity.SRC);
						sqlCommand.Parameters.AddWithValue("@ArticleID", entity.ArticleID);
						sqlCommand.Parameters.AddWithValue("@AvailableQuantity", entity.AvailableQuantity);
						sqlCommand.Parameters.AddWithValue("@ReOrderLevel", entity.ReOrderLevel);
						sqlCommand.Parameters.AddWithValue("@PickQuantity", entity.PickQuantity);
						sqlCommand.Parameters.AddWithValue("@PickLightStatus", entity.PickLightStatus);
						sqlCommand.Parameters.AddWithValue("@StockQuantity", entity.StockQuantity);
						sqlCommand.Parameters.AddWithValue("@StockLightStatus", entity.StockLightStatus);
						object obj = sqlCommand.ExecuteNonQuery();
						flag = true;
						sqlCommand.Dispose();
					}
				}
			}
			catch (Exception exception)
			{
				SysLog.WriteLog(3, exception.Message);
			}
			return flag;
		}
	}
}