using MSClassLibrary.Log;
using OPtlDAL.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;

namespace OPtlDAL.DataFactory
{
	internal class PtlBinHistoryFactory
	{
		public PtlBinHistoryFactory()
		{
		}

		public bool Add(ref PtlBinHistoryEntity entity)
		{
			bool flag = false;
			try
			{
				if (ConnectorFactory.Connect())
				{
					string str = "INSERT INTO tbl_ptl_bin_history (ID, SystemName, BinID, SRC, ModuleID, ModuleColor, ModuleFlash, ModuleBuzzer, IsLightCommandIssued, DisplayContent, ArticleID, StockIn, StockOut, Adjustment, Remarks, RequestID, RequestCode, LightOnOffCommand, OnTimeStamp, TurnOffByButtonPress, TurnOffByCommand, OffTimeStamp, UpdatedBy, UpdatedOn, CreatedBy, CreatedOn) VALUES (@ID, @SystemName, @BinID, @SRC, @ModuleID, @ModuleColor, @ModuleFlash, @ModuleBuzzer, @IsLightCommandIssued, @DisplayContent, @ArticleID, @StockIn, @StockOut, @Adjustment, @Remarks, @RequestID, @RequestCode, @LightOnOffCommand, @OnTimeStamp, @TurnOffByButtonPress, @TurnOffByCommand, @OffTimeStamp, @UpdatedBy, @UpdatedOn, @CreatedBy, @CreatedOn);";
					using (SqlCommand sqlCommand = new SqlCommand())
					{
						sqlCommand.Connection = ConnectorFactory.Connection;
						sqlCommand.CommandText = str;
						sqlCommand.Prepare();
						sqlCommand.Parameters.AddWithValue("@ID", entity.ID);
						sqlCommand.Parameters.AddWithValue("@SystemName", entity.SystemName);
						sqlCommand.Parameters.AddWithValue("@BinID", entity.BinID);
						sqlCommand.Parameters.AddWithValue("@SRC", entity.SRC);
						sqlCommand.Parameters.AddWithValue("@ModuleID", entity.ModuleID);
						sqlCommand.Parameters.AddWithValue("@ModuleColor", entity.ModuleColor);
						sqlCommand.Parameters.AddWithValue("@ModuleFlash", entity.ModuleFlash);
						sqlCommand.Parameters.AddWithValue("@ModuleBuzzer", entity.ModuleBuzzer);
						sqlCommand.Parameters.AddWithValue("@IsLightCommandIssued", entity.IsLightCommandIssued);
						sqlCommand.Parameters.AddWithValue("@DisplayContent", entity.DisplayContent);
						sqlCommand.Parameters.AddWithValue("@ArticleID", entity.ArticleID);
						sqlCommand.Parameters.AddWithValue("@StockIn", entity.StockIn);
						sqlCommand.Parameters.AddWithValue("@StockOut", entity.StockOut);
						sqlCommand.Parameters.AddWithValue("@Adjustment", entity.Adjustment);
						sqlCommand.Parameters.AddWithValue("@Remarks", entity.Remarks);
						sqlCommand.Parameters.AddWithValue("@RequestID", entity.RequestID);
						sqlCommand.Parameters.AddWithValue("@RequestCode", entity.RequestCode);
						sqlCommand.Parameters.AddWithValue("@LightOnOffCommand", entity.LightOnOffCommand);
						if (entity.OnTimeStamp != DateTime.MinValue)
						{
							sqlCommand.Parameters.AddWithValue("@OnTimeStamp", entity.OnTimeStamp);
						}
						else
						{
							sqlCommand.Parameters.AddWithValue("@OnTimeStamp", DateTime.Now);
						}
						sqlCommand.Parameters.AddWithValue("@TurnOffByButtonPress", entity.TurnOffByButtonPress);
						sqlCommand.Parameters.AddWithValue("@TurnOffByCommand", entity.TurnOffByCommand);
						if (entity.OffTimeStamp != DateTime.MinValue)
						{
							sqlCommand.Parameters.AddWithValue("@OffTimeStamp", entity.OffTimeStamp);
						}
						else
						{
							sqlCommand.Parameters.AddWithValue("@OffTimeStamp", DateTime.Now);
						}
						sqlCommand.Parameters.AddWithValue("@UpdatedBy", entity.UpdatedBy);
						if (entity.UpdatedOn != DateTime.MinValue)
						{
							sqlCommand.Parameters.AddWithValue("@UpdatedOn", entity.UpdatedOn);
						}
						else
						{
							sqlCommand.Parameters.AddWithValue("@UpdatedOn", DateTime.Now);
						}
						sqlCommand.Parameters.AddWithValue("@CreatedBy", entity.CreatedBy);
						if (entity.CreatedOn != DateTime.MinValue)
						{
							sqlCommand.Parameters.AddWithValue("@CreatedOn", entity.CreatedOn);
						}
						else
						{
							sqlCommand.Parameters.AddWithValue("@CreatedOn", DateTime.Now);
						}
						sqlCommand.ExecuteScalar();
						flag = true;
						sqlCommand.Dispose();
					}
				}
			}
			catch (Exception exception)
			{
				SysLog.WriteLog(3, exception.Message);
			}
			return flag;
		}

		public bool Delete(PtlBinHistoryEntity entity)
		{
			bool flag = false;
			try
			{
				if (ConnectorFactory.Connect())
				{
					string str = "DELETE FROM tbl_ptl_bin_history WHERE ID=@ID";
					using (SqlCommand sqlCommand = new SqlCommand())
					{
						sqlCommand.Connection = ConnectorFactory.Connection;
						sqlCommand.CommandText = str;
						sqlCommand.Prepare();
						sqlCommand.Parameters.AddWithValue("@ID", entity.ID);
						object obj = sqlCommand.ExecuteNonQuery();
						flag = true;
						sqlCommand.Dispose();
					}
				}
			}
			catch (Exception exception)
			{
				SysLog.WriteLog(3, exception.Message);
			}
			return flag;
		}

		public PtlBinHistoryEntity Get(Guid ID)
		{
			PtlBinHistoryEntity ptlBinHistoryEntity = null;
			try
			{
				if (ConnectorFactory.Connect())
				{
					string str = "SELECT ID, SystemName, BinID, SRC, ModuleID, ModuleColor, ModuleFlash, ModuleBuzzer, IsLightCommandIssued, DisplayContent, ArticleID, StockIn, StockOut, Adjustment, Remarks, RequestID, RequestCode, LightOnOffCommand, OnTimeStamp, TurnOffByButtonPress, TurnOffByCommand, OffTimeStamp, UpdatedBy, UpdatedOn, CreatedBy, CreatedOn FROM tbl_ptl_bin_history";
					str = string.Concat(str, " WHERE ID=@ID;");
					using (SqlCommand sqlCommand = new SqlCommand())
					{
						sqlCommand.Connection = ConnectorFactory.Connection;
						sqlCommand.CommandText = str;
						sqlCommand.Prepare();
						sqlCommand.Parameters.AddWithValue("@ID", ID);
						using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
						{
							try
							{
								try
								{
									if (sqlDataReader.Read())
									{
										ptlBinHistoryEntity = new PtlBinHistoryEntity();
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ID")))
										{
											ptlBinHistoryEntity.ID = sqlDataReader.GetGuid(sqlDataReader.GetOrdinal("ID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("SystemName")))
										{
											ptlBinHistoryEntity.SystemName = sqlDataReader.GetString(sqlDataReader.GetOrdinal("SystemName"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("BinID")))
										{
											ptlBinHistoryEntity.BinID = sqlDataReader.GetGuid(sqlDataReader.GetOrdinal("BinID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("SRC")))
										{
											ptlBinHistoryEntity.SRC = sqlDataReader.GetString(sqlDataReader.GetOrdinal("SRC"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ModuleID")))
										{
											ptlBinHistoryEntity.ModuleID = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("ModuleID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ModuleColor")))
										{
											ptlBinHistoryEntity.ModuleColor = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("ModuleColor"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ModuleFlash")))
										{
											ptlBinHistoryEntity.ModuleFlash = sqlDataReader.GetBoolean(sqlDataReader.GetOrdinal("ModuleFlash"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ModuleBuzzer")))
										{
											ptlBinHistoryEntity.ModuleBuzzer = sqlDataReader.GetBoolean(sqlDataReader.GetOrdinal("ModuleBuzzer"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("IsLightCommandIssued")))
										{
											ptlBinHistoryEntity.IsLightCommandIssued = sqlDataReader.GetBoolean(sqlDataReader.GetOrdinal("IsLightCommandIssued"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("DisplayContent")))
										{
											ptlBinHistoryEntity.DisplayContent = sqlDataReader.GetString(sqlDataReader.GetOrdinal("DisplayContent"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ArticleID")))
										{
											ptlBinHistoryEntity.ArticleID = sqlDataReader.GetString(sqlDataReader.GetOrdinal("ArticleID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("StockIn")))
										{
											ptlBinHistoryEntity.StockIn = sqlDataReader.GetDecimal(sqlDataReader.GetOrdinal("StockIn"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("StockOut")))
										{
											ptlBinHistoryEntity.StockOut = sqlDataReader.GetDecimal(sqlDataReader.GetOrdinal("StockOut"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("Adjustment")))
										{
											ptlBinHistoryEntity.Adjustment = sqlDataReader.GetDecimal(sqlDataReader.GetOrdinal("Adjustment"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("Remarks")))
										{
											ptlBinHistoryEntity.Remarks = sqlDataReader.GetString(sqlDataReader.GetOrdinal("Remarks"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("RequestID")))
										{
											ptlBinHistoryEntity.RequestID = sqlDataReader.GetString(sqlDataReader.GetOrdinal("RequestID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("RequestCode")))
										{
											ptlBinHistoryEntity.RequestCode = sqlDataReader.GetString(sqlDataReader.GetOrdinal("RequestCode"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("LightOnOffCommand")))
										{
											ptlBinHistoryEntity.LightOnOffCommand = sqlDataReader.GetString(sqlDataReader.GetOrdinal("LightOnOffCommand"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("OnTimeStamp")))
										{
											ptlBinHistoryEntity.OnTimeStamp = sqlDataReader.GetDateTime(sqlDataReader.GetOrdinal("OnTimeStamp"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("TurnOffByButtonPress")))
										{
											ptlBinHistoryEntity.TurnOffByButtonPress = sqlDataReader.GetBoolean(sqlDataReader.GetOrdinal("TurnOffByButtonPress"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("TurnOffByCommand")))
										{
											ptlBinHistoryEntity.TurnOffByCommand = sqlDataReader.GetBoolean(sqlDataReader.GetOrdinal("TurnOffByCommand"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("OffTimeStamp")))
										{
											ptlBinHistoryEntity.OffTimeStamp = sqlDataReader.GetDateTime(sqlDataReader.GetOrdinal("OffTimeStamp"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("UpdatedBy")))
										{
											ptlBinHistoryEntity.UpdatedBy = sqlDataReader.GetString(sqlDataReader.GetOrdinal("UpdatedBy"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("UpdatedOn")))
										{
											ptlBinHistoryEntity.UpdatedOn = sqlDataReader.GetDateTime(sqlDataReader.GetOrdinal("UpdatedOn"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("CreatedBy")))
										{
											ptlBinHistoryEntity.CreatedBy = sqlDataReader.GetString(sqlDataReader.GetOrdinal("CreatedBy"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("CreatedOn")))
										{
											ptlBinHistoryEntity.CreatedOn = sqlDataReader.GetDateTime(sqlDataReader.GetOrdinal("CreatedOn"));
										}
									}
								}
								catch (Exception exception)
								{
									SysLog.WriteLog(3, exception.Message);
								}
							}
							finally
							{
								sqlDataReader.Close();
							}
						}
						sqlCommand.Dispose();
					}
				}
			}
			catch (Exception exception1)
			{
				SysLog.WriteLog(3, exception1.Message);
			}
			return ptlBinHistoryEntity;
		}

		public List<PtlBinHistoryEntity> GetList(int topRecords)
		{
			List<PtlBinHistoryEntity> ptlBinHistoryEntities = new List<PtlBinHistoryEntity>();
			try
			{
				if (ConnectorFactory.Connect())
				{
					string str = "SELECT TOP @TOPRECORDS ID, SystemName, BinID, SRC, ModuleID, ModuleColor, ModuleFlash, ModuleBuzzer, IsLightCommandIssued, DisplayContent, ArticleID, StockIn, StockOut, Adjustment, Remarks, RequestID, RequestCode, LightOnOffCommand, OnTimeStamp, TurnOffByButtonPress, TurnOffByCommand, OffTimeStamp, UpdatedBy, UpdatedOn, CreatedBy, CreatedOn FROM tbl_ptl_bin_history";
					str = string.Concat(str, " ORDER BY CreatedOn DESC");
					using (SqlCommand sqlCommand = new SqlCommand())
					{
						sqlCommand.Connection = ConnectorFactory.Connection;
						sqlCommand.CommandText = str;
						sqlCommand.Prepare();
						sqlCommand.Parameters.AddWithValue("@TOPRECORDS", topRecords);
						using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
						{
							try
							{
								try
								{
									while (sqlDataReader.Read())
									{
										PtlBinHistoryEntity ptlBinHistoryEntity = new PtlBinHistoryEntity();
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ID")))
										{
											ptlBinHistoryEntity.ID = sqlDataReader.GetGuid(sqlDataReader.GetOrdinal("ID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("SystemName")))
										{
											ptlBinHistoryEntity.SystemName = sqlDataReader.GetString(sqlDataReader.GetOrdinal("SystemName"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("BinID")))
										{
											ptlBinHistoryEntity.BinID = sqlDataReader.GetGuid(sqlDataReader.GetOrdinal("BinID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("SRC")))
										{
											ptlBinHistoryEntity.SRC = sqlDataReader.GetString(sqlDataReader.GetOrdinal("SRC"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ModuleID")))
										{
											ptlBinHistoryEntity.ModuleID = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("ModuleID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ModuleColor")))
										{
											ptlBinHistoryEntity.ModuleColor = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("ModuleColor"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ModuleFlash")))
										{
											ptlBinHistoryEntity.ModuleFlash = sqlDataReader.GetBoolean(sqlDataReader.GetOrdinal("ModuleFlash"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ModuleBuzzer")))
										{
											ptlBinHistoryEntity.ModuleBuzzer = sqlDataReader.GetBoolean(sqlDataReader.GetOrdinal("ModuleBuzzer"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("IsLightCommandIssued")))
										{
											ptlBinHistoryEntity.IsLightCommandIssued = sqlDataReader.GetBoolean(sqlDataReader.GetOrdinal("IsLightCommandIssued"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("DisplayContent")))
										{
											ptlBinHistoryEntity.DisplayContent = sqlDataReader.GetString(sqlDataReader.GetOrdinal("DisplayContent"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ArticleID")))
										{
											ptlBinHistoryEntity.ArticleID = sqlDataReader.GetString(sqlDataReader.GetOrdinal("ArticleID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("StockIn")))
										{
											ptlBinHistoryEntity.StockIn = sqlDataReader.GetDecimal(sqlDataReader.GetOrdinal("StockIn"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("StockOut")))
										{
											ptlBinHistoryEntity.StockOut = sqlDataReader.GetDecimal(sqlDataReader.GetOrdinal("StockOut"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("Adjustment")))
										{
											ptlBinHistoryEntity.Adjustment = sqlDataReader.GetDecimal(sqlDataReader.GetOrdinal("Adjustment"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("Remarks")))
										{
											ptlBinHistoryEntity.Remarks = sqlDataReader.GetString(sqlDataReader.GetOrdinal("Remarks"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("RequestID")))
										{
											ptlBinHistoryEntity.RequestID = sqlDataReader.GetString(sqlDataReader.GetOrdinal("RequestID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("RequestCode")))
										{
											ptlBinHistoryEntity.RequestCode = sqlDataReader.GetString(sqlDataReader.GetOrdinal("RequestCode"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("LightOnOffCommand")))
										{
											ptlBinHistoryEntity.LightOnOffCommand = sqlDataReader.GetString(sqlDataReader.GetOrdinal("LightOnOffCommand"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("OnTimeStamp")))
										{
											ptlBinHistoryEntity.OnTimeStamp = sqlDataReader.GetDateTime(sqlDataReader.GetOrdinal("OnTimeStamp"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("TurnOffByButtonPress")))
										{
											ptlBinHistoryEntity.TurnOffByButtonPress = sqlDataReader.GetBoolean(sqlDataReader.GetOrdinal("TurnOffByButtonPress"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("TurnOffByCommand")))
										{
											ptlBinHistoryEntity.TurnOffByCommand = sqlDataReader.GetBoolean(sqlDataReader.GetOrdinal("TurnOffByCommand"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("OffTimeStamp")))
										{
											ptlBinHistoryEntity.OffTimeStamp = sqlDataReader.GetDateTime(sqlDataReader.GetOrdinal("OffTimeStamp"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("UpdatedBy")))
										{
											ptlBinHistoryEntity.UpdatedBy = sqlDataReader.GetString(sqlDataReader.GetOrdinal("UpdatedBy"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("UpdatedOn")))
										{
											ptlBinHistoryEntity.UpdatedOn = sqlDataReader.GetDateTime(sqlDataReader.GetOrdinal("UpdatedOn"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("CreatedBy")))
										{
											ptlBinHistoryEntity.CreatedBy = sqlDataReader.GetString(sqlDataReader.GetOrdinal("CreatedBy"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("CreatedOn")))
										{
											ptlBinHistoryEntity.CreatedOn = sqlDataReader.GetDateTime(sqlDataReader.GetOrdinal("CreatedOn"));
										}
										ptlBinHistoryEntities.Add(ptlBinHistoryEntity);
									}
								}
								catch (Exception exception)
								{
									SysLog.WriteLog(3, exception.Message);
								}
							}
							finally
							{
								sqlDataReader.Close();
							}
						}
						sqlCommand.Dispose();
					}
				}
			}
			catch (Exception exception1)
			{
				SysLog.WriteLog(3, exception1.Message);
			}
			return ptlBinHistoryEntities;
		}

		public bool Save(PtlBinHistoryEntity entity)
		{
			bool flag = false;
			try
			{
				if (ConnectorFactory.Connect())
				{
					string str = "UPDATE tbl_ptl_bin_history SET SystemName=@SystemName, BinID=@BinID, SRC=@SRC, ModuleID=@ModuleID, ModuleColor=@ModuleColor, ModuleFlash=@ModuleFlash, ModuleBuzzer=@ModuleBuzzer, IsLightCommandIssued=@IsLightCommandIssued, DisplayContent=@DisplayContent, ArticleID=@ArticleID, StockIn=@StockIn, StockOut=@StockOut, Adjustment=@Adjustment, Remarks=@Remarks, RequestID=@RequestID, RequestCode=@RequestCode, LightOnOffCommand=@LightOnOffCommand, OnTimeStamp=@OnTimeStamp, TurnOffByButtonPress=@TurnOffByButtonPress, TurnOffByCommand=@TurnOffByCommand, OffTimeStamp=@OffTimeStamp, UpdatedBy=@UpdatedBy, UpdatedOn=@UpdatedOn, CreatedBy=@CreatedBy, CreatedOn=@CreatedOn WHERE ID=@ID";
					using (SqlCommand sqlCommand = new SqlCommand())
					{
						sqlCommand.Connection = ConnectorFactory.Connection;
						sqlCommand.CommandText = str;
						sqlCommand.Prepare();
						sqlCommand.Parameters.AddWithValue("@ID", entity.ID);
						sqlCommand.Parameters.AddWithValue("@SystemName", entity.SystemName);
						sqlCommand.Parameters.AddWithValue("@BinID", entity.BinID);
						sqlCommand.Parameters.AddWithValue("@SRC", entity.SRC);
						sqlCommand.Parameters.AddWithValue("@ModuleID", entity.ModuleID);
						sqlCommand.Parameters.AddWithValue("@ModuleColor", entity.ModuleColor);
						sqlCommand.Parameters.AddWithValue("@ModuleFlash", entity.ModuleFlash);
						sqlCommand.Parameters.AddWithValue("@ModuleBuzzer", entity.ModuleBuzzer);
						sqlCommand.Parameters.AddWithValue("@IsLightCommandIssued", entity.IsLightCommandIssued);
						sqlCommand.Parameters.AddWithValue("@DisplayContent", entity.DisplayContent);
						sqlCommand.Parameters.AddWithValue("@ArticleID", entity.ArticleID);
						sqlCommand.Parameters.AddWithValue("@StockIn", entity.StockIn);
						sqlCommand.Parameters.AddWithValue("@StockOut", entity.StockOut);
						sqlCommand.Parameters.AddWithValue("@Adjustment", entity.Adjustment);
						sqlCommand.Parameters.AddWithValue("@Remarks", entity.Remarks);
						sqlCommand.Parameters.AddWithValue("@RequestID", entity.RequestID);
						sqlCommand.Parameters.AddWithValue("@RequestCode", entity.RequestCode);
						sqlCommand.Parameters.AddWithValue("@LightOnOffCommand", entity.LightOnOffCommand);
						if (entity.OnTimeStamp != DateTime.MinValue)
						{
							sqlCommand.Parameters.AddWithValue("@OnTimeStamp", entity.OnTimeStamp);
						}
						else
						{
							sqlCommand.Parameters.AddWithValue("@OnTimeStamp", DateTime.Now);
						}
						sqlCommand.Parameters.AddWithValue("@TurnOffByButtonPress", entity.TurnOffByButtonPress);
						sqlCommand.Parameters.AddWithValue("@TurnOffByCommand", entity.TurnOffByCommand);
						if (entity.OffTimeStamp != DateTime.MinValue)
						{
							sqlCommand.Parameters.AddWithValue("@OffTimeStamp", entity.OffTimeStamp);
						}
						else
						{
							sqlCommand.Parameters.AddWithValue("@OffTimeStamp", DateTime.Now);
						}
						sqlCommand.Parameters.AddWithValue("@UpdatedBy", entity.UpdatedBy);
						if (entity.UpdatedOn != DateTime.MinValue)
						{
							sqlCommand.Parameters.AddWithValue("@UpdatedOn", entity.UpdatedOn);
						}
						else
						{
							sqlCommand.Parameters.AddWithValue("@UpdatedOn", DateTime.Now);
						}
						sqlCommand.Parameters.AddWithValue("@CreatedBy", entity.CreatedBy);
						if (entity.CreatedOn != DateTime.MinValue)
						{
							sqlCommand.Parameters.AddWithValue("@CreatedOn", entity.CreatedOn);
						}
						else
						{
							sqlCommand.Parameters.AddWithValue("@CreatedOn", DateTime.Now);
						}
						object obj = sqlCommand.ExecuteNonQuery();
						flag = true;
						sqlCommand.Dispose();
					}
				}
			}
			catch (Exception exception)
			{
				SysLog.WriteLog(3, exception.Message);
			}
			return flag;
		}
	}
}