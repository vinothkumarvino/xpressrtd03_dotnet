using MSClassLibrary.Log;
using OPtlDAL.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;

namespace OPtlDAL.DataFactory
{
	internal class PtlControllerFactory
	{
		public PtlControllerFactory()
		{
		}

		public bool Add(ref PtlControllerEntity entity)
		{
			bool flag = false;
			try
			{
				if (ConnectorFactory.Connect())
				{
					string str = "INSERT INTO tbl_ptl_controller (SystemID, UnitNo, SystemName, NoOfLightModules, FlashEnabled, AudioEnabled, OverwriteEnabled) VALUES (@SystemID, @UnitNo, @SystemName, @NoOfLightModules, @FlashEnabled, @AudioEnabled, @OverwriteEnabled);";
					using (SqlCommand sqlCommand = new SqlCommand())
					{
						sqlCommand.Connection = ConnectorFactory.Connection;
						sqlCommand.CommandText = str;
						sqlCommand.Prepare();
						sqlCommand.Parameters.AddWithValue("@SystemID", entity.SystemID);
						sqlCommand.Parameters.AddWithValue("@UnitNo", entity.UnitNo);
						sqlCommand.Parameters.AddWithValue("@SystemName", entity.SystemName);
						sqlCommand.Parameters.AddWithValue("@NoOfLightModules", entity.NoOfLightModules);
						sqlCommand.Parameters.AddWithValue("@FlashEnabled", entity.FlashEnabled);
						sqlCommand.Parameters.AddWithValue("@AudioEnabled", entity.AudioEnabled);
						sqlCommand.Parameters.AddWithValue("@OverwriteEnabled", entity.OverwriteEnabled);
						sqlCommand.ExecuteScalar();
						flag = true;
						sqlCommand.Dispose();
					}
				}
			}
			catch (Exception exception)
			{
				SysLog.WriteLog(3, exception.Message);
			}
			return flag;
		}

		public bool Delete(PtlControllerEntity entity)
		{
			bool flag = false;
			try
			{
				if (ConnectorFactory.Connect())
				{
					string str = "DELETE FROM tbl_ptl_controller WHERE  SystemID=@SystemID";
					using (SqlCommand sqlCommand = new SqlCommand())
					{
						sqlCommand.Connection = ConnectorFactory.Connection;
						sqlCommand.CommandText = str;
						sqlCommand.Prepare();
						sqlCommand.Parameters.AddWithValue("@SystemID", entity.SystemID);
						object obj = sqlCommand.ExecuteNonQuery();
						flag = true;
						sqlCommand.Dispose();
					}
				}
			}
			catch (Exception exception)
			{
				SysLog.WriteLog(3, exception.Message);
			}
			return flag;
		}

		public PtlControllerEntity Get(Guid ID)
		{
			PtlControllerEntity ptlControllerEntity = null;
			try
			{
				if (ConnectorFactory.Connect())
				{
					string str = "SELECT SystemID, UnitNo, SystemName, NoOfLightModules, FlashEnabled, AudioEnabled, OverwriteEnabled FROM tbl_ptl_controller";
					str = string.Concat(str, " WHERE SystemID=@SystemID;");
					using (SqlCommand sqlCommand = new SqlCommand())
					{
						sqlCommand.Connection = ConnectorFactory.Connection;
						sqlCommand.CommandText = str;
						sqlCommand.Prepare();
						sqlCommand.Parameters.AddWithValue("@SystemID", ID);
						using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
						{
							try
							{
								try
								{
									if (sqlDataReader.Read())
									{
										ptlControllerEntity = new PtlControllerEntity();
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("SystemID")))
										{
											ptlControllerEntity.SystemID = sqlDataReader.GetGuid(sqlDataReader.GetOrdinal("SystemID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("UnitNo")))
										{
											ptlControllerEntity.UnitNo = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("UnitNo"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("SystemName")))
										{
											ptlControllerEntity.SystemName = sqlDataReader.GetString(sqlDataReader.GetOrdinal("SystemName"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("NoOfLightModules")))
										{
											ptlControllerEntity.NoOfLightModules = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("NoOfLightModules"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("FlashEnabled")))
										{
											ptlControllerEntity.FlashEnabled = sqlDataReader.GetBoolean(sqlDataReader.GetOrdinal("FlashEnabled"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("AudioEnabled")))
										{
											ptlControllerEntity.AudioEnabled = sqlDataReader.GetBoolean(sqlDataReader.GetOrdinal("AudioEnabled"));
										}
									}
								}
								catch (Exception exception)
								{
									SysLog.WriteLog(3, exception.Message);
								}
							}
							finally
							{
								sqlDataReader.Close();
							}
						}
						sqlCommand.Dispose();
					}
				}
			}
			catch (Exception exception1)
			{
				SysLog.WriteLog(3, exception1.Message);
			}
			return ptlControllerEntity;
		}

		public PtlControllerEntity Get(string systemName)
		{
			PtlControllerEntity ptlControllerEntity = null;
			try
			{
				if (ConnectorFactory.Connect())
				{
					string str = "SELECT SystemID, UnitNo, SystemName, NoOfLightModules, FlashEnabled, AudioEnabled, OverwriteEnabled FROM tbl_ptl_controller";
					str = string.Concat(str, " WHERE SystemName=@SystemName;");
					using (SqlCommand sqlCommand = new SqlCommand())
					{
						sqlCommand.Connection = ConnectorFactory.Connection;
						sqlCommand.CommandText = str;
						sqlCommand.Prepare();
						sqlCommand.Parameters.AddWithValue("@SystemName", systemName);
						using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
						{
							try
							{
								try
								{
									if (sqlDataReader.Read())
									{
										ptlControllerEntity = new PtlControllerEntity();
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("SystemID")))
										{
											ptlControllerEntity.SystemID = sqlDataReader.GetGuid(sqlDataReader.GetOrdinal("SystemID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("UnitNo")))
										{
											ptlControllerEntity.UnitNo = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("UnitNo"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("SystemName")))
										{
											ptlControllerEntity.SystemName = sqlDataReader.GetString(sqlDataReader.GetOrdinal("SystemName"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("NoOfLightModules")))
										{
											ptlControllerEntity.NoOfLightModules = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("NoOfLightModules"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("FlashEnabled")))
										{
											ptlControllerEntity.FlashEnabled = sqlDataReader.GetBoolean(sqlDataReader.GetOrdinal("FlashEnabled"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("AudioEnabled")))
										{
											ptlControllerEntity.AudioEnabled = sqlDataReader.GetBoolean(sqlDataReader.GetOrdinal("AudioEnabled"));
										}
									}
								}
								catch (Exception exception)
								{
									SysLog.WriteLog(3, exception.Message);
								}
							}
							finally
							{
								sqlDataReader.Close();
							}
						}
						sqlCommand.Dispose();
					}
				}
			}
			catch (Exception exception1)
			{
				SysLog.WriteLog(3, exception1.Message);
			}
			return ptlControllerEntity;
		}

		public List<PtlControllerEntity> GetList()
		{
			List<PtlControllerEntity> ptlControllerEntities = new List<PtlControllerEntity>();
			try
			{
				if (ConnectorFactory.Connect())
				{
					string str = "SELECT SystemID, UnitNo, SystemName, NoOfLightModules, FlashEnabled, AudioEnabled, OverwriteEnabled FROM tbl_ptl_controller";
					using (SqlCommand sqlCommand = new SqlCommand())
					{
						sqlCommand.Connection = ConnectorFactory.Connection;
						sqlCommand.CommandText = str;
						sqlCommand.Prepare();
						using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
						{
							try
							{
								try
								{
									while (sqlDataReader.Read())
									{
										PtlControllerEntity ptlControllerEntity = new PtlControllerEntity();
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("SystemID")))
										{
											ptlControllerEntity.SystemID = sqlDataReader.GetGuid(sqlDataReader.GetOrdinal("SystemID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("UnitNo")))
										{
											ptlControllerEntity.UnitNo = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("UnitNo"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("SystemName")))
										{
											ptlControllerEntity.SystemName = sqlDataReader.GetString(sqlDataReader.GetOrdinal("SystemName"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("NoOfLightModules")))
										{
											ptlControllerEntity.NoOfLightModules = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("NoOfLightModules"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("FlashEnabled")))
										{
											ptlControllerEntity.FlashEnabled = sqlDataReader.GetBoolean(sqlDataReader.GetOrdinal("FlashEnabled"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("AudioEnabled")))
										{
											ptlControllerEntity.AudioEnabled = sqlDataReader.GetBoolean(sqlDataReader.GetOrdinal("AudioEnabled"));
										}
										ptlControllerEntities.Add(ptlControllerEntity);
									}
								}
								catch (Exception exception)
								{
									SysLog.WriteLog(3, exception.Message);
								}
							}
							finally
							{
								sqlDataReader.Close();
							}
						}
						sqlCommand.Dispose();
					}
				}
			}
			catch (Exception exception1)
			{
				SysLog.WriteLog(3, exception1.Message);
			}
			return ptlControllerEntities;
		}

		public bool Save(PtlControllerEntity entity)
		{
			bool flag = false;
			try
			{
				if (ConnectorFactory.Connect())
				{
					string str = "UPDATE tbl_ptl_controller SET UnitNo=@UnitNo, SystemName=@SystemName, NoOfLightModules=@NoOfLightModules, FlashEnabled=@FlashEnabled, AudioEnabled=@AudioEnabled, OverwriteEnabled=@OverwriteEnabled WHERE SystemID=@SystemID";
					using (SqlCommand sqlCommand = new SqlCommand())
					{
						sqlCommand.Connection = ConnectorFactory.Connection;
						sqlCommand.CommandText = str;
						sqlCommand.Prepare();
						sqlCommand.Parameters.AddWithValue("@SystemID", entity.SystemID);
						sqlCommand.Parameters.AddWithValue("@UnitNo", entity.UnitNo);
						sqlCommand.Parameters.AddWithValue("@SystemName", entity.SystemName);
						sqlCommand.Parameters.AddWithValue("@NoOfLightModules", entity.NoOfLightModules);
						sqlCommand.Parameters.AddWithValue("@FlashEnabled", entity.FlashEnabled);
						sqlCommand.Parameters.AddWithValue("@AudioEnabled", entity.AudioEnabled);
						sqlCommand.Parameters.AddWithValue("@OverwriteEnabled", entity.OverwriteEnabled);
						object obj = sqlCommand.ExecuteNonQuery();
						flag = true;
						sqlCommand.Dispose();
					}
				}
			}
			catch (Exception exception)
			{
				SysLog.WriteLog(3, exception.Message);
			}
			return flag;
		}
	}
}