using MSClassLibrary.Log;
using OPtlDAL.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;

namespace OPtlDAL.DataFactory
{
	internal class PtlOrdersHistoryFactory
	{
		public PtlOrdersHistoryFactory()
		{
		}

		public bool Add(ref PtlOrdersHistoryEntity entity)
		{
			bool flag = false;
			try
			{
				if (ConnectorFactory.Connect())
				{
					string str = "INSERT INTO tbl_ptl_orders_history (ID, TransactionType, PatientID, PatientName, MedicalRecordNo, ArticleID, Quantity, UserName, Area, TransactionDateTime) VALUES (@ID, @TransactionType, @PatientID, @PatientName, @MedicalRecordNo, @ArticleID, @Quantity, @UserName, @Area, @TransactionDateTime);";
					using (SqlCommand sqlCommand = new SqlCommand())
					{
						sqlCommand.Connection = ConnectorFactory.Connection;
						sqlCommand.CommandText = str;
						sqlCommand.Prepare();
						sqlCommand.Parameters.AddWithValue("@ID", entity.ID);
						sqlCommand.Parameters.AddWithValue("@TransactionType", entity.TransactionType);
						sqlCommand.Parameters.AddWithValue("@PatientID", entity.PatientID);
						sqlCommand.Parameters.AddWithValue("@PatientName", entity.PatientName);
						sqlCommand.Parameters.AddWithValue("@MedicalRecordNo", entity.MedicalRecordNo);
						sqlCommand.Parameters.AddWithValue("@ArticleID", entity.ArticleID);
						sqlCommand.Parameters.AddWithValue("@Quantity", entity.Quantity);
						sqlCommand.Parameters.AddWithValue("@UserName", entity.UserName);
						sqlCommand.Parameters.AddWithValue("@Area", entity.Area);
						if (entity.TransactionDateTime != DateTime.MinValue)
						{
							sqlCommand.Parameters.AddWithValue("@TransactionDateTime", entity.TransactionDateTime);
						}
						else
						{
							sqlCommand.Parameters.AddWithValue("@TransactionDateTime", DateTime.Now);
						}
						sqlCommand.ExecuteScalar();
						flag = true;
						sqlCommand.Dispose();
					}
				}
			}
			catch (Exception exception)
			{
				SysLog.WriteLog(3, exception.Message);
			}
			return flag;
		}

		public bool Delete(PtlOrdersHistoryEntity entity)
		{
			bool flag = false;
			try
			{
				if (ConnectorFactory.Connect())
				{
					string str = "DELETE FROM tbl_ptl_orders_history WHERE ID=@ID";
					using (SqlCommand sqlCommand = new SqlCommand())
					{
						sqlCommand.Connection = ConnectorFactory.Connection;
						sqlCommand.CommandText = str;
						sqlCommand.Prepare();
						sqlCommand.Parameters.AddWithValue("@ID", entity.ID);
						object obj = sqlCommand.ExecuteNonQuery();
						flag = true;
						sqlCommand.Dispose();
					}
				}
			}
			catch (Exception exception)
			{
				SysLog.WriteLog(3, exception.Message);
			}
			return flag;
		}

		public PtlOrdersHistoryEntity Get(Guid ID)
		{
			PtlOrdersHistoryEntity ptlOrdersHistoryEntity = null;
			try
			{
				if (ConnectorFactory.Connect())
				{
					string str = "SELECT ID, TransactionType, PatientID, PatientName, MedicalRecordNo, ArticleID, Quantity, UserName, Area, TransactionDateTime FROM tbl_ptl_orders_history";
					str = string.Concat(str, " WHERE ID=@ID;");
					using (SqlCommand sqlCommand = new SqlCommand())
					{
						sqlCommand.Connection = ConnectorFactory.Connection;
						sqlCommand.CommandText = str;
						sqlCommand.Prepare();
						sqlCommand.Parameters.AddWithValue("@ID", ID);
						using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
						{
							try
							{
								try
								{
									if (sqlDataReader.Read())
									{
										ptlOrdersHistoryEntity = new PtlOrdersHistoryEntity();
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ID")))
										{
											ptlOrdersHistoryEntity.ID = sqlDataReader.GetGuid(sqlDataReader.GetOrdinal("ID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("TransactionType")))
										{
											ptlOrdersHistoryEntity.TransactionType = sqlDataReader.GetString(sqlDataReader.GetOrdinal("TransactionType"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("PatientID")))
										{
											ptlOrdersHistoryEntity.PatientID = sqlDataReader.GetString(sqlDataReader.GetOrdinal("PatientID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("PatientName")))
										{
											ptlOrdersHistoryEntity.PatientName = sqlDataReader.GetString(sqlDataReader.GetOrdinal("PatientName"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("MedicalRecordNo")))
										{
											ptlOrdersHistoryEntity.MedicalRecordNo = sqlDataReader.GetString(sqlDataReader.GetOrdinal("MedicalRecordNo"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ArticleID")))
										{
											ptlOrdersHistoryEntity.ArticleID = sqlDataReader.GetString(sqlDataReader.GetOrdinal("ArticleID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("Quantity")))
										{
											ptlOrdersHistoryEntity.Quantity = sqlDataReader.GetDecimal(sqlDataReader.GetOrdinal("Quantity"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("UserName")))
										{
											ptlOrdersHistoryEntity.UserName = sqlDataReader.GetString(sqlDataReader.GetOrdinal("UserName"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("Area")))
										{
											ptlOrdersHistoryEntity.Area = sqlDataReader.GetString(sqlDataReader.GetOrdinal("Area"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("TransactionDateTime")))
										{
											ptlOrdersHistoryEntity.TransactionDateTime = sqlDataReader.GetDateTime(sqlDataReader.GetOrdinal("TransactionDateTime"));
										}
									}
								}
								catch (Exception exception)
								{
									SysLog.WriteLog(3, exception.Message);
								}
							}
							finally
							{
								sqlDataReader.Close();
							}
						}
						sqlCommand.Dispose();
					}
				}
			}
			catch (Exception exception1)
			{
				SysLog.WriteLog(3, exception1.Message);
			}
			return ptlOrdersHistoryEntity;
		}

		public List<PtlOrdersHistoryEntity> GetList(Guid ID)
		{
			List<PtlOrdersHistoryEntity> ptlOrdersHistoryEntities = new List<PtlOrdersHistoryEntity>();
			try
			{
				if (ConnectorFactory.Connect())
				{
					string str = "SELECT ID, TransactionType, PatientID, PatientName, MedicalRecordNo, ArticleID, Quantity, UserName, Area, TransactionDateTime FROM tbl_ptl_orders_history";
					str = string.Concat(str, " WHERE ID=@ID;");
					using (SqlCommand sqlCommand = new SqlCommand())
					{
						sqlCommand.Connection = ConnectorFactory.Connection;
						sqlCommand.CommandText = str;
						sqlCommand.Prepare();
						sqlCommand.Parameters.AddWithValue("@ID", ID);
						using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
						{
							try
							{
								try
								{
									while (sqlDataReader.Read())
									{
										PtlOrdersHistoryEntity ptlOrdersHistoryEntity = new PtlOrdersHistoryEntity();
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ID")))
										{
											ptlOrdersHistoryEntity.ID = sqlDataReader.GetGuid(sqlDataReader.GetOrdinal("ID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("TransactionType")))
										{
											ptlOrdersHistoryEntity.TransactionType = sqlDataReader.GetString(sqlDataReader.GetOrdinal("TransactionType"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("PatientID")))
										{
											ptlOrdersHistoryEntity.PatientID = sqlDataReader.GetString(sqlDataReader.GetOrdinal("PatientID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("PatientName")))
										{
											ptlOrdersHistoryEntity.PatientName = sqlDataReader.GetString(sqlDataReader.GetOrdinal("PatientName"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("MedicalRecordNo")))
										{
											ptlOrdersHistoryEntity.MedicalRecordNo = sqlDataReader.GetString(sqlDataReader.GetOrdinal("MedicalRecordNo"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ArticleID")))
										{
											ptlOrdersHistoryEntity.ArticleID = sqlDataReader.GetString(sqlDataReader.GetOrdinal("ArticleID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("Quantity")))
										{
											ptlOrdersHistoryEntity.Quantity = sqlDataReader.GetDecimal(sqlDataReader.GetOrdinal("Quantity"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("UserName")))
										{
											ptlOrdersHistoryEntity.UserName = sqlDataReader.GetString(sqlDataReader.GetOrdinal("UserName"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("Area")))
										{
											ptlOrdersHistoryEntity.Area = sqlDataReader.GetString(sqlDataReader.GetOrdinal("Area"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("TransactionDateTime")))
										{
											ptlOrdersHistoryEntity.TransactionDateTime = sqlDataReader.GetDateTime(sqlDataReader.GetOrdinal("TransactionDateTime"));
										}
										ptlOrdersHistoryEntities.Add(ptlOrdersHistoryEntity);
									}
								}
								catch (Exception exception)
								{
									SysLog.WriteLog(3, exception.Message);
								}
							}
							finally
							{
								sqlDataReader.Close();
							}
						}
						sqlCommand.Dispose();
					}
				}
			}
			catch (Exception exception1)
			{
				SysLog.WriteLog(3, exception1.Message);
			}
			return ptlOrdersHistoryEntities;
		}

		public bool Save(PtlOrdersHistoryEntity entity)
		{
			bool flag = false;
			try
			{
				if (ConnectorFactory.Connect())
				{
					string str = "UPDATE tbl_ptl_orders_history SET TransactionType=@TransactionType, PatientID=@PatientID, PatientName=@PatientName, MedicalRecordNo=@MedicalRecordNo, ArticleID=@ArticleID, Quantity=@Quantity, UserName=@UserName, Area=@Area, TransactionDateTime=@TransactionDateTime WHERE ID=@ID";
					using (SqlCommand sqlCommand = new SqlCommand())
					{
						sqlCommand.Connection = ConnectorFactory.Connection;
						sqlCommand.CommandText = str;
						sqlCommand.Prepare();
						sqlCommand.Parameters.AddWithValue("@ID", entity.ID);
						sqlCommand.Parameters.AddWithValue("@TransactionType", entity.TransactionType);
						sqlCommand.Parameters.AddWithValue("@PatientID", entity.PatientID);
						sqlCommand.Parameters.AddWithValue("@PatientName", entity.PatientName);
						sqlCommand.Parameters.AddWithValue("@MedicalRecordNo", entity.MedicalRecordNo);
						sqlCommand.Parameters.AddWithValue("@ArticleID", entity.ArticleID);
						sqlCommand.Parameters.AddWithValue("@Quantity", entity.Quantity);
						sqlCommand.Parameters.AddWithValue("@UserName", entity.UserName);
						sqlCommand.Parameters.AddWithValue("@Area", entity.Area);
						if (entity.TransactionDateTime != DateTime.MinValue)
						{
							sqlCommand.Parameters.AddWithValue("@TransactionDateTime", entity.TransactionDateTime);
						}
						else
						{
							sqlCommand.Parameters.AddWithValue("@TransactionDateTime", DateTime.Now);
						}
						object obj = sqlCommand.ExecuteNonQuery();
						flag = true;
						sqlCommand.Dispose();
					}
				}
			}
			catch (Exception exception)
			{
				SysLog.WriteLog(3, exception.Message);
			}
			return flag;
		}
	}
}