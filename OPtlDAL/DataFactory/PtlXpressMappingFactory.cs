using MSClassLibrary.Log;
using OPtlDAL.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;

namespace OPtlDAL.DataFactory
{
	internal class PtlXpressMappingFactory
	{
		public PtlXpressMappingFactory()
		{
		}

		public bool Add(ref PtlXpressMappingEntity entity)
		{
			bool flag = false;
			try
			{
				if (ConnectorFactory.Connect())
				{
					string str = "INSERT INTO tbl_ptl_xpress_mapping (PigeonHoleID, SRC, IsFront, IsBack) VALUES (@PigeonHoleID, @SRC, @IsFront, @IsBack);";
					using (SqlCommand sqlCommand = new SqlCommand())
					{
						sqlCommand.Connection = ConnectorFactory.Connection;
						sqlCommand.CommandText = str;
						sqlCommand.Prepare();
						sqlCommand.Parameters.AddWithValue("@PigeonHoleID", entity.PigeonHoleID);
						sqlCommand.Parameters.AddWithValue("@SRC", entity.SRC);
						sqlCommand.Parameters.AddWithValue("@IsFront", entity.IsFront);
						sqlCommand.Parameters.AddWithValue("@IsBack", entity.IsBack);
						sqlCommand.ExecuteScalar();
						flag = true;
						sqlCommand.Dispose();
					}
				}
			}
			catch (Exception exception)
			{
				SysLog.WriteLog(3, exception.Message);
			}
			return flag;
		}

		public bool Delete(PtlXpressMappingEntity entity)
		{
			bool flag = false;
			try
			{
				if (ConnectorFactory.Connect())
				{
					string str = "DELETE FROM tbl_ptl_xpress_mapping WHERE ID=@ID";
					using (SqlCommand sqlCommand = new SqlCommand())
					{
						sqlCommand.Connection = ConnectorFactory.Connection;
						sqlCommand.CommandText = str;
						sqlCommand.Prepare();
						sqlCommand.Parameters.AddWithValue("@PigeonHoleID", entity.PigeonHoleID);
						object obj = sqlCommand.ExecuteNonQuery();
						flag = true;
						sqlCommand.Dispose();
					}
				}
			}
			catch (Exception exception)
			{
				SysLog.WriteLog(3, exception.Message);
			}
			return flag;
		}

		public PtlXpressMappingEntity Get(string pigeonHoleID)
		{
			PtlXpressMappingEntity ptlXpressMappingEntity = null;
			try
			{
				if (ConnectorFactory.Connect())
				{
					string str = "SELECT PigeonHoleID, SRC, IsFront, IsBack FROM tbl_ptl_xpress_mapping";
					str = string.Concat(str, " WHERE PigeonHoleID=@PigeonHoleID;");
					using (SqlCommand sqlCommand = new SqlCommand())
					{
						sqlCommand.Connection = ConnectorFactory.Connection;
						sqlCommand.CommandText = str;
						sqlCommand.Prepare();
						sqlCommand.Parameters.AddWithValue("@PigeonHoleID", pigeonHoleID);
						using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
						{
							try
							{
								try
								{
									if (sqlDataReader.Read())
									{
										ptlXpressMappingEntity = new PtlXpressMappingEntity();
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("PigeonHoleID")))
										{
											ptlXpressMappingEntity.PigeonHoleID = sqlDataReader.GetString(sqlDataReader.GetOrdinal("PigeonHoleID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("SRC")))
										{
											ptlXpressMappingEntity.SRC = sqlDataReader.GetString(sqlDataReader.GetOrdinal("SRC"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("IsFront")))
										{
											ptlXpressMappingEntity.IsFront = sqlDataReader.GetBoolean(sqlDataReader.GetOrdinal("IsFront"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("IsBack")))
										{
											ptlXpressMappingEntity.IsBack = sqlDataReader.GetBoolean(sqlDataReader.GetOrdinal("IsBack"));
										}
									}
								}
								catch (Exception exception)
								{
									SysLog.WriteLog(3, exception.Message);
								}
							}
							finally
							{
								sqlDataReader.Close();
							}
						}
						sqlCommand.Dispose();
					}
				}
			}
			catch (Exception exception1)
			{
				SysLog.WriteLog(3, exception1.Message);
			}
			return ptlXpressMappingEntity;
		}

		public List<PtlXpressMappingEntity> GetList(string pigeonHoleID)
		{
			List<PtlXpressMappingEntity> ptlXpressMappingEntities = new List<PtlXpressMappingEntity>();
			try
			{
				if (ConnectorFactory.Connect())
				{
					string str = "SELECT PigeonHoleID, SRC, IsFront, IsBack FROM tbl_ptl_xpress_mapping";
					str = string.Concat(str, " WHERE PigeonHoleID=@PigeonHoleID;");
					using (SqlCommand sqlCommand = new SqlCommand())
					{
						sqlCommand.Connection = ConnectorFactory.Connection;
						sqlCommand.CommandText = str;
						sqlCommand.Prepare();
						sqlCommand.Parameters.AddWithValue("@PigeonHoleID", pigeonHoleID);
						using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
						{
							try
							{
								try
								{
									while (sqlDataReader.Read())
									{
										PtlXpressMappingEntity ptlXpressMappingEntity = new PtlXpressMappingEntity();
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("PigeonHoleID")))
										{
											ptlXpressMappingEntity.PigeonHoleID = sqlDataReader.GetString(sqlDataReader.GetOrdinal("PigeonHoleID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("SRC")))
										{
											ptlXpressMappingEntity.SRC = sqlDataReader.GetString(sqlDataReader.GetOrdinal("SRC"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("IsFront")))
										{
											ptlXpressMappingEntity.IsFront = sqlDataReader.GetBoolean(sqlDataReader.GetOrdinal("IsFront"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("IsBack")))
										{
											ptlXpressMappingEntity.IsBack = sqlDataReader.GetBoolean(sqlDataReader.GetOrdinal("IsBack"));
										}
										ptlXpressMappingEntities.Add(ptlXpressMappingEntity);
									}
								}
								catch (Exception exception)
								{
									SysLog.WriteLog(3, exception.Message);
								}
							}
							finally
							{
								sqlDataReader.Close();
							}
						}
						sqlCommand.Dispose();
					}
				}
			}
			catch (Exception exception1)
			{
				SysLog.WriteLog(3, exception1.Message);
			}
			return ptlXpressMappingEntities;
		}

		public List<PtlXpressMappingEntity> GetList()
		{
			List<PtlXpressMappingEntity> ptlXpressMappingEntities = new List<PtlXpressMappingEntity>();
			try
			{
				if (ConnectorFactory.Connect())
				{
					string str = "SELECT PigeonHoleID, SRC, IsFront, IsBack FROM tbl_ptl_xpress_mapping";
					using (SqlCommand sqlCommand = new SqlCommand())
					{
						sqlCommand.Connection = ConnectorFactory.Connection;
						sqlCommand.CommandText = str;
						sqlCommand.Prepare();
						using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
						{
							try
							{
								try
								{
									while (sqlDataReader.Read())
									{
										PtlXpressMappingEntity ptlXpressMappingEntity = new PtlXpressMappingEntity();
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("PigeonHoleID")))
										{
											ptlXpressMappingEntity.PigeonHoleID = sqlDataReader.GetString(sqlDataReader.GetOrdinal("PigeonHoleID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("SRC")))
										{
											ptlXpressMappingEntity.SRC = sqlDataReader.GetString(sqlDataReader.GetOrdinal("SRC"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("IsFront")))
										{
											ptlXpressMappingEntity.IsFront = sqlDataReader.GetBoolean(sqlDataReader.GetOrdinal("IsFront"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("IsBack")))
										{
											ptlXpressMappingEntity.IsBack = sqlDataReader.GetBoolean(sqlDataReader.GetOrdinal("IsBack"));
										}
										ptlXpressMappingEntities.Add(ptlXpressMappingEntity);
									}
								}
								catch (Exception exception)
								{
									SysLog.WriteLog(3, exception.Message);
								}
							}
							finally
							{
								sqlDataReader.Close();
							}
						}
						sqlCommand.Dispose();
					}
				}
			}
			catch (Exception exception1)
			{
				SysLog.WriteLog(3, exception1.Message);
			}
			return ptlXpressMappingEntities;
		}

		public PtlXpressMappingEntity GetSRC(string SRC)
		{
			PtlXpressMappingEntity ptlXpressMappingEntity = null;
			try
			{
				if (ConnectorFactory.Connect())
				{
					string str = "SELECT PigeonHoleID, SRC, IsFront, IsBack FROM tbl_ptl_xpress_mapping";
					str = string.Concat(str, " WHERE SRC=@SRC;");
					using (SqlCommand sqlCommand = new SqlCommand())
					{
						sqlCommand.Connection = ConnectorFactory.Connection;
						sqlCommand.CommandText = str;
						sqlCommand.Prepare();
						sqlCommand.Parameters.AddWithValue("@SRC", SRC);
						using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
						{
							try
							{
								try
								{
									if (sqlDataReader.Read())
									{
										ptlXpressMappingEntity = new PtlXpressMappingEntity();
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("PigeonHoleID")))
										{
											ptlXpressMappingEntity.PigeonHoleID = sqlDataReader.GetString(sqlDataReader.GetOrdinal("PigeonHoleID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("SRC")))
										{
											ptlXpressMappingEntity.SRC = sqlDataReader.GetString(sqlDataReader.GetOrdinal("SRC"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("IsFront")))
										{
											ptlXpressMappingEntity.IsFront = sqlDataReader.GetBoolean(sqlDataReader.GetOrdinal("IsFront"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("IsBack")))
										{
											ptlXpressMappingEntity.IsBack = sqlDataReader.GetBoolean(sqlDataReader.GetOrdinal("IsBack"));
										}
									}
								}
								catch (Exception exception)
								{
									SysLog.WriteLog(3, exception.Message);
								}
							}
							finally
							{
								sqlDataReader.Close();
							}
						}
						sqlCommand.Dispose();
					}
				}
			}
			catch (Exception exception1)
			{
				SysLog.WriteLog(3, exception1.Message);
			}
			return ptlXpressMappingEntity;
		}

		public bool Save(PtlXpressMappingEntity entity)
		{
			bool flag = false;
			try
			{
				if (ConnectorFactory.Connect())
				{
					string str = "UPDATE tbl_ptl_xpress_mapping SET SRC=@SRC, IsFront=@IsFront, IsBack=@IsBack WHERE PigeonHoleID=@PigeonHoleID";
					using (SqlCommand sqlCommand = new SqlCommand())
					{
						sqlCommand.Connection = ConnectorFactory.Connection;
						sqlCommand.CommandText = str;
						sqlCommand.Prepare();
						sqlCommand.Parameters.AddWithValue("@PigeonHoleID", entity.PigeonHoleID);
						sqlCommand.Parameters.AddWithValue("@SRC", entity.SRC);
						sqlCommand.Parameters.AddWithValue("@IsFront", entity.IsFront);
						sqlCommand.Parameters.AddWithValue("@IsBack", entity.IsBack);
						object obj = sqlCommand.ExecuteNonQuery();
						flag = true;
						sqlCommand.Dispose();
					}
				}
			}
			catch (Exception exception)
			{
				SysLog.WriteLog(3, exception.Message);
			}
			return flag;
		}
	}
}