using MSClassLibrary.Log;
using OPtlDAL.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;

namespace OPtlDAL.DataFactory
{
	internal class DailyTaskFactory
	{
		public DailyTaskFactory()
		{
		}

		public bool Add(ref DailyTaskEntity entity)
		{
			bool flag = false;
			try
			{
				if (ConnectorFactory.Connect())
				{
					string str = "INSERT INTO tbl_daily_task (ID, Description, LastExecutionTimeStamp) VALUES (@ID, @Description, @LastExecutionTimeStamp);";
					using (SqlCommand sqlCommand = new SqlCommand())
					{
						sqlCommand.Connection = ConnectorFactory.Connection;
						sqlCommand.CommandText = str;
						sqlCommand.Prepare();
						sqlCommand.Parameters.AddWithValue("@ID", entity.ID);
						sqlCommand.Parameters.AddWithValue("@Description", entity.Description);
						if (entity.LastExecutionTimeStamp != DateTime.MinValue)
						{
							sqlCommand.Parameters.AddWithValue("@LastExecutionTimeStamp", entity.LastExecutionTimeStamp);
						}
						else
						{
							sqlCommand.Parameters.AddWithValue("@LastExecutionTimeStamp", DateTime.Now);
						}
						sqlCommand.ExecuteScalar();
						flag = true;
						sqlCommand.Dispose();
					}
				}
			}
			catch (Exception exception)
			{
				SysLog.WriteLog(3, exception.Message);
			}
			return flag;
		}

		public bool Delete(DailyTaskEntity entity)
		{
			bool flag = false;
			try
			{
				if (ConnectorFactory.Connect())
				{
					string str = "DELETE FROM tbl_daily_task WHERE ID=@ID";
					using (SqlCommand sqlCommand = new SqlCommand())
					{
						sqlCommand.Connection = ConnectorFactory.Connection;
						sqlCommand.CommandText = str;
						sqlCommand.Prepare();
						sqlCommand.Parameters.AddWithValue("@ID", entity.ID);
						object obj = sqlCommand.ExecuteNonQuery();
						flag = true;
						sqlCommand.Dispose();
					}
				}
			}
			catch (Exception exception)
			{
				SysLog.WriteLog(3, exception.Message);
			}
			return flag;
		}

		public DailyTaskEntity Get(Guid ID)
		{
			DailyTaskEntity dailyTaskEntity = null;
			try
			{
				if (ConnectorFactory.Connect())
				{
					string str = "SELECT ID, Description, LastExecutionTimeStamp FROM tbl_daily_task";
					str = string.Concat(str, " WHERE ID=@ID;");
					using (SqlCommand sqlCommand = new SqlCommand())
					{
						sqlCommand.Connection = ConnectorFactory.Connection;
						sqlCommand.CommandText = str;
						sqlCommand.Prepare();
						sqlCommand.Parameters.AddWithValue("@ID", ID);
						using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
						{
							try
							{
								try
								{
									if (sqlDataReader.Read())
									{
										dailyTaskEntity = new DailyTaskEntity();
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ID")))
										{
											dailyTaskEntity.ID = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("ID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("Description")))
										{
											dailyTaskEntity.Description = sqlDataReader.GetString(sqlDataReader.GetOrdinal("Description"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("LastExecutionTimeStamp")))
										{
											dailyTaskEntity.LastExecutionTimeStamp = sqlDataReader.GetDateTime(sqlDataReader.GetOrdinal("LastExecutionTimeStamp"));
										}
									}
								}
								catch (Exception exception)
								{
									SysLog.WriteLog(3, exception.Message);
								}
							}
							finally
							{
								sqlDataReader.Close();
							}
						}
						sqlCommand.Dispose();
					}
				}
			}
			catch (Exception exception1)
			{
				SysLog.WriteLog(3, exception1.Message);
			}
			return dailyTaskEntity;
		}

		public List<DailyTaskEntity> GetList()
		{
			List<DailyTaskEntity> dailyTaskEntities = new List<DailyTaskEntity>();
			try
			{
				if (ConnectorFactory.Connect())
				{
					string str = "SELECT ID, Description, LastExecutionTimeStamp FROM tbl_daily_task";
					using (SqlCommand sqlCommand = new SqlCommand())
					{
						sqlCommand.Connection = ConnectorFactory.Connection;
						sqlCommand.CommandText = str;
						sqlCommand.Prepare();
						using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
						{
							try
							{
								try
								{
									while (sqlDataReader.Read())
									{
										DailyTaskEntity dailyTaskEntity = new DailyTaskEntity();
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ID")))
										{
											dailyTaskEntity.ID = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("ID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("Description")))
										{
											dailyTaskEntity.Description = sqlDataReader.GetString(sqlDataReader.GetOrdinal("Description"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("LastExecutionTimeStamp")))
										{
											dailyTaskEntity.LastExecutionTimeStamp = sqlDataReader.GetDateTime(sqlDataReader.GetOrdinal("LastExecutionTimeStamp"));
										}
										dailyTaskEntities.Add(dailyTaskEntity);
									}
								}
								catch (Exception exception)
								{
									SysLog.WriteLog(3, exception.Message);
								}
							}
							finally
							{
								sqlDataReader.Close();
							}
						}
						sqlCommand.Dispose();
					}
				}
			}
			catch (Exception exception1)
			{
				SysLog.WriteLog(3, exception1.Message);
			}
			return dailyTaskEntities;
		}

		public bool Save(DailyTaskEntity entity)
		{
			bool flag = false;
			try
			{
				if (ConnectorFactory.Connect())
				{
					string str = "UPDATE tbl_daily_task SET Description=@Description, LastExecutionTimeStamp=@LastExecutionTimeStamp WHERE ID=@ID";
					using (SqlCommand sqlCommand = new SqlCommand())
					{
						sqlCommand.Connection = ConnectorFactory.Connection;
						sqlCommand.CommandText = str;
						sqlCommand.Prepare();
						sqlCommand.Parameters.AddWithValue("@ID", entity.ID);
						sqlCommand.Parameters.AddWithValue("@Description", entity.Description);
						if (entity.LastExecutionTimeStamp != DateTime.MinValue)
						{
							sqlCommand.Parameters.AddWithValue("@LastExecutionTimeStamp", entity.LastExecutionTimeStamp);
						}
						else
						{
							sqlCommand.Parameters.AddWithValue("@LastExecutionTimeStamp", DateTime.Now);
						}
						object obj = sqlCommand.ExecuteNonQuery();
						flag = true;
						sqlCommand.Dispose();
					}
				}
			}
			catch (Exception exception)
			{
				SysLog.WriteLog(3, exception.Message);
			}
			return flag;
		}
	}
}