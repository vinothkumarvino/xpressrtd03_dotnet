using MSClassLibrary.Log;
using OPtlDAL.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;

namespace OPtlDAL.DataFactory
{
	internal class PtlSystemFactory
	{
		public PtlSystemFactory()
		{
		}

		public bool Add(ref PtlSystemEntity entity)
		{
			bool flag = false;
			try
			{
				if (ConnectorFactory.Connect())
				{
					string str = "INSERT INTO tbl_ptl_system (SystemName, UnitNo, GroupName, Mode, ModeString, IPAddress, IPPort, Description, Area, IsActive, Status, StatusMessage, Notes, StartDateTime, LastStopDateTime, CreatedBy, CreatedOn, UpdatedBy, UpdatedOn) VALUES (@SystemName, @UnitNo, @GroupName, @Mode, @ModeString, @IPAddress, @IPPort, @Description, @Area, @IsActive, @Status, @StatusMessage, @Notes, @StartDateTime, @LastStopDateTime, @CreatedBy, @CreatedOn, @UpdatedBy, @UpdatedOn);";
					using (SqlCommand sqlCommand = new SqlCommand())
					{
						sqlCommand.Connection = ConnectorFactory.Connection;
						sqlCommand.CommandText = str;
						sqlCommand.Prepare();
						sqlCommand.Parameters.AddWithValue("@SystemName", entity.SystemName);
						sqlCommand.Parameters.AddWithValue("@UnitNo", entity.UnitNo);
						sqlCommand.Parameters.AddWithValue("@GroupName", entity.GroupName);
						sqlCommand.Parameters.AddWithValue("@Mode", entity.Mode);
						sqlCommand.Parameters.AddWithValue("@ModeString", entity.ModeString);
						sqlCommand.Parameters.AddWithValue("@IPAddress", entity.IPAddress);
						sqlCommand.Parameters.AddWithValue("@IPPort", entity.IPPort);
						sqlCommand.Parameters.AddWithValue("@Description", entity.Description);
						sqlCommand.Parameters.AddWithValue("@Area", entity.Area);
						sqlCommand.Parameters.AddWithValue("@IsActive", entity.IsActive);
						sqlCommand.Parameters.AddWithValue("@Status", entity.Status);
						sqlCommand.Parameters.AddWithValue("@StatusMessage", entity.StatusMessage);
						sqlCommand.Parameters.AddWithValue("@Notes", entity.Notes);
						if (entity.StartDateTime != DateTime.MinValue)
						{
							sqlCommand.Parameters.AddWithValue("@StartDateTime", entity.StartDateTime);
						}
						else
						{
							sqlCommand.Parameters.AddWithValue("@StartDateTime", DateTime.Now);
						}
						if (entity.LastStopDateTime != DateTime.MinValue)
						{
							sqlCommand.Parameters.AddWithValue("@LastStopDateTime", entity.LastStopDateTime);
						}
						else
						{
							sqlCommand.Parameters.AddWithValue("@LastStopDateTime", DateTime.Now);
						}
						sqlCommand.Parameters.AddWithValue("@CreatedBy", entity.CreatedBy);
						if (entity.CreatedOn != DateTime.MinValue)
						{
							sqlCommand.Parameters.AddWithValue("@CreatedOn", entity.CreatedOn);
						}
						else
						{
							sqlCommand.Parameters.AddWithValue("@CreatedOn", DateTime.Now);
						}
						sqlCommand.Parameters.AddWithValue("@UpdatedBy", entity.UpdatedBy);
						if (entity.UpdatedOn != DateTime.MinValue)
						{
							sqlCommand.Parameters.AddWithValue("@UpdatedOn", entity.UpdatedOn);
						}
						else
						{
							sqlCommand.Parameters.AddWithValue("@UpdatedOn", DateTime.Now);
						}
						sqlCommand.ExecuteScalar();
						flag = true;
						sqlCommand.Dispose();
					}
				}
			}
			catch (Exception exception)
			{
				SysLog.WriteLog(3, exception.Message);
			}
			return flag;
		}

		public bool Delete(PtlSystemEntity entity)
		{
			bool flag = false;
			try
			{
				if (ConnectorFactory.Connect())
				{
					string str = "DELETE FROM tbl_ptl_system WHERE SystemName=@SystemName";
					using (SqlCommand sqlCommand = new SqlCommand())
					{
						sqlCommand.Connection = ConnectorFactory.Connection;
						sqlCommand.CommandText = str;
						sqlCommand.Prepare();
						sqlCommand.Parameters.AddWithValue("@SystemName", entity.SystemName);
						object obj = sqlCommand.ExecuteNonQuery();
						flag = true;
						sqlCommand.Dispose();
					}
				}
			}
			catch (Exception exception)
			{
				SysLog.WriteLog(3, exception.Message);
			}
			return flag;
		}

		public PtlSystemEntity Get(string systemName)
		{
			PtlSystemEntity ptlSystemEntity = null;
			try
			{
				if (ConnectorFactory.Connect())
				{
					string str = "SELECT SystemName, UnitNo, GroupName, Mode, ModeString, IPAddress, IPPort, Description, Area, IsActive, Status, StatusMessage, Notes, StartDateTime, LastStopDateTime, CreatedBy, CreatedOn, UpdatedBy, UpdatedOn FROM tbl_ptl_system";
					str = string.Concat(str, " WHERE SystemName=@SystemName;");
					using (SqlCommand sqlCommand = new SqlCommand())
					{
						sqlCommand.Connection = ConnectorFactory.Connection;
						sqlCommand.CommandText = str;
						sqlCommand.Prepare();
						sqlCommand.Parameters.AddWithValue("@SystemName", systemName);
						using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
						{
							try
							{
								try
								{
									if (sqlDataReader.Read())
									{
										ptlSystemEntity = new PtlSystemEntity();
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("SystemName")))
										{
											ptlSystemEntity.SystemName = sqlDataReader.GetString(sqlDataReader.GetOrdinal("SystemName"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("UnitNo")))
										{
											ptlSystemEntity.UnitNo = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("UnitNo"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("GroupName")))
										{
											ptlSystemEntity.GroupName = sqlDataReader.GetString(sqlDataReader.GetOrdinal("GroupName"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("Mode")))
										{
											ptlSystemEntity.Mode = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("Mode"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ModeString")))
										{
											ptlSystemEntity.ModeString = sqlDataReader.GetString(sqlDataReader.GetOrdinal("ModeString"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("IPAddress")))
										{
											ptlSystemEntity.IPAddress = sqlDataReader.GetString(sqlDataReader.GetOrdinal("IPAddress"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("IPPort")))
										{
											ptlSystemEntity.IPPort = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("IPPort"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("Description")))
										{
											ptlSystemEntity.Description = sqlDataReader.GetString(sqlDataReader.GetOrdinal("Description"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("Area")))
										{
											ptlSystemEntity.Area = sqlDataReader.GetString(sqlDataReader.GetOrdinal("Area"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("IsActive")))
										{
											ptlSystemEntity.IsActive = sqlDataReader.GetBoolean(sqlDataReader.GetOrdinal("IsActive"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("Status")))
										{
											ptlSystemEntity.Status = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("Status"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("StatusMessage")))
										{
											ptlSystemEntity.StatusMessage = sqlDataReader.GetString(sqlDataReader.GetOrdinal("StatusMessage"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("Notes")))
										{
											ptlSystemEntity.Notes = sqlDataReader.GetString(sqlDataReader.GetOrdinal("Notes"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("StartDateTime")))
										{
											ptlSystemEntity.StartDateTime = sqlDataReader.GetDateTime(sqlDataReader.GetOrdinal("StartDateTime"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("LastStopDateTime")))
										{
											ptlSystemEntity.LastStopDateTime = sqlDataReader.GetDateTime(sqlDataReader.GetOrdinal("LastStopDateTime"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("CreatedBy")))
										{
											ptlSystemEntity.CreatedBy = sqlDataReader.GetString(sqlDataReader.GetOrdinal("CreatedBy"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("CreatedOn")))
										{
											ptlSystemEntity.CreatedOn = sqlDataReader.GetDateTime(sqlDataReader.GetOrdinal("CreatedOn"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("UpdatedBy")))
										{
											ptlSystemEntity.UpdatedBy = sqlDataReader.GetString(sqlDataReader.GetOrdinal("UpdatedBy"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("UpdatedOn")))
										{
											ptlSystemEntity.UpdatedOn = sqlDataReader.GetDateTime(sqlDataReader.GetOrdinal("UpdatedOn"));
										}
									}
								}
								catch (Exception exception)
								{
									SysLog.WriteLog(3, exception.Message);
								}
							}
							finally
							{
								sqlDataReader.Close();
							}
						}
						sqlCommand.Dispose();
					}
				}
			}
			catch (Exception exception1)
			{
				SysLog.WriteLog(3, exception1.Message);
			}
			return ptlSystemEntity;
		}

		public List<PtlSystemEntity> GetControllerList(string groupName)
		{
			List<PtlSystemEntity> ptlSystemEntities = new List<PtlSystemEntity>();
			try
			{
				if (ConnectorFactory.Connect())
				{
					string str = "SELECT SystemName, UnitNo, GroupName, Mode, ModeString, IPAddress, IPPort, Description, Area, IsActive, Status, StatusMessage, Notes, StartDateTime, LastStopDateTime, CreatedBy, CreatedOn, UpdatedBy, UpdatedOn FROM tbl_ptl_system";
					str = string.Concat(str, " WHERE GroupName=@GroupName AND IsActive=@IsActive");
					using (SqlCommand sqlCommand = new SqlCommand())
					{
						sqlCommand.Connection = ConnectorFactory.Connection;
						sqlCommand.CommandText = str;
						sqlCommand.Prepare();
						sqlCommand.Parameters.AddWithValue("@GroupName", groupName);
						sqlCommand.Parameters.AddWithValue("@IsActive", true);
						using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
						{
							try
							{
								try
								{
									while (sqlDataReader.Read())
									{
										PtlSystemEntity ptlSystemEntity = new PtlSystemEntity();
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("SystemName")))
										{
											ptlSystemEntity.SystemName = sqlDataReader.GetString(sqlDataReader.GetOrdinal("SystemName"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("UnitNo")))
										{
											ptlSystemEntity.UnitNo = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("UnitNo"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("GroupName")))
										{
											ptlSystemEntity.GroupName = sqlDataReader.GetString(sqlDataReader.GetOrdinal("GroupName"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("Mode")))
										{
											ptlSystemEntity.Mode = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("Mode"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ModeString")))
										{
											ptlSystemEntity.ModeString = sqlDataReader.GetString(sqlDataReader.GetOrdinal("ModeString"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("IPAddress")))
										{
											ptlSystemEntity.IPAddress = sqlDataReader.GetString(sqlDataReader.GetOrdinal("IPAddress"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("IPPort")))
										{
											ptlSystemEntity.IPPort = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("IPPort"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("Description")))
										{
											ptlSystemEntity.Description = sqlDataReader.GetString(sqlDataReader.GetOrdinal("Description"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("Area")))
										{
											ptlSystemEntity.Area = sqlDataReader.GetString(sqlDataReader.GetOrdinal("Area"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("IsActive")))
										{
											ptlSystemEntity.IsActive = sqlDataReader.GetBoolean(sqlDataReader.GetOrdinal("IsActive"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("Status")))
										{
											ptlSystemEntity.Status = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("Status"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("StatusMessage")))
										{
											ptlSystemEntity.StatusMessage = sqlDataReader.GetString(sqlDataReader.GetOrdinal("StatusMessage"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("Notes")))
										{
											ptlSystemEntity.Notes = sqlDataReader.GetString(sqlDataReader.GetOrdinal("Notes"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("StartDateTime")))
										{
											ptlSystemEntity.StartDateTime = sqlDataReader.GetDateTime(sqlDataReader.GetOrdinal("StartDateTime"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("LastStopDateTime")))
										{
											ptlSystemEntity.LastStopDateTime = sqlDataReader.GetDateTime(sqlDataReader.GetOrdinal("LastStopDateTime"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("CreatedBy")))
										{
											ptlSystemEntity.CreatedBy = sqlDataReader.GetString(sqlDataReader.GetOrdinal("CreatedBy"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("CreatedOn")))
										{
											ptlSystemEntity.CreatedOn = sqlDataReader.GetDateTime(sqlDataReader.GetOrdinal("CreatedOn"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("UpdatedBy")))
										{
											ptlSystemEntity.UpdatedBy = sqlDataReader.GetString(sqlDataReader.GetOrdinal("UpdatedBy"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("UpdatedOn")))
										{
											ptlSystemEntity.UpdatedOn = sqlDataReader.GetDateTime(sqlDataReader.GetOrdinal("UpdatedOn"));
										}
										ptlSystemEntities.Add(ptlSystemEntity);
									}
								}
								catch (Exception exception)
								{
									SysLog.WriteLog(3, exception.Message);
								}
							}
							finally
							{
								sqlDataReader.Close();
							}
						}
						sqlCommand.Dispose();
					}
				}
			}
			catch (Exception exception1)
			{
				SysLog.WriteLog(3, exception1.Message);
			}
			return ptlSystemEntities;
		}

		public List<PtlSystemEntity> GetList()
		{
			List<PtlSystemEntity> ptlSystemEntities = new List<PtlSystemEntity>();
			try
			{
				if (ConnectorFactory.Connect())
				{
					string str = "SELECT SystemName, UnitNo, GroupName, Mode, ModeString, IPAddress, IPPort, Description, Area, IsActive, Status, StatusMessage, Notes, StartDateTime, LastStopDateTime, CreatedBy, CreatedOn, UpdatedBy, UpdatedOn FROM tbl_ptl_system";
					using (SqlCommand sqlCommand = new SqlCommand())
					{
						sqlCommand.Connection = ConnectorFactory.Connection;
						sqlCommand.CommandText = str;
						sqlCommand.Prepare();
						using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
						{
							try
							{
								try
								{
									while (sqlDataReader.Read())
									{
										PtlSystemEntity ptlSystemEntity = new PtlSystemEntity();
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("SystemName")))
										{
											ptlSystemEntity.SystemName = sqlDataReader.GetString(sqlDataReader.GetOrdinal("SystemName"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("UnitNo")))
										{
											ptlSystemEntity.UnitNo = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("UnitNo"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("GroupName")))
										{
											ptlSystemEntity.GroupName = sqlDataReader.GetString(sqlDataReader.GetOrdinal("GroupName"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("Mode")))
										{
											ptlSystemEntity.Mode = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("Mode"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ModeString")))
										{
											ptlSystemEntity.ModeString = sqlDataReader.GetString(sqlDataReader.GetOrdinal("ModeString"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("IPAddress")))
										{
											ptlSystemEntity.IPAddress = sqlDataReader.GetString(sqlDataReader.GetOrdinal("IPAddress"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("IPPort")))
										{
											ptlSystemEntity.IPPort = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("IPPort"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("Description")))
										{
											ptlSystemEntity.Description = sqlDataReader.GetString(sqlDataReader.GetOrdinal("Description"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("Area")))
										{
											ptlSystemEntity.Area = sqlDataReader.GetString(sqlDataReader.GetOrdinal("Area"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("IsActive")))
										{
											ptlSystemEntity.IsActive = sqlDataReader.GetBoolean(sqlDataReader.GetOrdinal("IsActive"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("Status")))
										{
											ptlSystemEntity.Status = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("Status"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("StatusMessage")))
										{
											ptlSystemEntity.StatusMessage = sqlDataReader.GetString(sqlDataReader.GetOrdinal("StatusMessage"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("Notes")))
										{
											ptlSystemEntity.Notes = sqlDataReader.GetString(sqlDataReader.GetOrdinal("Notes"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("StartDateTime")))
										{
											ptlSystemEntity.StartDateTime = sqlDataReader.GetDateTime(sqlDataReader.GetOrdinal("StartDateTime"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("LastStopDateTime")))
										{
											ptlSystemEntity.LastStopDateTime = sqlDataReader.GetDateTime(sqlDataReader.GetOrdinal("LastStopDateTime"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("CreatedBy")))
										{
											ptlSystemEntity.CreatedBy = sqlDataReader.GetString(sqlDataReader.GetOrdinal("CreatedBy"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("CreatedOn")))
										{
											ptlSystemEntity.CreatedOn = sqlDataReader.GetDateTime(sqlDataReader.GetOrdinal("CreatedOn"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("UpdatedBy")))
										{
											ptlSystemEntity.UpdatedBy = sqlDataReader.GetString(sqlDataReader.GetOrdinal("UpdatedBy"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("UpdatedOn")))
										{
											ptlSystemEntity.UpdatedOn = sqlDataReader.GetDateTime(sqlDataReader.GetOrdinal("UpdatedOn"));
										}
										ptlSystemEntities.Add(ptlSystemEntity);
									}
								}
								catch (Exception exception)
								{
									SysLog.WriteLog(3, exception.Message);
								}
							}
							finally
							{
								sqlDataReader.Close();
							}
						}
						sqlCommand.Dispose();
					}
				}
			}
			catch (Exception exception1)
			{
				SysLog.WriteLog(3, exception1.Message);
			}
			return ptlSystemEntities;
		}

		public bool Save(PtlSystemEntity entity)
		{
			bool flag = false;
			try
			{
				if (ConnectorFactory.Connect())
				{
					string str = "UPDATE tbl_ptl_system SET UnitNo=@UnitNo, GroupName=@GroupName, Mode=@Mode, ModeString=@ModeString, IPAddress=@IPAddress, IPPort=@IPPort, Description=@Description, Area=@Area, IsActive=@IsActive, Status=@Status, StatusMessage=@StatusMessage, Notes=@Notes, StartDateTime=@StartDateTime, LastStopDateTime=@LastStopDateTime, CreatedBy=@CreatedBy, CreatedOn=@CreatedOn, UpdatedBy=@UpdatedBy, UpdatedOn=@UpdatedOn WHERE SystemName=@SystemName";
					using (SqlCommand sqlCommand = new SqlCommand())
					{
						sqlCommand.Connection = ConnectorFactory.Connection;
						sqlCommand.CommandText = str;
						sqlCommand.Prepare();
						sqlCommand.Parameters.AddWithValue("@SystemName", entity.SystemName);
						sqlCommand.Parameters.AddWithValue("@UnitNo", entity.UnitNo);
						sqlCommand.Parameters.AddWithValue("@GroupName", entity.GroupName);
						sqlCommand.Parameters.AddWithValue("@Mode", entity.Mode);
						sqlCommand.Parameters.AddWithValue("@ModeString", entity.ModeString);
						sqlCommand.Parameters.AddWithValue("@IPAddress", entity.IPAddress);
						sqlCommand.Parameters.AddWithValue("@IPPort", entity.IPPort);
						sqlCommand.Parameters.AddWithValue("@Description", entity.Description);
						sqlCommand.Parameters.AddWithValue("@Area", entity.Area);
						sqlCommand.Parameters.AddWithValue("@IsActive", entity.IsActive);
						sqlCommand.Parameters.AddWithValue("@Status", entity.Status);
						sqlCommand.Parameters.AddWithValue("@StatusMessage", entity.StatusMessage);
						sqlCommand.Parameters.AddWithValue("@Notes", entity.Notes);
						if (entity.StartDateTime != DateTime.MinValue)
						{
							sqlCommand.Parameters.AddWithValue("@StartDateTime", entity.StartDateTime);
						}
						else
						{
							sqlCommand.Parameters.AddWithValue("@StartDateTime", DBNull.Value);
						}
						if (entity.LastStopDateTime != DateTime.MinValue)
						{
							sqlCommand.Parameters.AddWithValue("@LastStopDateTime", entity.LastStopDateTime);
						}
						else
						{
							sqlCommand.Parameters.AddWithValue("@LastStopDateTime", DBNull.Value);
						}
						sqlCommand.Parameters.AddWithValue("@CreatedBy", entity.CreatedBy);
						if (entity.CreatedOn != DateTime.MinValue)
						{
							sqlCommand.Parameters.AddWithValue("@CreatedOn", entity.CreatedOn);
						}
						else
						{
							sqlCommand.Parameters.AddWithValue("@CreatedOn", DateTime.Now);
						}
						sqlCommand.Parameters.AddWithValue("@UpdatedBy", entity.UpdatedBy);
						if (entity.UpdatedOn != DateTime.MinValue)
						{
							sqlCommand.Parameters.AddWithValue("@UpdatedOn", entity.UpdatedOn);
						}
						else
						{
							sqlCommand.Parameters.AddWithValue("@UpdatedOn", DateTime.Now);
						}
						object obj = sqlCommand.ExecuteNonQuery();
						flag = true;
						sqlCommand.Dispose();
					}
				}
			}
			catch (Exception exception)
			{
				SysLog.WriteLog(3, exception.Message);
			}
			return flag;
		}
	}
}