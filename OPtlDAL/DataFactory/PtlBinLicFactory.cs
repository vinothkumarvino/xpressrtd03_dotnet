using MSClassLibrary.Log;
using OPtlDAL.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;

namespace OPtlDAL.DataFactory
{
	internal class PtlBinLicFactory
	{
		public PtlBinLicFactory()
		{
		}

		public bool Add(ref PtlBinLicEntity entity)
		{
			bool flag = false;
			try
			{
				if (ConnectorFactory.Connect())
				{
					string str = "INSERT INTO tbl_ptl_bin_lic (ID, SystemName, BinID, SRC, ModuleID, ModuleColor, ModuleFlash, ModuleBuzzer, IsLightCommandIssued, DisplayContent, ArticleID, StockIn, StockOut, Adjustment, Remarks, RequestID, RequestCode, LightOnOffCommand, OnTimeStamp, TurnOffByButtonPress, TurnOffByCommand, OffTimeStamp, UpdatedBy, UpdatedOn, CreatedBy, CreatedOn) VALUES (@ID, @SystemName, @BinID, @SRC, @ModuleID, @ModuleColor, @ModuleFlash, @ModuleBuzzer, @IsLightCommandIssued, @DisplayContent, @ArticleID, @StockIn, @StockOut, @Adjustment, @Remarks, @RequestID, @RequestCode, @LightOnOffCommand, @OnTimeStamp, @TurnOffByButtonPress, @TurnOffByCommand, @OffTimeStamp, @UpdatedBy, @UpdatedOn, @CreatedBy, @CreatedOn);";
					using (SqlCommand sqlCommand = new SqlCommand())
					{
						sqlCommand.Connection = ConnectorFactory.Connection;
						sqlCommand.CommandText = str;
						sqlCommand.Prepare();
						sqlCommand.Parameters.AddWithValue("@ID", entity.ID);
						sqlCommand.Parameters.AddWithValue("@SystemName", entity.SystemName);
						sqlCommand.Parameters.AddWithValue("@BinID", entity.BinID);
						sqlCommand.Parameters.AddWithValue("@SRC", entity.SRC);
						sqlCommand.Parameters.AddWithValue("@ModuleID", entity.ModuleID);
						sqlCommand.Parameters.AddWithValue("@ModuleColor", entity.ModuleColor);
						sqlCommand.Parameters.AddWithValue("@ModuleFlash", entity.ModuleFlash);
						sqlCommand.Parameters.AddWithValue("@ModuleBuzzer", entity.ModuleBuzzer);
						sqlCommand.Parameters.AddWithValue("@IsLightCommandIssued", entity.IsLightCommandIssued);
						sqlCommand.Parameters.AddWithValue("@DisplayContent", entity.DisplayContent);
						sqlCommand.Parameters.AddWithValue("@ArticleID", entity.ArticleID);
						sqlCommand.Parameters.AddWithValue("@StockIn", entity.StockIn);
						sqlCommand.Parameters.AddWithValue("@StockOut", entity.StockOut);
						sqlCommand.Parameters.AddWithValue("@Adjustment", entity.Adjustment);
						sqlCommand.Parameters.AddWithValue("@Remarks", entity.Remarks);
						sqlCommand.Parameters.AddWithValue("@RequestID", entity.RequestID);
						sqlCommand.Parameters.AddWithValue("@RequestCode", entity.RequestCode);
						sqlCommand.Parameters.AddWithValue("@LightOnOffCommand", entity.LightOnOffCommand);
						if (entity.OnTimeStamp != DateTime.MinValue)
						{
							sqlCommand.Parameters.AddWithValue("@OnTimeStamp", entity.OnTimeStamp);
						}
						else
						{
							sqlCommand.Parameters.AddWithValue("@OnTimeStamp", DateTime.Now);
						}
						sqlCommand.Parameters.AddWithValue("@TurnOffByButtonPress", entity.TurnOffByButtonPress);
						sqlCommand.Parameters.AddWithValue("@TurnOffByCommand", entity.TurnOffByCommand);
						if (entity.OffTimeStamp != DateTime.MinValue)
						{
							sqlCommand.Parameters.AddWithValue("@OffTimeStamp", entity.OffTimeStamp);
						}
						else
						{
							sqlCommand.Parameters.AddWithValue("@OffTimeStamp", DateTime.Now);
						}
						sqlCommand.Parameters.AddWithValue("@UpdatedBy", entity.UpdatedBy);
						if (entity.UpdatedOn != DateTime.MinValue)
						{
							sqlCommand.Parameters.AddWithValue("@UpdatedOn", entity.UpdatedOn);
						}
						else
						{
							sqlCommand.Parameters.AddWithValue("@UpdatedOn", DateTime.Now);
						}
						sqlCommand.Parameters.AddWithValue("@CreatedBy", entity.CreatedBy);
						if (entity.CreatedOn != DateTime.MinValue)
						{
							sqlCommand.Parameters.AddWithValue("@CreatedOn", entity.CreatedOn);
						}
						else
						{
							sqlCommand.Parameters.AddWithValue("@CreatedOn", DateTime.Now);
						}
						sqlCommand.ExecuteScalar();
						flag = true;
						sqlCommand.Dispose();
					}
				}
			}
			catch (Exception exception)
			{
				SysLog.WriteLog(3, exception.Message);
			}
			return flag;
		}

		public bool Delete(PtlBinLicEntity entity)
		{
			bool flag = false;
			try
			{
				if (ConnectorFactory.Connect())
				{
					string str = "DELETE FROM tbl_ptl_bin_lic WHERE ID=@ID";
					using (SqlCommand sqlCommand = new SqlCommand())
					{
						sqlCommand.Connection = ConnectorFactory.Connection;
						sqlCommand.CommandText = str;
						sqlCommand.Prepare();
						sqlCommand.Parameters.AddWithValue("@ID", entity.ID);
						object obj = sqlCommand.ExecuteNonQuery();
						flag = true;
						sqlCommand.Dispose();
					}
				}
			}
			catch (Exception exception)
			{
				SysLog.WriteLog(3, exception.Message);
			}
			return flag;
		}

		public PtlBinLicEntity Get(string srcReference)
		{
			PtlBinLicEntity ptlBinLicEntity = null;
			try
			{
				if (ConnectorFactory.Connect())
				{
					string str = "SELECT ID, SystemName, BinID, SRC, ModuleID, ModuleColor, ModuleFlash, ModuleBuzzer, IsLightCommandIssued, DisplayContent, ArticleID, StockIn, StockOut, Adjustment, Remarks, RequestID, RequestCode, LightOnOffCommand, OnTimeStamp, TurnOffByButtonPress, TurnOffByCommand, OffTimeStamp, UpdatedBy, UpdatedOn, CreatedBy, CreatedOn FROM tbl_ptl_bin_lic";
					str = string.Concat(str, " WHERE SRC=@SRC ORDER BY CreatedOn;");
					using (SqlCommand sqlCommand = new SqlCommand())
					{
						sqlCommand.Connection = ConnectorFactory.Connection;
						sqlCommand.CommandText = str;
						sqlCommand.Prepare();
						sqlCommand.Parameters.AddWithValue("@SRC", srcReference);
						using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
						{
							try
							{
								try
								{
									if (sqlDataReader.Read())
									{
										ptlBinLicEntity = new PtlBinLicEntity();
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ID")))
										{
											ptlBinLicEntity.ID = sqlDataReader.GetGuid(sqlDataReader.GetOrdinal("ID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("SystemName")))
										{
											ptlBinLicEntity.SystemName = sqlDataReader.GetString(sqlDataReader.GetOrdinal("SystemName"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("BinID")))
										{
											ptlBinLicEntity.BinID = sqlDataReader.GetGuid(sqlDataReader.GetOrdinal("BinID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("SRC")))
										{
											ptlBinLicEntity.SRC = sqlDataReader.GetString(sqlDataReader.GetOrdinal("SRC"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ModuleID")))
										{
											ptlBinLicEntity.ModuleID = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("ModuleID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ModuleColor")))
										{
											ptlBinLicEntity.ModuleColor = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("ModuleColor"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ModuleFlash")))
										{
											ptlBinLicEntity.ModuleFlash = sqlDataReader.GetBoolean(sqlDataReader.GetOrdinal("ModuleFlash"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ModuleBuzzer")))
										{
											ptlBinLicEntity.ModuleBuzzer = sqlDataReader.GetBoolean(sqlDataReader.GetOrdinal("ModuleBuzzer"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("IsLightCommandIssued")))
										{
											ptlBinLicEntity.IsLightCommandIssued = sqlDataReader.GetBoolean(sqlDataReader.GetOrdinal("IsLightCommandIssued"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("DisplayContent")))
										{
											ptlBinLicEntity.DisplayContent = sqlDataReader.GetString(sqlDataReader.GetOrdinal("DisplayContent"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ArticleID")))
										{
											ptlBinLicEntity.ArticleID = sqlDataReader.GetString(sqlDataReader.GetOrdinal("ArticleID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("StockIn")))
										{
											ptlBinLicEntity.StockIn = sqlDataReader.GetDecimal(sqlDataReader.GetOrdinal("StockIn"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("StockOut")))
										{
											ptlBinLicEntity.StockOut = sqlDataReader.GetDecimal(sqlDataReader.GetOrdinal("StockOut"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("Adjustment")))
										{
											ptlBinLicEntity.Adjustment = sqlDataReader.GetDecimal(sqlDataReader.GetOrdinal("Adjustment"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("Remarks")))
										{
											ptlBinLicEntity.Remarks = sqlDataReader.GetString(sqlDataReader.GetOrdinal("Remarks"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("RequestID")))
										{
											ptlBinLicEntity.RequestID = sqlDataReader.GetString(sqlDataReader.GetOrdinal("RequestID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("RequestCode")))
										{
											ptlBinLicEntity.RequestCode = sqlDataReader.GetString(sqlDataReader.GetOrdinal("RequestCode"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("LightOnOffCommand")))
										{
											ptlBinLicEntity.LightOnOffCommand = sqlDataReader.GetString(sqlDataReader.GetOrdinal("LightOnOffCommand"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("OnTimeStamp")))
										{
											ptlBinLicEntity.OnTimeStamp = sqlDataReader.GetDateTime(sqlDataReader.GetOrdinal("OnTimeStamp"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("TurnOffByButtonPress")))
										{
											ptlBinLicEntity.TurnOffByButtonPress = sqlDataReader.GetBoolean(sqlDataReader.GetOrdinal("TurnOffByButtonPress"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("TurnOffByCommand")))
										{
											ptlBinLicEntity.TurnOffByCommand = sqlDataReader.GetBoolean(sqlDataReader.GetOrdinal("TurnOffByCommand"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("OffTimeStamp")))
										{
											ptlBinLicEntity.OffTimeStamp = sqlDataReader.GetDateTime(sqlDataReader.GetOrdinal("OffTimeStamp"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("UpdatedBy")))
										{
											ptlBinLicEntity.UpdatedBy = sqlDataReader.GetString(sqlDataReader.GetOrdinal("UpdatedBy"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("UpdatedOn")))
										{
											ptlBinLicEntity.UpdatedOn = sqlDataReader.GetDateTime(sqlDataReader.GetOrdinal("UpdatedOn"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("CreatedBy")))
										{
											ptlBinLicEntity.CreatedBy = sqlDataReader.GetString(sqlDataReader.GetOrdinal("CreatedBy"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("CreatedOn")))
										{
											ptlBinLicEntity.CreatedOn = sqlDataReader.GetDateTime(sqlDataReader.GetOrdinal("CreatedOn"));
										}
									}
								}
								catch (Exception exception)
								{
									SysLog.WriteLog(3, exception.Message);
								}
							}
							finally
							{
								sqlDataReader.Close();
							}
						}
						sqlCommand.Dispose();
					}
				}
			}
			catch (Exception exception1)
			{
				SysLog.WriteLog(3, exception1.Message);
			}
			return ptlBinLicEntity;
		}

		public PtlBinLicEntity Get(Guid ID)
		{
			PtlBinLicEntity ptlBinLicEntity = null;
			try
			{
				if (ConnectorFactory.Connect())
				{
					string str = "SELECT ID, SystemName, BinID, SRC, ModuleID, ModuleColor, ModuleFlash, ModuleBuzzer, IsLightCommandIssued, DisplayContent, ArticleID, StockIn, StockOut, Adjustment, Remarks, RequestID, RequestCode, LightOnOffCommand, OnTimeStamp, TurnOffByButtonPress, TurnOffByCommand, OffTimeStamp, UpdatedBy, UpdatedOn, CreatedBy, CreatedOn FROM tbl_ptl_bin_lic";
					str = string.Concat(str, " WHERE ID=@ID;");
					using (SqlCommand sqlCommand = new SqlCommand())
					{
						sqlCommand.Connection = ConnectorFactory.Connection;
						sqlCommand.CommandText = str;
						sqlCommand.Prepare();
						sqlCommand.Parameters.AddWithValue("@ID", ID);
						using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
						{
							try
							{
								try
								{
									if (sqlDataReader.Read())
									{
										ptlBinLicEntity = new PtlBinLicEntity();
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ID")))
										{
											ptlBinLicEntity.ID = sqlDataReader.GetGuid(sqlDataReader.GetOrdinal("ID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("SystemName")))
										{
											ptlBinLicEntity.SystemName = sqlDataReader.GetString(sqlDataReader.GetOrdinal("SystemName"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("BinID")))
										{
											ptlBinLicEntity.BinID = sqlDataReader.GetGuid(sqlDataReader.GetOrdinal("BinID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("SRC")))
										{
											ptlBinLicEntity.SRC = sqlDataReader.GetString(sqlDataReader.GetOrdinal("SRC"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ModuleID")))
										{
											ptlBinLicEntity.ModuleID = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("ModuleID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ModuleColor")))
										{
											ptlBinLicEntity.ModuleColor = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("ModuleColor"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ModuleFlash")))
										{
											ptlBinLicEntity.ModuleFlash = sqlDataReader.GetBoolean(sqlDataReader.GetOrdinal("ModuleFlash"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ModuleBuzzer")))
										{
											ptlBinLicEntity.ModuleBuzzer = sqlDataReader.GetBoolean(sqlDataReader.GetOrdinal("ModuleBuzzer"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("IsLightCommandIssued")))
										{
											ptlBinLicEntity.IsLightCommandIssued = sqlDataReader.GetBoolean(sqlDataReader.GetOrdinal("IsLightCommandIssued"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("DisplayContent")))
										{
											ptlBinLicEntity.DisplayContent = sqlDataReader.GetString(sqlDataReader.GetOrdinal("DisplayContent"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ArticleID")))
										{
											ptlBinLicEntity.ArticleID = sqlDataReader.GetString(sqlDataReader.GetOrdinal("ArticleID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("StockIn")))
										{
											ptlBinLicEntity.StockIn = sqlDataReader.GetDecimal(sqlDataReader.GetOrdinal("StockIn"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("StockOut")))
										{
											ptlBinLicEntity.StockOut = sqlDataReader.GetDecimal(sqlDataReader.GetOrdinal("StockOut"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("Adjustment")))
										{
											ptlBinLicEntity.Adjustment = sqlDataReader.GetDecimal(sqlDataReader.GetOrdinal("Adjustment"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("Remarks")))
										{
											ptlBinLicEntity.Remarks = sqlDataReader.GetString(sqlDataReader.GetOrdinal("Remarks"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("RequestID")))
										{
											ptlBinLicEntity.RequestID = sqlDataReader.GetString(sqlDataReader.GetOrdinal("RequestID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("RequestCode")))
										{
											ptlBinLicEntity.RequestCode = sqlDataReader.GetString(sqlDataReader.GetOrdinal("RequestCode"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("LightOnOffCommand")))
										{
											ptlBinLicEntity.LightOnOffCommand = sqlDataReader.GetString(sqlDataReader.GetOrdinal("LightOnOffCommand"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("OnTimeStamp")))
										{
											ptlBinLicEntity.OnTimeStamp = sqlDataReader.GetDateTime(sqlDataReader.GetOrdinal("OnTimeStamp"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("TurnOffByButtonPress")))
										{
											ptlBinLicEntity.TurnOffByButtonPress = sqlDataReader.GetBoolean(sqlDataReader.GetOrdinal("TurnOffByButtonPress"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("TurnOffByCommand")))
										{
											ptlBinLicEntity.TurnOffByCommand = sqlDataReader.GetBoolean(sqlDataReader.GetOrdinal("TurnOffByCommand"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("OffTimeStamp")))
										{
											ptlBinLicEntity.OffTimeStamp = sqlDataReader.GetDateTime(sqlDataReader.GetOrdinal("OffTimeStamp"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("UpdatedBy")))
										{
											ptlBinLicEntity.UpdatedBy = sqlDataReader.GetString(sqlDataReader.GetOrdinal("UpdatedBy"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("UpdatedOn")))
										{
											ptlBinLicEntity.UpdatedOn = sqlDataReader.GetDateTime(sqlDataReader.GetOrdinal("UpdatedOn"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("CreatedBy")))
										{
											ptlBinLicEntity.CreatedBy = sqlDataReader.GetString(sqlDataReader.GetOrdinal("CreatedBy"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("CreatedOn")))
										{
											ptlBinLicEntity.CreatedOn = sqlDataReader.GetDateTime(sqlDataReader.GetOrdinal("CreatedOn"));
										}
									}
								}
								catch (Exception exception)
								{
									SysLog.WriteLog(3, exception.Message);
								}
							}
							finally
							{
								sqlDataReader.Close();
							}
						}
						sqlCommand.Dispose();
					}
				}
			}
			catch (Exception exception1)
			{
				SysLog.WriteLog(3, exception1.Message);
			}
			return ptlBinLicEntity;
		}

		public PtlBinLicEntity GetLIC(string srcReference)
		{
			PtlBinLicEntity ptlBinLicEntity = null;
			try
			{
				if (ConnectorFactory.Connect())
				{
					string str = "SELECT ID, SystemName, BinID, SRC, ModuleID, ModuleColor, ModuleFlash, ModuleBuzzer, IsLightCommandIssued, DisplayContent, ArticleID, StockIn, StockOut, Adjustment, Remarks, RequestID, RequestCode, LightOnOffCommand, OnTimeStamp, TurnOffByButtonPress, TurnOffByCommand, OffTimeStamp, UpdatedBy, UpdatedOn, CreatedBy, CreatedOn FROM tbl_ptl_bin_lic";
					str = string.Concat(str, " WHERE SRC=@SRC AND IsLightCommandIssued=@IsLightCommandIssued ORDER BY CreatedOn;");
					using (SqlCommand sqlCommand = new SqlCommand())
					{
						sqlCommand.Connection = ConnectorFactory.Connection;
						sqlCommand.CommandText = str;
						sqlCommand.Prepare();
						sqlCommand.Parameters.AddWithValue("@SRC", srcReference);
						sqlCommand.Parameters.AddWithValue("@IsLightCommandIssued", true);
						using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
						{
							try
							{
								try
								{
									if (sqlDataReader.Read())
									{
										ptlBinLicEntity = new PtlBinLicEntity();
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ID")))
										{
											ptlBinLicEntity.ID = sqlDataReader.GetGuid(sqlDataReader.GetOrdinal("ID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("SystemName")))
										{
											ptlBinLicEntity.SystemName = sqlDataReader.GetString(sqlDataReader.GetOrdinal("SystemName"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("BinID")))
										{
											ptlBinLicEntity.BinID = sqlDataReader.GetGuid(sqlDataReader.GetOrdinal("BinID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("SRC")))
										{
											ptlBinLicEntity.SRC = sqlDataReader.GetString(sqlDataReader.GetOrdinal("SRC"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ModuleID")))
										{
											ptlBinLicEntity.ModuleID = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("ModuleID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ModuleColor")))
										{
											ptlBinLicEntity.ModuleColor = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("ModuleColor"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ModuleFlash")))
										{
											ptlBinLicEntity.ModuleFlash = sqlDataReader.GetBoolean(sqlDataReader.GetOrdinal("ModuleFlash"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ModuleBuzzer")))
										{
											ptlBinLicEntity.ModuleBuzzer = sqlDataReader.GetBoolean(sqlDataReader.GetOrdinal("ModuleBuzzer"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("IsLightCommandIssued")))
										{
											ptlBinLicEntity.IsLightCommandIssued = sqlDataReader.GetBoolean(sqlDataReader.GetOrdinal("IsLightCommandIssued"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("DisplayContent")))
										{
											ptlBinLicEntity.DisplayContent = sqlDataReader.GetString(sqlDataReader.GetOrdinal("DisplayContent"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ArticleID")))
										{
											ptlBinLicEntity.ArticleID = sqlDataReader.GetString(sqlDataReader.GetOrdinal("ArticleID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("StockIn")))
										{
											ptlBinLicEntity.StockIn = sqlDataReader.GetDecimal(sqlDataReader.GetOrdinal("StockIn"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("StockOut")))
										{
											ptlBinLicEntity.StockOut = sqlDataReader.GetDecimal(sqlDataReader.GetOrdinal("StockOut"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("Adjustment")))
										{
											ptlBinLicEntity.Adjustment = sqlDataReader.GetDecimal(sqlDataReader.GetOrdinal("Adjustment"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("Remarks")))
										{
											ptlBinLicEntity.Remarks = sqlDataReader.GetString(sqlDataReader.GetOrdinal("Remarks"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("RequestID")))
										{
											ptlBinLicEntity.RequestID = sqlDataReader.GetString(sqlDataReader.GetOrdinal("RequestID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("RequestCode")))
										{
											ptlBinLicEntity.RequestCode = sqlDataReader.GetString(sqlDataReader.GetOrdinal("RequestCode"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("LightOnOffCommand")))
										{
											ptlBinLicEntity.LightOnOffCommand = sqlDataReader.GetString(sqlDataReader.GetOrdinal("LightOnOffCommand"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("OnTimeStamp")))
										{
											ptlBinLicEntity.OnTimeStamp = sqlDataReader.GetDateTime(sqlDataReader.GetOrdinal("OnTimeStamp"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("TurnOffByButtonPress")))
										{
											ptlBinLicEntity.TurnOffByButtonPress = sqlDataReader.GetBoolean(sqlDataReader.GetOrdinal("TurnOffByButtonPress"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("TurnOffByCommand")))
										{
											ptlBinLicEntity.TurnOffByCommand = sqlDataReader.GetBoolean(sqlDataReader.GetOrdinal("TurnOffByCommand"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("OffTimeStamp")))
										{
											ptlBinLicEntity.OffTimeStamp = sqlDataReader.GetDateTime(sqlDataReader.GetOrdinal("OffTimeStamp"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("UpdatedBy")))
										{
											ptlBinLicEntity.UpdatedBy = sqlDataReader.GetString(sqlDataReader.GetOrdinal("UpdatedBy"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("UpdatedOn")))
										{
											ptlBinLicEntity.UpdatedOn = sqlDataReader.GetDateTime(sqlDataReader.GetOrdinal("UpdatedOn"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("CreatedBy")))
										{
											ptlBinLicEntity.CreatedBy = sqlDataReader.GetString(sqlDataReader.GetOrdinal("CreatedBy"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("CreatedOn")))
										{
											ptlBinLicEntity.CreatedOn = sqlDataReader.GetDateTime(sqlDataReader.GetOrdinal("CreatedOn"));
										}
									}
								}
								catch (Exception exception)
								{
									SysLog.WriteLog(3, exception.Message);
								}
							}
							finally
							{
								sqlDataReader.Close();
							}
						}
						sqlCommand.Dispose();
					}
				}
			}
			catch (Exception exception1)
			{
				SysLog.WriteLog(3, exception1.Message);
			}
			return ptlBinLicEntity;
		}

		public List<PtlBinLicEntity> GetList()
		{
			List<PtlBinLicEntity> ptlBinLicEntities = new List<PtlBinLicEntity>();
			try
			{
				if (ConnectorFactory.Connect())
				{
					string str = "SELECT ID, SystemName, BinID, SRC, ModuleID, ModuleColor, ModuleFlash, ModuleBuzzer, IsLightCommandIssued, DisplayContent, ArticleID, StockIn, StockOut, Adjustment, Remarks, RequestID, RequestCode, LightOnOffCommand, OnTimeStamp, TurnOffByButtonPress, TurnOffByCommand, OffTimeStamp, UpdatedBy, UpdatedOn, CreatedBy, CreatedOn FROM tbl_ptl_bin_lic";
					str = string.Concat(str, " ORDER BY CreatedOn");
					using (SqlCommand sqlCommand = new SqlCommand())
					{
						sqlCommand.Connection = ConnectorFactory.Connection;
						sqlCommand.CommandText = str;
						sqlCommand.Prepare();
						using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
						{
							try
							{
								try
								{
									while (sqlDataReader.Read())
									{
										PtlBinLicEntity ptlBinLicEntity = new PtlBinLicEntity();
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ID")))
										{
											ptlBinLicEntity.ID = sqlDataReader.GetGuid(sqlDataReader.GetOrdinal("ID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("SystemName")))
										{
											ptlBinLicEntity.SystemName = sqlDataReader.GetString(sqlDataReader.GetOrdinal("SystemName"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("BinID")))
										{
											ptlBinLicEntity.BinID = sqlDataReader.GetGuid(sqlDataReader.GetOrdinal("BinID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("SRC")))
										{
											ptlBinLicEntity.SRC = sqlDataReader.GetString(sqlDataReader.GetOrdinal("SRC"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ModuleID")))
										{
											ptlBinLicEntity.ModuleID = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("ModuleID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ModuleColor")))
										{
											ptlBinLicEntity.ModuleColor = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("ModuleColor"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ModuleFlash")))
										{
											ptlBinLicEntity.ModuleFlash = sqlDataReader.GetBoolean(sqlDataReader.GetOrdinal("ModuleFlash"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ModuleBuzzer")))
										{
											ptlBinLicEntity.ModuleBuzzer = sqlDataReader.GetBoolean(sqlDataReader.GetOrdinal("ModuleBuzzer"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("IsLightCommandIssued")))
										{
											ptlBinLicEntity.IsLightCommandIssued = sqlDataReader.GetBoolean(sqlDataReader.GetOrdinal("IsLightCommandIssued"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("DisplayContent")))
										{
											ptlBinLicEntity.DisplayContent = sqlDataReader.GetString(sqlDataReader.GetOrdinal("DisplayContent"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ArticleID")))
										{
											ptlBinLicEntity.ArticleID = sqlDataReader.GetString(sqlDataReader.GetOrdinal("ArticleID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("StockIn")))
										{
											ptlBinLicEntity.StockIn = sqlDataReader.GetDecimal(sqlDataReader.GetOrdinal("StockIn"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("StockOut")))
										{
											ptlBinLicEntity.StockOut = sqlDataReader.GetDecimal(sqlDataReader.GetOrdinal("StockOut"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("Adjustment")))
										{
											ptlBinLicEntity.Adjustment = sqlDataReader.GetDecimal(sqlDataReader.GetOrdinal("Adjustment"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("Remarks")))
										{
											ptlBinLicEntity.Remarks = sqlDataReader.GetString(sqlDataReader.GetOrdinal("Remarks"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("RequestID")))
										{
											ptlBinLicEntity.RequestID = sqlDataReader.GetString(sqlDataReader.GetOrdinal("RequestID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("RequestCode")))
										{
											ptlBinLicEntity.RequestCode = sqlDataReader.GetString(sqlDataReader.GetOrdinal("RequestCode"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("LightOnOffCommand")))
										{
											ptlBinLicEntity.LightOnOffCommand = sqlDataReader.GetString(sqlDataReader.GetOrdinal("LightOnOffCommand"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("OnTimeStamp")))
										{
											ptlBinLicEntity.OnTimeStamp = sqlDataReader.GetDateTime(sqlDataReader.GetOrdinal("OnTimeStamp"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("TurnOffByButtonPress")))
										{
											ptlBinLicEntity.TurnOffByButtonPress = sqlDataReader.GetBoolean(sqlDataReader.GetOrdinal("TurnOffByButtonPress"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("TurnOffByCommand")))
										{
											ptlBinLicEntity.TurnOffByCommand = sqlDataReader.GetBoolean(sqlDataReader.GetOrdinal("TurnOffByCommand"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("OffTimeStamp")))
										{
											ptlBinLicEntity.OffTimeStamp = sqlDataReader.GetDateTime(sqlDataReader.GetOrdinal("OffTimeStamp"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("UpdatedBy")))
										{
											ptlBinLicEntity.UpdatedBy = sqlDataReader.GetString(sqlDataReader.GetOrdinal("UpdatedBy"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("UpdatedOn")))
										{
											ptlBinLicEntity.UpdatedOn = sqlDataReader.GetDateTime(sqlDataReader.GetOrdinal("UpdatedOn"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("CreatedBy")))
										{
											ptlBinLicEntity.CreatedBy = sqlDataReader.GetString(sqlDataReader.GetOrdinal("CreatedBy"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("CreatedOn")))
										{
											ptlBinLicEntity.CreatedOn = sqlDataReader.GetDateTime(sqlDataReader.GetOrdinal("CreatedOn"));
										}
										ptlBinLicEntities.Add(ptlBinLicEntity);
									}
								}
								catch (Exception exception)
								{
									SysLog.WriteLog(3, exception.Message);
								}
							}
							finally
							{
								sqlDataReader.Close();
							}
						}
						sqlCommand.Dispose();
					}
				}
			}
			catch (Exception exception1)
			{
				SysLog.WriteLog(3, exception1.Message);
			}
			return ptlBinLicEntities;
		}

		public List<PtlBinLicEntity> GetList(string srcReference)
		{
			List<PtlBinLicEntity> ptlBinLicEntities = new List<PtlBinLicEntity>();
			try
			{
				if (ConnectorFactory.Connect())
				{
					string str = "SELECT ID, SystemName, BinID, SRC, ModuleID, ModuleColor, ModuleFlash, ModuleBuzzer, IsLightCommandIssued, DisplayContent, ArticleID, StockIn, StockOut, Adjustment, Remarks, RequestID, RequestCode, LightOnOffCommand, OnTimeStamp, TurnOffByButtonPress, TurnOffByCommand, OffTimeStamp, UpdatedBy, UpdatedOn, CreatedBy, CreatedOn FROM tbl_ptl_bin_lic";
					str = string.Concat(str, " WHERE SRC=@SRC ORDER BY CreatedOn");
					using (SqlCommand sqlCommand = new SqlCommand())
					{
						sqlCommand.Connection = ConnectorFactory.Connection;
						sqlCommand.CommandText = str;
						sqlCommand.Prepare();
						sqlCommand.Parameters.AddWithValue("@SRC", srcReference);
						using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
						{
							try
							{
								try
								{
									while (sqlDataReader.Read())
									{
										PtlBinLicEntity ptlBinLicEntity = new PtlBinLicEntity();
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ID")))
										{
											ptlBinLicEntity.ID = sqlDataReader.GetGuid(sqlDataReader.GetOrdinal("ID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("SystemName")))
										{
											ptlBinLicEntity.SystemName = sqlDataReader.GetString(sqlDataReader.GetOrdinal("SystemName"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("BinID")))
										{
											ptlBinLicEntity.BinID = sqlDataReader.GetGuid(sqlDataReader.GetOrdinal("BinID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("SRC")))
										{
											ptlBinLicEntity.SRC = sqlDataReader.GetString(sqlDataReader.GetOrdinal("SRC"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ModuleID")))
										{
											ptlBinLicEntity.ModuleID = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("ModuleID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ModuleColor")))
										{
											ptlBinLicEntity.ModuleColor = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("ModuleColor"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ModuleFlash")))
										{
											ptlBinLicEntity.ModuleFlash = sqlDataReader.GetBoolean(sqlDataReader.GetOrdinal("ModuleFlash"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ModuleBuzzer")))
										{
											ptlBinLicEntity.ModuleBuzzer = sqlDataReader.GetBoolean(sqlDataReader.GetOrdinal("ModuleBuzzer"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("IsLightCommandIssued")))
										{
											ptlBinLicEntity.IsLightCommandIssued = sqlDataReader.GetBoolean(sqlDataReader.GetOrdinal("IsLightCommandIssued"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("DisplayContent")))
										{
											ptlBinLicEntity.DisplayContent = sqlDataReader.GetString(sqlDataReader.GetOrdinal("DisplayContent"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ArticleID")))
										{
											ptlBinLicEntity.ArticleID = sqlDataReader.GetString(sqlDataReader.GetOrdinal("ArticleID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("StockIn")))
										{
											ptlBinLicEntity.StockIn = sqlDataReader.GetDecimal(sqlDataReader.GetOrdinal("StockIn"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("StockOut")))
										{
											ptlBinLicEntity.StockOut = sqlDataReader.GetDecimal(sqlDataReader.GetOrdinal("StockOut"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("Adjustment")))
										{
											ptlBinLicEntity.Adjustment = sqlDataReader.GetDecimal(sqlDataReader.GetOrdinal("Adjustment"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("Remarks")))
										{
											ptlBinLicEntity.Remarks = sqlDataReader.GetString(sqlDataReader.GetOrdinal("Remarks"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("RequestID")))
										{
											ptlBinLicEntity.RequestID = sqlDataReader.GetString(sqlDataReader.GetOrdinal("RequestID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("RequestCode")))
										{
											ptlBinLicEntity.RequestCode = sqlDataReader.GetString(sqlDataReader.GetOrdinal("RequestCode"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("LightOnOffCommand")))
										{
											ptlBinLicEntity.LightOnOffCommand = sqlDataReader.GetString(sqlDataReader.GetOrdinal("LightOnOffCommand"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("OnTimeStamp")))
										{
											ptlBinLicEntity.OnTimeStamp = sqlDataReader.GetDateTime(sqlDataReader.GetOrdinal("OnTimeStamp"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("TurnOffByButtonPress")))
										{
											ptlBinLicEntity.TurnOffByButtonPress = sqlDataReader.GetBoolean(sqlDataReader.GetOrdinal("TurnOffByButtonPress"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("TurnOffByCommand")))
										{
											ptlBinLicEntity.TurnOffByCommand = sqlDataReader.GetBoolean(sqlDataReader.GetOrdinal("TurnOffByCommand"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("OffTimeStamp")))
										{
											ptlBinLicEntity.OffTimeStamp = sqlDataReader.GetDateTime(sqlDataReader.GetOrdinal("OffTimeStamp"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("UpdatedBy")))
										{
											ptlBinLicEntity.UpdatedBy = sqlDataReader.GetString(sqlDataReader.GetOrdinal("UpdatedBy"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("UpdatedOn")))
										{
											ptlBinLicEntity.UpdatedOn = sqlDataReader.GetDateTime(sqlDataReader.GetOrdinal("UpdatedOn"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("CreatedBy")))
										{
											ptlBinLicEntity.CreatedBy = sqlDataReader.GetString(sqlDataReader.GetOrdinal("CreatedBy"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("CreatedOn")))
										{
											ptlBinLicEntity.CreatedOn = sqlDataReader.GetDateTime(sqlDataReader.GetOrdinal("CreatedOn"));
										}
										ptlBinLicEntities.Add(ptlBinLicEntity);
									}
								}
								catch (Exception exception)
								{
									SysLog.WriteLog(3, exception.Message);
								}
							}
							finally
							{
								sqlDataReader.Close();
							}
						}
						sqlCommand.Dispose();
					}
				}
			}
			catch (Exception exception1)
			{
				SysLog.WriteLog(3, exception1.Message);
			}
			return ptlBinLicEntities;
		}

		public List<PtlBinLicEntity> GetList(string srcReference, string onOff)
		{
			List<PtlBinLicEntity> ptlBinLicEntities = new List<PtlBinLicEntity>();
			try
			{
				if (ConnectorFactory.Connect())
				{
					string str = "SELECT ID, SystemName, BinID, SRC, ModuleID, ModuleColor, ModuleFlash, ModuleBuzzer, IsLightCommandIssued, DisplayContent, ArticleID, StockIn, StockOut, Adjustment, Remarks, RequestID, RequestCode, LightOnOffCommand, OnTimeStamp, TurnOffByButtonPress, TurnOffByCommand, OffTimeStamp, UpdatedBy, UpdatedOn, CreatedBy, CreatedOn FROM tbl_ptl_bin_lic";
					str = string.Concat(str, " WHERE SRC=@SRC AND LightOnOffCommand=@LightOnOffCommand ORDER BY CreatedOn");
					using (SqlCommand sqlCommand = new SqlCommand())
					{
						sqlCommand.Connection = ConnectorFactory.Connection;
						sqlCommand.CommandText = str;
						sqlCommand.Prepare();
						sqlCommand.Parameters.AddWithValue("@SRC", srcReference);
						sqlCommand.Parameters.AddWithValue("@LightOnOffCommand", onOff);
						using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
						{
							try
							{
								try
								{
									while (sqlDataReader.Read())
									{
										PtlBinLicEntity ptlBinLicEntity = new PtlBinLicEntity();
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ID")))
										{
											ptlBinLicEntity.ID = sqlDataReader.GetGuid(sqlDataReader.GetOrdinal("ID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("SystemName")))
										{
											ptlBinLicEntity.SystemName = sqlDataReader.GetString(sqlDataReader.GetOrdinal("SystemName"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("BinID")))
										{
											ptlBinLicEntity.BinID = sqlDataReader.GetGuid(sqlDataReader.GetOrdinal("BinID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("SRC")))
										{
											ptlBinLicEntity.SRC = sqlDataReader.GetString(sqlDataReader.GetOrdinal("SRC"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ModuleID")))
										{
											ptlBinLicEntity.ModuleID = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("ModuleID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ModuleColor")))
										{
											ptlBinLicEntity.ModuleColor = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("ModuleColor"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ModuleFlash")))
										{
											ptlBinLicEntity.ModuleFlash = sqlDataReader.GetBoolean(sqlDataReader.GetOrdinal("ModuleFlash"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ModuleBuzzer")))
										{
											ptlBinLicEntity.ModuleBuzzer = sqlDataReader.GetBoolean(sqlDataReader.GetOrdinal("ModuleBuzzer"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("IsLightCommandIssued")))
										{
											ptlBinLicEntity.IsLightCommandIssued = sqlDataReader.GetBoolean(sqlDataReader.GetOrdinal("IsLightCommandIssued"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("DisplayContent")))
										{
											ptlBinLicEntity.DisplayContent = sqlDataReader.GetString(sqlDataReader.GetOrdinal("DisplayContent"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ArticleID")))
										{
											ptlBinLicEntity.ArticleID = sqlDataReader.GetString(sqlDataReader.GetOrdinal("ArticleID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("StockIn")))
										{
											ptlBinLicEntity.StockIn = sqlDataReader.GetDecimal(sqlDataReader.GetOrdinal("StockIn"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("StockOut")))
										{
											ptlBinLicEntity.StockOut = sqlDataReader.GetDecimal(sqlDataReader.GetOrdinal("StockOut"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("Adjustment")))
										{
											ptlBinLicEntity.Adjustment = sqlDataReader.GetDecimal(sqlDataReader.GetOrdinal("Adjustment"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("Remarks")))
										{
											ptlBinLicEntity.Remarks = sqlDataReader.GetString(sqlDataReader.GetOrdinal("Remarks"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("RequestID")))
										{
											ptlBinLicEntity.RequestID = sqlDataReader.GetString(sqlDataReader.GetOrdinal("RequestID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("RequestCode")))
										{
											ptlBinLicEntity.RequestCode = sqlDataReader.GetString(sqlDataReader.GetOrdinal("RequestCode"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("LightOnOffCommand")))
										{
											ptlBinLicEntity.LightOnOffCommand = sqlDataReader.GetString(sqlDataReader.GetOrdinal("LightOnOffCommand"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("OnTimeStamp")))
										{
											ptlBinLicEntity.OnTimeStamp = sqlDataReader.GetDateTime(sqlDataReader.GetOrdinal("OnTimeStamp"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("TurnOffByButtonPress")))
										{
											ptlBinLicEntity.TurnOffByButtonPress = sqlDataReader.GetBoolean(sqlDataReader.GetOrdinal("TurnOffByButtonPress"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("TurnOffByCommand")))
										{
											ptlBinLicEntity.TurnOffByCommand = sqlDataReader.GetBoolean(sqlDataReader.GetOrdinal("TurnOffByCommand"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("OffTimeStamp")))
										{
											ptlBinLicEntity.OffTimeStamp = sqlDataReader.GetDateTime(sqlDataReader.GetOrdinal("OffTimeStamp"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("UpdatedBy")))
										{
											ptlBinLicEntity.UpdatedBy = sqlDataReader.GetString(sqlDataReader.GetOrdinal("UpdatedBy"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("UpdatedOn")))
										{
											ptlBinLicEntity.UpdatedOn = sqlDataReader.GetDateTime(sqlDataReader.GetOrdinal("UpdatedOn"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("CreatedBy")))
										{
											ptlBinLicEntity.CreatedBy = sqlDataReader.GetString(sqlDataReader.GetOrdinal("CreatedBy"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("CreatedOn")))
										{
											ptlBinLicEntity.CreatedOn = sqlDataReader.GetDateTime(sqlDataReader.GetOrdinal("CreatedOn"));
										}
										ptlBinLicEntities.Add(ptlBinLicEntity);
									}
								}
								catch (Exception exception)
								{
									SysLog.WriteLog(3, exception.Message);
								}
							}
							finally
							{
								sqlDataReader.Close();
							}
						}
						sqlCommand.Dispose();
					}
				}
			}
			catch (Exception exception1)
			{
				SysLog.WriteLog(3, exception1.Message);
			}
			return ptlBinLicEntities;
		}

		public bool Save(PtlBinLicEntity entity)
		{
			bool flag = false;
			try
			{
				if (ConnectorFactory.Connect())
				{
					string str = "UPDATE tbl_ptl_bin_lic SET SystemName=@SystemName, BinID=@BinID, SRC=@SRC, ModuleID=@ModuleID, ModuleColor=@ModuleColor, ModuleFlash=@ModuleFlash, ModuleBuzzer=@ModuleBuzzer, IsLightCommandIssued=@IsLightCommandIssued, DisplayContent=@DisplayContent, ArticleID=@ArticleID, StockIn=@StockIn, StockOut=@StockOut, Adjustment=@Adjustment, Remarks=@Remarks, RequestID=@RequestID, RequestCode=@RequestCode, LightOnOffCommand=@LightOnOffCommand, OnTimeStamp=@OnTimeStamp, TurnOffByButtonPress=@TurnOffByButtonPress, TurnOffByCommand=@TurnOffByCommand, OffTimeStamp=@OffTimeStamp, UpdatedBy=@UpdatedBy, UpdatedOn=@UpdatedOn, CreatedBy=@CreatedBy, CreatedOn=@CreatedOn WHERE ID=@ID";
					using (SqlCommand sqlCommand = new SqlCommand())
					{
						sqlCommand.Connection = ConnectorFactory.Connection;
						sqlCommand.CommandText = str;
						sqlCommand.Prepare();
						sqlCommand.Parameters.AddWithValue("@ID", entity.ID);
						sqlCommand.Parameters.AddWithValue("@SystemName", entity.SystemName);
						sqlCommand.Parameters.AddWithValue("@BinID", entity.BinID);
						sqlCommand.Parameters.AddWithValue("@SRC", entity.SRC);
						sqlCommand.Parameters.AddWithValue("@ModuleID", entity.ModuleID);
						sqlCommand.Parameters.AddWithValue("@ModuleColor", entity.ModuleColor);
						sqlCommand.Parameters.AddWithValue("@ModuleFlash", entity.ModuleFlash);
						sqlCommand.Parameters.AddWithValue("@ModuleBuzzer", entity.ModuleBuzzer);
						sqlCommand.Parameters.AddWithValue("@IsLightCommandIssued", entity.IsLightCommandIssued);
						sqlCommand.Parameters.AddWithValue("@DisplayContent", entity.DisplayContent);
						sqlCommand.Parameters.AddWithValue("@ArticleID", entity.ArticleID);
						sqlCommand.Parameters.AddWithValue("@StockIn", entity.StockIn);
						sqlCommand.Parameters.AddWithValue("@StockOut", entity.StockOut);
						sqlCommand.Parameters.AddWithValue("@Adjustment", entity.Adjustment);
						sqlCommand.Parameters.AddWithValue("@Remarks", entity.Remarks);
						sqlCommand.Parameters.AddWithValue("@RequestID", entity.RequestID);
						sqlCommand.Parameters.AddWithValue("@RequestCode", entity.RequestCode);
						sqlCommand.Parameters.AddWithValue("@LightOnOffCommand", entity.LightOnOffCommand);
						if (entity.OnTimeStamp != DateTime.MinValue)
						{
							sqlCommand.Parameters.AddWithValue("@OnTimeStamp", entity.OnTimeStamp);
						}
						else
						{
							sqlCommand.Parameters.AddWithValue("@OnTimeStamp", DateTime.Now);
						}
						sqlCommand.Parameters.AddWithValue("@TurnOffByButtonPress", entity.TurnOffByButtonPress);
						sqlCommand.Parameters.AddWithValue("@TurnOffByCommand", entity.TurnOffByCommand);
						if (entity.OffTimeStamp != DateTime.MinValue)
						{
							sqlCommand.Parameters.AddWithValue("@OffTimeStamp", entity.OffTimeStamp);
						}
						else
						{
							sqlCommand.Parameters.AddWithValue("@OffTimeStamp", DateTime.Now);
						}
						sqlCommand.Parameters.AddWithValue("@UpdatedBy", entity.UpdatedBy);
						if (entity.UpdatedOn != DateTime.MinValue)
						{
							sqlCommand.Parameters.AddWithValue("@UpdatedOn", entity.UpdatedOn);
						}
						else
						{
							sqlCommand.Parameters.AddWithValue("@UpdatedOn", DateTime.Now);
						}
						sqlCommand.Parameters.AddWithValue("@CreatedBy", entity.CreatedBy);
						if (entity.CreatedOn != DateTime.MinValue)
						{
							sqlCommand.Parameters.AddWithValue("@CreatedOn", entity.CreatedOn);
						}
						else
						{
							sqlCommand.Parameters.AddWithValue("@CreatedOn", DateTime.Now);
						}
						object obj = sqlCommand.ExecuteNonQuery();
						flag = true;
						sqlCommand.Dispose();
					}
				}
			}
			catch (Exception exception)
			{
				SysLog.WriteLog(3, exception.Message);
			}
			return flag;
		}
	}
}