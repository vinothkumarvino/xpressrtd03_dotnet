using MSClassLibrary.Log;
using OPtlDAL.Common;
using System;
using System.Data;
using System.Data.SqlClient;

namespace OPtlDAL.DataFactory
{
	internal static class ConnectorFactory
	{
		public static SqlConnection Connection;

		public static string ConnectionString;

		static ConnectorFactory()
		{
			ConnectorFactory.ConnectionString = string.Empty;
		}

		public static bool Connect()
		{
			bool flag = false;
			ConnectorFactory.ConnectionString = SysConfig.ConnectionString;
			if ((ConnectorFactory.Connection == null ? false : ConnectorFactory.Connection.State != ConnectionState.Closed))
			{
				flag = true;
			}
			else if (string.IsNullOrEmpty(ConnectorFactory.ConnectionString))
			{
				SysLog.WriteLog(3, "Invalid Database Connection String");
			}
			else
			{
				ConnectorFactory.Connection = new SqlConnection(ConnectorFactory.ConnectionString);
				try
				{
					SysLog.WriteLog(6, "Connecting to MsSQL...");
					ConnectorFactory.Connection.Open();
					SysLog.WriteLog(6, "Connecting to MsSQL - Success");
					flag = true;
				}
				catch
				{
					throw;
				}
				SysLog.WriteLog(6, "Done.");
			}
			return flag;
		}
	}
}