using MSClassLibrary.Log;
using OPtlDAL.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;

namespace OPtlDAL.DataFactory
{
	internal class ReorderListFactory
	{
		public ReorderListFactory()
		{
		}

		public bool Add(ref ReorderListEntity entity)
		{
			bool flag = false;
			try
			{
				if (ConnectorFactory.Connect())
				{
					string str = "INSERT INTO tbl_reorder_list (ID, ControllerID, BinID, ArticleID, ReOrderLevel, AvailableQuantity, RecordTimeStamp) VALUES (@ID, @ControllerID, @BinID, @ArticleID, @ReOrderLevel, @AvailableQuantity, @RecordTimeStamp);";
					using (SqlCommand sqlCommand = new SqlCommand())
					{
						sqlCommand.Connection = ConnectorFactory.Connection;
						sqlCommand.CommandText = str;
						sqlCommand.Prepare();
						sqlCommand.Parameters.AddWithValue("@ID", entity.ID);
						sqlCommand.Parameters.AddWithValue("@ControllerID", entity.ControllerID);
						sqlCommand.Parameters.AddWithValue("@BinID", entity.BinID);
						sqlCommand.Parameters.AddWithValue("@ArticleID", entity.ArticleID);
						sqlCommand.Parameters.AddWithValue("@ReOrderLevel", entity.ReOrderLevel);
						sqlCommand.Parameters.AddWithValue("@AvailableQuantity", entity.AvailableQuantity);
						if (entity.RecordTimeStamp != DateTime.MinValue)
						{
							sqlCommand.Parameters.AddWithValue("@RecordTimeStamp", entity.RecordTimeStamp);
						}
						else
						{
							sqlCommand.Parameters.AddWithValue("@RecordTimeStamp", DateTime.Now);
						}
						sqlCommand.ExecuteScalar();
						flag = true;
						sqlCommand.Dispose();
					}
				}
			}
			catch (Exception exception)
			{
				SysLog.WriteLog(3, exception.Message);
			}
			return flag;
		}

		public bool Delete(ReorderListEntity entity)
		{
			bool flag = false;
			try
			{
				if (ConnectorFactory.Connect())
				{
					string str = "DELETE FROM tbl_reorder_list WHERE ID=@ID";
					using (SqlCommand sqlCommand = new SqlCommand())
					{
						sqlCommand.Connection = ConnectorFactory.Connection;
						sqlCommand.CommandText = str;
						sqlCommand.Prepare();
						sqlCommand.Parameters.AddWithValue("@ID", entity.ID);
						object obj = sqlCommand.ExecuteNonQuery();
						flag = true;
						sqlCommand.Dispose();
					}
				}
			}
			catch (Exception exception)
			{
				SysLog.WriteLog(3, exception.Message);
			}
			return flag;
		}

		public ReorderListEntity Get(Guid ID)
		{
			ReorderListEntity reorderListEntity = null;
			try
			{
				if (ConnectorFactory.Connect())
				{
					string str = "SELECT ID, ControllerID, BinID, ArticleID, ReOrderLevel, AvailableQuantity, RecordTimeStamp FROM tbl_reorder_list";
					str = string.Concat(str, " WHERE ID=@ID;");
					using (SqlCommand sqlCommand = new SqlCommand())
					{
						sqlCommand.Connection = ConnectorFactory.Connection;
						sqlCommand.CommandText = str;
						sqlCommand.Prepare();
						sqlCommand.Parameters.AddWithValue("@ID", ID);
						using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
						{
							try
							{
								try
								{
									if (sqlDataReader.Read())
									{
										reorderListEntity = new ReorderListEntity();
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ID")))
										{
											reorderListEntity.ID = sqlDataReader.GetGuid(sqlDataReader.GetOrdinal("ID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ControllerID")))
										{
											reorderListEntity.ControllerID = sqlDataReader.GetInt64(sqlDataReader.GetOrdinal("ControllerID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("BinID")))
										{
											reorderListEntity.BinID = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("BinID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ArticleID")))
										{
											reorderListEntity.ArticleID = sqlDataReader.GetString(sqlDataReader.GetOrdinal("ArticleID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ReOrderLevel")))
										{
											reorderListEntity.ReOrderLevel = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("ReOrderLevel"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("AvailableQuantity")))
										{
											reorderListEntity.AvailableQuantity = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("AvailableQuantity"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("RecordTimeStamp")))
										{
											reorderListEntity.RecordTimeStamp = sqlDataReader.GetDateTime(sqlDataReader.GetOrdinal("RecordTimeStamp"));
										}
									}
								}
								catch (Exception exception)
								{
									SysLog.WriteLog(3, exception.Message);
								}
							}
							finally
							{
								sqlDataReader.Close();
							}
						}
						sqlCommand.Dispose();
					}
				}
			}
			catch (Exception exception1)
			{
				SysLog.WriteLog(3, exception1.Message);
			}
			return reorderListEntity;
		}

		public List<ReorderListEntity> GetList()
		{
			List<ReorderListEntity> reorderListEntities = new List<ReorderListEntity>();
			try
			{
				if (ConnectorFactory.Connect())
				{
					string str = "SELECT ID, ControllerID, BinID, ArticleID, ReOrderLevel, AvailableQuantity, RecordTimeStamp FROM tbl_reorder_list";
					using (SqlCommand sqlCommand = new SqlCommand())
					{
						sqlCommand.Connection = ConnectorFactory.Connection;
						sqlCommand.CommandText = str;
						sqlCommand.Prepare();
						using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
						{
							try
							{
								try
								{
									while (sqlDataReader.Read())
									{
										ReorderListEntity reorderListEntity = new ReorderListEntity();
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ID")))
										{
											reorderListEntity.ID = sqlDataReader.GetGuid(sqlDataReader.GetOrdinal("ID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ControllerID")))
										{
											reorderListEntity.ControllerID = sqlDataReader.GetInt64(sqlDataReader.GetOrdinal("ControllerID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("BinID")))
										{
											reorderListEntity.BinID = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("BinID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ArticleID")))
										{
											reorderListEntity.ArticleID = sqlDataReader.GetString(sqlDataReader.GetOrdinal("ArticleID"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("ReOrderLevel")))
										{
											reorderListEntity.ReOrderLevel = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("ReOrderLevel"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("AvailableQuantity")))
										{
											reorderListEntity.AvailableQuantity = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("AvailableQuantity"));
										}
										if (!sqlDataReader.IsDBNull(sqlDataReader.GetOrdinal("RecordTimeStamp")))
										{
											reorderListEntity.RecordTimeStamp = sqlDataReader.GetDateTime(sqlDataReader.GetOrdinal("RecordTimeStamp"));
										}
										reorderListEntities.Add(reorderListEntity);
									}
								}
								catch (Exception exception)
								{
									SysLog.WriteLog(3, exception.Message);
								}
							}
							finally
							{
								sqlDataReader.Close();
							}
						}
						sqlCommand.Dispose();
					}
				}
			}
			catch (Exception exception1)
			{
				SysLog.WriteLog(3, exception1.Message);
			}
			return reorderListEntities;
		}

		public bool Save(ReorderListEntity entity)
		{
			bool flag = false;
			try
			{
				if (ConnectorFactory.Connect())
				{
					string str = "UPDATE tbl_reorder_list SET ControllerID=@ControllerID, BinID=@BinID, ArticleID=@ArticleID, ReOrderLevel=@ReOrderLevel, AvailableQuantity=@AvailableQuantity, RecordTimeStamp=@RecordTimeStamp WHERE ID=@ID";
					using (SqlCommand sqlCommand = new SqlCommand())
					{
						sqlCommand.Connection = ConnectorFactory.Connection;
						sqlCommand.CommandText = str;
						sqlCommand.Prepare();
						sqlCommand.Parameters.AddWithValue("@ID", entity.ID);
						sqlCommand.Parameters.AddWithValue("@ControllerID", entity.ControllerID);
						sqlCommand.Parameters.AddWithValue("@BinID", entity.BinID);
						sqlCommand.Parameters.AddWithValue("@ArticleID", entity.ArticleID);
						sqlCommand.Parameters.AddWithValue("@ReOrderLevel", entity.ReOrderLevel);
						sqlCommand.Parameters.AddWithValue("@AvailableQuantity", entity.AvailableQuantity);
						if (entity.RecordTimeStamp != DateTime.MinValue)
						{
							sqlCommand.Parameters.AddWithValue("@RecordTimeStamp", entity.RecordTimeStamp);
						}
						else
						{
							sqlCommand.Parameters.AddWithValue("@RecordTimeStamp", DateTime.Now);
						}
						object obj = sqlCommand.ExecuteNonQuery();
						flag = true;
						sqlCommand.Dispose();
					}
				}
			}
			catch (Exception exception)
			{
				SysLog.WriteLog(3, exception.Message);
			}
			return flag;
		}
	}
}