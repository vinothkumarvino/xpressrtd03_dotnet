using System;

namespace OPtlDAL.Entity
{
	[Serializable]
	public class ReorderListEntity
	{
		private Guid iD = Guid.Empty;

		private long controllerID = (long)0;

		private int binID = 0;

		private string articleID = string.Empty;

		private int reOrderLevel = 0;

		private int availableQuantity = 0;

		private DateTime recordTimeStamp = DateTime.MinValue;

		public string ArticleID
		{
			get
			{
				return this.articleID;
			}
			set
			{
				this.articleID = value;
			}
		}

		public int AvailableQuantity
		{
			get
			{
				return this.availableQuantity;
			}
			set
			{
				this.availableQuantity = value;
			}
		}

		public int BinID
		{
			get
			{
				return this.binID;
			}
			set
			{
				this.binID = value;
			}
		}

		public long ControllerID
		{
			get
			{
				return this.controllerID;
			}
			set
			{
				this.controllerID = value;
			}
		}

		public Guid ID
		{
			get
			{
				return this.iD;
			}
			set
			{
				this.iD = value;
			}
		}

		public string IDGuidString
		{
			get
			{
				return this.iD.ToString();
			}
			set
			{
				try
				{
					this.iD = Guid.Parse(value);
				}
				catch
				{
				}
			}
		}

		public DateTime RecordTimeStamp
		{
			get
			{
				return this.recordTimeStamp;
			}
			set
			{
				this.recordTimeStamp = value;
			}
		}

		public int ReOrderLevel
		{
			get
			{
				return this.reOrderLevel;
			}
			set
			{
				this.reOrderLevel = value;
			}
		}

		public ReorderListEntity()
		{
		}
	}
}