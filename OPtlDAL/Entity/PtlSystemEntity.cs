using System;

namespace OPtlDAL.Entity
{
	[Serializable]
	public class PtlSystemEntity
	{
		private string systemName = string.Empty;

		private int unitNo = 0;

		private string groupName = string.Empty;

		private int mode = 0;

		private string modeString = string.Empty;

		private string iPAddress = string.Empty;

		private int iPPort = 0;

		private string description = string.Empty;

		private string area = string.Empty;

		private bool isActive = false;

		private int status = 0;

		private string statusMessage = string.Empty;

		private string notes = string.Empty;

		private DateTime startDateTime = DateTime.MinValue;

		private DateTime lastStopDateTime = DateTime.MinValue;

		private string createdBy = string.Empty;

		private DateTime createdOn = DateTime.MinValue;

		private string updatedBy = string.Empty;

		private DateTime updatedOn = DateTime.MinValue;

		public string Area
		{
			get
			{
				return this.area;
			}
			set
			{
				this.area = value;
			}
		}

		public string CreatedBy
		{
			get
			{
				return this.createdBy;
			}
			set
			{
				this.createdBy = value;
			}
		}

		public DateTime CreatedOn
		{
			get
			{
				return this.createdOn;
			}
			set
			{
				this.createdOn = value;
			}
		}

		public string Description
		{
			get
			{
				return this.description;
			}
			set
			{
				this.description = value;
			}
		}

		public string GroupName
		{
			get
			{
				return this.groupName;
			}
			set
			{
				this.groupName = value;
			}
		}

		public string IPAddress
		{
			get
			{
				return this.iPAddress;
			}
			set
			{
				this.iPAddress = value;
			}
		}

		public int IPPort
		{
			get
			{
				return this.iPPort;
			}
			set
			{
				this.iPPort = value;
			}
		}

		public bool IsActive
		{
			get
			{
				return this.isActive;
			}
			set
			{
				this.isActive = value;
			}
		}

		public DateTime LastStopDateTime
		{
			get
			{
				return this.lastStopDateTime;
			}
			set
			{
				this.lastStopDateTime = value;
			}
		}

		public int Mode
		{
			get
			{
				return this.mode;
			}
			set
			{
				this.mode = value;
			}
		}

		public string ModeString
		{
			get
			{
				return this.modeString;
			}
			set
			{
				this.modeString = value;
			}
		}

		public string Notes
		{
			get
			{
				return this.notes;
			}
			set
			{
				this.notes = value;
			}
		}

		public DateTime StartDateTime
		{
			get
			{
				return this.startDateTime;
			}
			set
			{
				this.startDateTime = value;
			}
		}

		public int Status
		{
			get
			{
				return this.status;
			}
			set
			{
				this.status = value;
			}
		}

		public string StatusMessage
		{
			get
			{
				return this.statusMessage;
			}
			set
			{
				this.statusMessage = value;
			}
		}

		public string SystemName
		{
			get
			{
				return this.systemName;
			}
			set
			{
				this.systemName = value;
			}
		}

		public int UnitNo
		{
			get
			{
				return this.unitNo;
			}
			set
			{
				this.unitNo = value;
			}
		}

		public string UpdatedBy
		{
			get
			{
				return this.updatedBy;
			}
			set
			{
				this.updatedBy = value;
			}
		}

		public DateTime UpdatedOn
		{
			get
			{
				return this.updatedOn;
			}
			set
			{
				this.updatedOn = value;
			}
		}

		public PtlSystemEntity()
		{
		}
	}
}