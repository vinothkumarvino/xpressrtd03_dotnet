using System;

namespace OPtlDAL.Entity
{
	[Serializable]
	public class PtlBinsEntity
	{
		private Guid binID = Guid.Empty;

		private string systemName = string.Empty;

		private int moduleID = 0;

		private int moduleColor = 0;

		private bool moduleFlash = false;

		private string sRC = string.Empty;

		private string articleID = string.Empty;

		private int availableQuantity = 0;

		private int reOrderLevel = 0;

		private int pickQuantity = 0;

		private bool pickLightStatus = false;

		private int stockQuantity = 0;

		private bool stockLightStatus = false;

		public string ArticleID
		{
			get
			{
				return this.articleID;
			}
			set
			{
				this.articleID = value;
			}
		}

		public int AvailableQuantity
		{
			get
			{
				return this.availableQuantity;
			}
			set
			{
				this.availableQuantity = value;
			}
		}

		public Guid BinID
		{
			get
			{
				return this.binID;
			}
			set
			{
				this.binID = value;
			}
		}

		public string BinIDGuidString
		{
			get
			{
				return this.binID.ToString();
			}
			set
			{
				try
				{
					this.binID = Guid.Parse(value);
				}
				catch
				{
				}
			}
		}

		public int ModuleColor
		{
			get
			{
				return this.moduleColor;
			}
			set
			{
				this.moduleColor = value;
			}
		}

		public bool ModuleFlash
		{
			get
			{
				return this.moduleFlash;
			}
			set
			{
				this.moduleFlash = value;
			}
		}

		public int ModuleID
		{
			get
			{
				return this.moduleID;
			}
			set
			{
				this.moduleID = value;
			}
		}

		public bool PickLightStatus
		{
			get
			{
				return this.pickLightStatus;
			}
			set
			{
				this.pickLightStatus = value;
			}
		}

		public int PickQuantity
		{
			get
			{
				return this.pickQuantity;
			}
			set
			{
				this.pickQuantity = value;
			}
		}

		public int ReOrderLevel
		{
			get
			{
				return this.reOrderLevel;
			}
			set
			{
				this.reOrderLevel = value;
			}
		}

		public string SRC
		{
			get
			{
				return this.sRC;
			}
			set
			{
				this.sRC = value;
			}
		}

		public bool StockLightStatus
		{
			get
			{
				return this.stockLightStatus;
			}
			set
			{
				this.stockLightStatus = value;
			}
		}

		public int StockQuantity
		{
			get
			{
				return this.stockQuantity;
			}
			set
			{
				this.stockQuantity = value;
			}
		}

		public string SystemName
		{
			get
			{
				return this.systemName;
			}
			set
			{
				this.systemName = value;
			}
		}

		public PtlBinsEntity()
		{
		}
	}
}