using System;

namespace OPtlDAL.Entity
{
	public class DailyTaskEntity
	{
		private int iD = 0;

		private string description = string.Empty;

		private DateTime lastExecutionTimeStamp = DateTime.MinValue;

		public string Description
		{
			get
			{
				return this.description;
			}
			set
			{
				this.description = value;
			}
		}

		public bool ExecuteDailyTask
		{
			get
			{
				bool flag = false;
				if (Convert.ToInt32((DateTime.Now - this.LastExecutionTimeStamp).TotalDays) >= 1)
				{
					flag = true;
				}
				return flag;
			}
		}

		public int ID
		{
			get
			{
				return this.iD;
			}
			set
			{
				this.iD = value;
			}
		}

		public DateTime LastExecutionTimeStamp
		{
			get
			{
				return this.lastExecutionTimeStamp;
			}
			set
			{
				this.lastExecutionTimeStamp = value;
			}
		}

		public DailyTaskEntity()
		{
		}
	}
}