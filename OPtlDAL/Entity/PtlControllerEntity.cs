using System;

namespace OPtlDAL.Entity
{
	[Serializable]
	public class PtlControllerEntity
	{
		private Guid systemID = Guid.Empty;

		private int unitNo = 0;

		private string systemName = string.Empty;

		private int noOfLightModules = 0;

		private bool flashEnabled = false;

		private bool audioEnabled = false;

		private bool overwriteEnabled = false;

		public bool AudioEnabled
		{
			get
			{
				return this.audioEnabled;
			}
			set
			{
				this.audioEnabled = value;
			}
		}

		public bool FlashEnabled
		{
			get
			{
				return this.flashEnabled;
			}
			set
			{
				this.flashEnabled = value;
			}
		}

		public int NoOfLightModules
		{
			get
			{
				return this.noOfLightModules;
			}
			set
			{
				this.noOfLightModules = value;
			}
		}

		public bool OverwriteEnabled
		{
			get
			{
				return this.overwriteEnabled;
			}
			set
			{
				this.overwriteEnabled = value;
			}
		}

		public Guid SystemID
		{
			get
			{
				return this.systemID;
			}
			set
			{
				this.systemID = value;
			}
		}

		public string SystemIDGuidString
		{
			get
			{
				return this.systemID.ToString();
			}
			set
			{
				try
				{
					this.systemID = Guid.Parse(value);
				}
				catch
				{
				}
			}
		}

		public string SystemName
		{
			get
			{
				return this.systemName;
			}
			set
			{
				this.systemName = value;
			}
		}

		public int UnitNo
		{
			get
			{
				return this.unitNo;
			}
			set
			{
				this.unitNo = value;
			}
		}

		public PtlControllerEntity()
		{
		}
	}
}