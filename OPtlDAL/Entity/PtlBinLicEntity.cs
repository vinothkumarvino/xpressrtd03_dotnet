using System;

namespace OPtlDAL.Entity
{
	[Serializable]
	public class PtlBinLicEntity
	{
		private Guid iD = Guid.Empty;

		private string systemName = string.Empty;

		private Guid binID = Guid.Empty;

		private string sRC = string.Empty;

		private int moduleID = 0;

		private int moduleColor = 0;

		private bool moduleFlash = false;

		private bool moduleBuzzer = false;

		private bool isLightCommandIssued = false;

		private string displayContent = string.Empty;

		private string articleID = string.Empty;

		private decimal stockIn = new decimal();

		private decimal stockOut = new decimal();

		private decimal adjustment = new decimal();

		private string remarks = string.Empty;

		private string requestID = string.Empty;

		private string requestCode = string.Empty;

		private string lightOnOffCommand = string.Empty;

		private DateTime onTimeStamp = DateTime.MinValue;

		private bool turnOffByButtonPress = false;

		private bool turnOffByCommand = false;

		private DateTime offTimeStamp = DateTime.MinValue;

		private string updatedBy = string.Empty;

		private DateTime updatedOn = DateTime.MinValue;

		private string createdBy = string.Empty;

		private DateTime createdOn = DateTime.MinValue;

		public decimal Adjustment
		{
			get
			{
				return this.adjustment;
			}
			set
			{
				this.adjustment = value;
			}
		}

		public string ArticleID
		{
			get
			{
				return this.articleID;
			}
			set
			{
				this.articleID = value;
			}
		}

		public Guid BinID
		{
			get
			{
				return this.binID;
			}
			set
			{
				this.binID = value;
			}
		}

		public string BinIDGuidString
		{
			get
			{
				return this.binID.ToString();
			}
			set
			{
				try
				{
					this.binID = Guid.Parse(value);
				}
				catch
				{
				}
			}
		}

		public string CreatedBy
		{
			get
			{
				return this.createdBy;
			}
			set
			{
				this.createdBy = value;
			}
		}

		public DateTime CreatedOn
		{
			get
			{
				return this.createdOn;
			}
			set
			{
				this.createdOn = value;
			}
		}

		public string DisplayContent
		{
			get
			{
				return this.displayContent;
			}
			set
			{
				this.displayContent = value;
			}
		}

		public string FormatedDisplayContent
		{
			get
			{
				string str;
				try
				{
					int num = int.Parse(this.displayContent);
					str = num.ToString("00000");
				}
				catch
				{
					str = 0.ToString("00000");
				}
				return str;
			}
		}

		public Guid ID
		{
			get
			{
				return this.iD;
			}
			set
			{
				this.iD = value;
			}
		}

		public string IDGuidString
		{
			get
			{
				return this.iD.ToString();
			}
			set
			{
				try
				{
					this.iD = Guid.Parse(value);
				}
				catch
				{
				}
			}
		}

		public bool IsDisplayContent
		{
			get
			{
				bool flag = false;
				if (!this.displayContent.Equals("[0]"))
				{
					flag = true;
				}
				return flag;
			}
		}

		public bool IsLightCommandIssued
		{
			get
			{
				return this.isLightCommandIssued;
			}
			set
			{
				this.isLightCommandIssued = value;
			}
		}

		public string LightOnOffCommand
		{
			get
			{
				return this.lightOnOffCommand;
			}
			set
			{
				this.lightOnOffCommand = value;
			}
		}

		public bool ModuleBuzzer
		{
			get
			{
				return this.moduleBuzzer;
			}
			set
			{
				this.moduleBuzzer = value;
			}
		}

		public int ModuleColor
		{
			get
			{
				return this.moduleColor;
			}
			set
			{
				this.moduleColor = value;
			}
		}

		public bool ModuleFlash
		{
			get
			{
				return this.moduleFlash;
			}
			set
			{
				this.moduleFlash = value;
			}
		}

		public int ModuleID
		{
			get
			{
				return this.moduleID;
			}
			set
			{
				this.moduleID = value;
			}
		}

		public string ModuleIDString
		{
			get
			{
				return this.moduleID.ToString("0000");
			}
		}

		public DateTime OffTimeStamp
		{
			get
			{
				return this.offTimeStamp;
			}
			set
			{
				this.offTimeStamp = value;
			}
		}

		public DateTime OnTimeStamp
		{
			get
			{
				return this.onTimeStamp;
			}
			set
			{
				this.onTimeStamp = value;
			}
		}

		public string Remarks
		{
			get
			{
				return this.remarks;
			}
			set
			{
				this.remarks = value;
			}
		}

		public string RequestCode
		{
			get
			{
				return this.requestCode;
			}
			set
			{
				this.requestCode = value;
			}
		}

		public string RequestID
		{
			get
			{
				return this.requestID;
			}
			set
			{
				this.requestID = value;
			}
		}

		public string SRC
		{
			get
			{
				return this.sRC;
			}
			set
			{
				this.sRC = value;
			}
		}

		public decimal StockIn
		{
			get
			{
				return this.stockIn;
			}
			set
			{
				this.stockIn = value;
			}
		}

		public decimal StockOut
		{
			get
			{
				return this.stockOut;
			}
			set
			{
				this.stockOut = value;
			}
		}

		public string SystemName
		{
			get
			{
				return this.systemName;
			}
			set
			{
				this.systemName = value;
			}
		}

		public bool TurnOffByButtonPress
		{
			get
			{
				return this.turnOffByButtonPress;
			}
			set
			{
				this.turnOffByButtonPress = value;
			}
		}

		public bool TurnOffByCommand
		{
			get
			{
				return this.turnOffByCommand;
			}
			set
			{
				this.turnOffByCommand = value;
			}
		}

		public string UpdatedBy
		{
			get
			{
				return this.updatedBy;
			}
			set
			{
				this.updatedBy = value;
			}
		}

		public DateTime UpdatedOn
		{
			get
			{
				return this.updatedOn;
			}
			set
			{
				this.updatedOn = value;
			}
		}

		public PtlBinLicEntity()
		{
		}
	}
}