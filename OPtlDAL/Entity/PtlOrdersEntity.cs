using System;

namespace OPtlDAL.Entity
{
	[Serializable]
	public class PtlOrdersEntity
	{
		private Guid iD = Guid.Empty;

		private string transactionType = string.Empty;

		private string patientID = string.Empty;

		private string patientName = string.Empty;

		private string medicalRecordNo = string.Empty;

		private string articleID = string.Empty;

		private decimal quantity = new decimal();

		private string userName = string.Empty;

		private string area = string.Empty;

		private DateTime transactionDateTime = DateTime.MinValue;

		public string Area
		{
			get
			{
				return this.area;
			}
			set
			{
				this.area = value;
			}
		}

		public string ArticleID
		{
			get
			{
				return this.articleID;
			}
			set
			{
				this.articleID = value;
			}
		}

		public Guid ID
		{
			get
			{
				return this.iD;
			}
			set
			{
				this.iD = value;
			}
		}

		public string IDGuidString
		{
			get
			{
				return this.iD.ToString();
			}
			set
			{
				try
				{
					this.iD = Guid.Parse(value);
				}
				catch
				{
				}
			}
		}

		public string MedicalRecordNo
		{
			get
			{
				return this.medicalRecordNo;
			}
			set
			{
				this.medicalRecordNo = value;
			}
		}

		public string PatientID
		{
			get
			{
				return this.patientID;
			}
			set
			{
				this.patientID = value;
			}
		}

		public string PatientName
		{
			get
			{
				return this.patientName;
			}
			set
			{
				this.patientName = value;
			}
		}

		public decimal Quantity
		{
			get
			{
				return this.quantity;
			}
			set
			{
				this.quantity = value;
			}
		}

		public DateTime TransactionDateTime
		{
			get
			{
				return this.transactionDateTime;
			}
			set
			{
				this.transactionDateTime = value;
			}
		}

		public string TransactionType
		{
			get
			{
				return this.transactionType;
			}
			set
			{
				this.transactionType = value;
			}
		}

		public string UserName
		{
			get
			{
				return this.userName;
			}
			set
			{
				this.userName = value;
			}
		}

		public PtlOrdersEntity()
		{
		}
	}
}