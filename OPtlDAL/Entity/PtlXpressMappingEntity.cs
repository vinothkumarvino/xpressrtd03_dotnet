using System;

namespace OPtlDAL.Entity
{
	[Serializable]
	public class PtlXpressMappingEntity
	{
		private string pigeonHoleID = string.Empty;

		private string sRC = string.Empty;

		private bool isFront = false;

		private bool isBack = false;

		public bool IsBack
		{
			get
			{
				return this.isBack;
			}
			set
			{
				this.isBack = value;
			}
		}

		public bool IsFront
		{
			get
			{
				return this.isFront;
			}
			set
			{
				this.isFront = value;
			}
		}

		public string PigeonHoleID
		{
			get
			{
				return this.pigeonHoleID;
			}
			set
			{
				this.pigeonHoleID = value;
			}
		}

		public string SRC
		{
			get
			{
				return this.sRC;
			}
			set
			{
				this.sRC = value;
			}
		}

		public PtlXpressMappingEntity()
		{
		}
	}
}