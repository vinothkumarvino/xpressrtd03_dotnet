using System;

namespace OPtlDAL.Entity
{
	[Serializable]
	public class ArticleMasterEntity
	{
		private string articleID = string.Empty;

		private string articleName = string.Empty;

		private string articleItemCode = string.Empty;

		private string articleItemBarCode = string.Empty;

		private string abbreviatedDosageForm = string.Empty;

		private string packageUnit = string.Empty;

		private decimal width = new decimal();

		private decimal length = new decimal();

		private decimal height = new decimal();

		private decimal diameter = new decimal();

		private decimal weight = new decimal();

		private int packageSize = 0;

		private int tabOrCapPerBlister = 0;

		private int noOfBlister = 0;

		private DateTime expiryDate = DateTime.MinValue;

		private bool isActive = false;

		private string notes = string.Empty;

		private int reOrderLevel = 0;

		private int totalQuantity = 0;

		private string createdBy = string.Empty;

		private DateTime createdOn = DateTime.MinValue;

		private string updatedBy = string.Empty;

		private DateTime updatedOn = DateTime.MinValue;

		public string AbbreviatedDosageForm
		{
			get
			{
				return this.abbreviatedDosageForm;
			}
			set
			{
				this.abbreviatedDosageForm = value;
			}
		}

		public string ArticleID
		{
			get
			{
				return this.articleID;
			}
			set
			{
				this.articleID = value;
			}
		}

		public string ArticleItemBarCode
		{
			get
			{
				return this.articleItemBarCode;
			}
			set
			{
				this.articleItemBarCode = value;
			}
		}

		public string ArticleItemCode
		{
			get
			{
				return this.articleItemCode;
			}
			set
			{
				this.articleItemCode = value;
			}
		}

		public string ArticleName
		{
			get
			{
				return this.articleName;
			}
			set
			{
				this.articleName = value;
			}
		}

		public string CreatedBy
		{
			get
			{
				return this.createdBy;
			}
			set
			{
				this.createdBy = value;
			}
		}

		public DateTime CreatedOn
		{
			get
			{
				return this.createdOn;
			}
			set
			{
				this.createdOn = value;
			}
		}

		public decimal Diameter
		{
			get
			{
				return this.diameter;
			}
			set
			{
				this.diameter = value;
			}
		}

		public DateTime ExpiryDate
		{
			get
			{
				return this.expiryDate;
			}
			set
			{
				this.expiryDate = value;
			}
		}

		public decimal Height
		{
			get
			{
				return this.height;
			}
			set
			{
				this.height = value;
			}
		}

		public bool IsActive
		{
			get
			{
				return this.isActive;
			}
			set
			{
				this.isActive = value;
			}
		}

		public decimal Length
		{
			get
			{
				return this.length;
			}
			set
			{
				this.length = value;
			}
		}

		public int NoOfBlister
		{
			get
			{
				return this.noOfBlister;
			}
			set
			{
				this.noOfBlister = value;
			}
		}

		public string Notes
		{
			get
			{
				return this.notes;
			}
			set
			{
				this.notes = value;
			}
		}

		public int PackageSize
		{
			get
			{
				return this.packageSize;
			}
			set
			{
				this.packageSize = value;
			}
		}

		public string PackageUnit
		{
			get
			{
				return this.packageUnit;
			}
			set
			{
				this.packageUnit = value;
			}
		}

		public int ReOrderLevel
		{
			get
			{
				return this.reOrderLevel;
			}
			set
			{
				this.reOrderLevel = value;
			}
		}

		public int TabOrCapPerBlister
		{
			get
			{
				return this.tabOrCapPerBlister;
			}
			set
			{
				this.tabOrCapPerBlister = value;
			}
		}

		public int TotalQuantity
		{
			get
			{
				return this.totalQuantity;
			}
			set
			{
				this.totalQuantity = value;
			}
		}

		public string UpdatedBy
		{
			get
			{
				return this.updatedBy;
			}
			set
			{
				this.updatedBy = value;
			}
		}

		public DateTime UpdatedOn
		{
			get
			{
				return this.updatedOn;
			}
			set
			{
				this.updatedOn = value;
			}
		}

		public decimal Weight
		{
			get
			{
				return this.weight;
			}
			set
			{
				this.weight = value;
			}
		}

		public decimal Width
		{
			get
			{
				return this.width;
			}
			set
			{
				this.width = value;
			}
		}

		public ArticleMasterEntity()
		{
		}
	}
}