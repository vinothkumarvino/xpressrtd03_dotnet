﻿using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Runtime.Versioning;

[assembly: AssemblyCompany("OmniHealth Pte Ltd, Singapore.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCopyright("Copyright © 2017 OmniHealth Pte Ltd, Singapore.")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: AssemblyProduct("OConsisQueue")]
[assembly: AssemblyTitle("OConsisQueue")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyVersion("1.0.6600.17666")]
[assembly: CompilationRelaxations(8)]
[assembly: ComVisible(false)]
[assembly: Debuggable(DebuggableAttribute.DebuggingModes.Default | DebuggableAttribute.DebuggingModes.DisableOptimizations | DebuggableAttribute.DebuggingModes.IgnoreSymbolStoreSequencePoints | DebuggableAttribute.DebuggingModes.EnableEditAndContinue)]
[assembly: Guid("9b85110d-dc6d-4cea-9d64-d61f3273164b")]
[assembly: RuntimeCompatibility(WrapNonExceptionThrows=true)]
