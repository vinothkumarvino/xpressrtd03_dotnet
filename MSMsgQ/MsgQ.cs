using MSClassLibrary.Log;
using System;
using System.Messaging;
using System.Reflection;
using System.Threading;

namespace MSMsgQ
{
	public static class MsgQ
	{
		public static string CALLING_SERVICE_NAME;

		static MsgQ()
		{
			MsgQ.CALLING_SERVICE_NAME = "MS01";
		}

		public static bool Delete(EnumQueueName qName, Message data)
		{
			bool flag = false;
			try
			{
				using (MessageQueue q = MsgQ.GetQ(qName, string.Format("MS-{0}", qName.ToString())))
				{
					q.Formatter = new BinaryMessageFormatter();
					q.ReceiveById(data.Id);
					q.Close();
					flag = true;
				}
			}
			catch (MessageQueueException messageQueueException1)
			{
				MessageQueueException messageQueueException = messageQueueException1;
				if (messageQueueException.MessageQueueErrorCode == MessageQueueErrorCode.AccessDenied)
				{
					SysLog.WriteLog(3, "Access is denied. Queue might be a system queue.");
					throw;
				}
				SysLog.WriteLog(3, string.Format("{0}-{1}", messageQueueException.MessageQueueErrorCode, messageQueueException.Message));
				throw;
			}
			catch (Exception exception)
			{
				SysLog.WriteLog(3, exception.Message);
			}
			return flag;
		}

		public static Message[] GetAllMessage(EnumQueueName qName)
		{
			Message[] allMessages;
			using (MessageQueue q = MsgQ.GetQ(qName, string.Format("MS-{0}", qName.ToString())))
			{
				MessagePropertyFilter messagePropertyFilter = new MessagePropertyFilter()
				{
					AdministrationQueue = false,
					ArrivedTime = false,
					CorrelationId = false,
					Priority = false,
					ResponseQueue = false,
					SentTime = false,
					Body = false,
					Label = false,
					Id = false
				};
				allMessages = q.GetAllMessages();
			}
			return allMessages;
		}

		public static Message[] GetCurrentMessage(EnumQueueName qName)
		{
			Message[] allMessages;
			using (MessageQueue q = MsgQ.GetQ(qName, string.Format("MS-{0}", qName.ToString())))
			{
				q.Formatter = new BinaryMessageFormatter();
				allMessages = q.GetAllMessages();
			}
			return allMessages;
		}

		private static int GetMessageCount(MessageQueue q)
		{
			q.MessageReadPropertyFilter = new MessagePropertyFilter()
			{
				AdministrationQueue = false,
				ArrivedTime = false,
				CorrelationId = false,
				Priority = false,
				ResponseQueue = false,
				SentTime = false,
				Body = false,
				Label = false,
				Id = false
			};
			return (int)q.GetAllMessages().Length;
		}

		public static int GetMessageCount(EnumQueueName qName)
		{
			int length = 0;
			try
			{
				using (MessageQueue q = MsgQ.GetQ(qName, string.Format("MS-{0}", qName.ToString())))
				{
					q.MessageReadPropertyFilter = new MessagePropertyFilter()
					{
						AdministrationQueue = false,
						ArrivedTime = false,
						CorrelationId = false,
						Priority = false,
						ResponseQueue = false,
						SentTime = false,
						Body = false,
						Label = false,
						Id = false
					};
					length = (int)q.GetAllMessages().Length;
				}
			}
			catch (Exception exception)
			{
				SysLog.WriteLog(3, exception.Message);
			}
			return length;
		}

		private static MessageQueue GetQ(EnumQueueName qName, string Label)
		{
			MessageQueue messageQueues = null;
			try
			{
				string str = string.Format(".\\Private$\\{0}.{1}", MsgQ.CALLING_SERVICE_NAME, qName.ToString());
				if (!MessageQueue.Exists(str))
				{
					SysLog.WriteLog(4, string.Format("Q {0} does not exist", str));
					SysLog.WriteLog(6, string.Format("Q {0} creating new.", str));
					using (MessageQueue label = MessageQueue.Create(str))
					{
						label.Label = Label;
						label.SetPermissions("Everyone", MessageQueueAccessRights.FullControl, AccessControlEntryType.Allow);
						label.Close();
					}
					Thread.Sleep(1000);
					SysLog.WriteLog(6, string.Format("Q {0} successfully created", str));
				}
				messageQueues = new MessageQueue(str);
			}
			catch (Exception exception)
			{
				SysLog.WriteLog(3, exception.Message);
			}
			return messageQueues;
		}

		public static string GetVersion()
		{
			return Assembly.GetExecutingAssembly().GetName().Version.ToString();
		}

		public static bool Read(EnumQueueName qName, ref Message data)
		{
			bool flag = false;
			try
			{
				using (MessageQueue q = MsgQ.GetQ(qName, string.Format("MS-{0}", qName.ToString())))
				{
					q.Formatter = new BinaryMessageFormatter();
					using (Message message = q.Peek())
					{
						if (message != null)
						{
							SysLog.WriteLog(6, string.Concat("Reading (Output) Q: ", message.Label));
							data = message;
							q.Close();
							flag = true;
						}
					}
				}
			}
			catch (MessageQueueException messageQueueException1)
			{
				MessageQueueException messageQueueException = messageQueueException1;
				if (messageQueueException.MessageQueueErrorCode == MessageQueueErrorCode.AccessDenied)
				{
					SysLog.WriteLog(3, "Access is denied. Queue might be a system queue.");
					throw;
				}
				SysLog.WriteLog(3, string.Format("{0}-{1}", messageQueueException.MessageQueueErrorCode, messageQueueException.Message));
				throw;
			}
			catch (Exception exception)
			{
				SysLog.WriteLog(3, exception.Message);
			}
			return flag;
		}

		public static void Send(EnumQueueName qName, MessagePriority priority, string messageLabel, object data)
		{
			try
			{
				using (MessageQueue q = MsgQ.GetQ(qName, string.Format("MS-{0}", qName.ToString())))
				{
					SysLog.WriteLog(6, string.Concat("Adding data to Q [", qName.ToString(), "]"));
					using (Message message = new Message(data, new BinaryMessageFormatter()))
					{
						DateTime now = DateTime.Now;
						message.Label = string.Format("{0}-{1}", messageLabel, now.ToString("yyyyMMddHHmmssfff"));
						message.Priority = priority;
						q.Send(message);
						string str = string.Format("Queue {0} contains {1} messages", qName.ToString(), MsgQ.GetMessageCount(q));
						SysLog.WriteLog(6, str);
						q.Close();
					}
				}
			}
			catch (Exception exception)
			{
				SysLog.WriteLog(3, exception.Message);
			}
		}
	}
}