using System;

namespace MSMsgQ
{
	public enum EnumQueueName
	{
		Opas2ConsisQLow,
		Opas2ConsisQNormal,
		Opas2ConsisQHigh,
		Opas2ScriptProQLow,
		Opas2ScriptProQNormal,
		Opas2ScriptProQHigh,
		Opas2PickToLightQLow,
		Opas2PickToLightQNormal,
		Opas2PickToLightQHigh,
		Opas2Transport,
		Consis2Opas,
		ScriptPro2Opas,
		PickToLight2Opas,
		Transport2Opas
	}
}